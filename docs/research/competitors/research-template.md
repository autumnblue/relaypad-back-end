# Competitor research template
Template for documenting critical information about a competing product.

| Contents |
|:---------|
| [Research summary](#research-summary) |
| [Value proposition](#value-proposition) |
| [Onboarding experience](#onboarding-experience) |
| [Business model](#business-model) |

## Research summary
*Place to summarize findings and general thoughts about competitor's product.*
…

## Value proposition
Documenting how the competitor positions themselves to customers.

 - **Problems being solved**
   - …
 - **Core messaging**
   - …

## Onboarding experience
Overview of first time user experience.

| Screenshot | Notes |
|:-----------|:------|
| | |

## Business model
Notes on how this competitor drives revenue and current performance (if information available).

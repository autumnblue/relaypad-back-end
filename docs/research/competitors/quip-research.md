# Quip research
> A San Francisco company committed to changing the way teams work and helping organizations work less dumb.

> …

> Bret Taylor and Kevin Gibbs leave Facebook and Google to found Quip and redefine productivity software. Quip raises $15M in series A from Benchmark Capital and launch service mid-2013

> …

> Today, over a million users and thousands of teams have adopted Quip's living documents in place of email, files and superfluous chat.

> …

> We have obsessed (compulsively obsessed… day and night) about the intersection between communication and content. When you build a living document with Quip, the content (words, images, spreadsheet, data) becomes the communication — clarity prevails, good decision making is accelerated and team work improves.

| Contents |
|:---------|
| [Research summary](#research-summary) |
| [Value proposition](#value-proposition) |
| [Onboarding experience](#onboarding-experience) |
| [Business model](#business-model) |

## Research summary
Definitely feels like a high quality "recent" player to the collaboration space. Very directly focused on improving the collaboration aspects of Google Docs. In truth, after testing out the experience I'm not sold on how it's actually better than Google Docs, UX problems seem common with web editor experiences.

**Tl;DR:**
 - Feels like the most polished alternative to Google Docs, talk a lot about `Living documents` and recently released spreadsheet solution. They are doing a nice job of embedding "live" spreadsheets in documents.
 - Strong product marketing and despite offering chat are [embracing integration with slack](https://quip.com/slack). Have good use case examples for [how different teams might use Quip](https://quip.com/teams), e.g. how executives might plan their weekly meetings.
 - Biggest weakness experienced so far is that polish of the in-app experience. Building an editor is hard and it shows, not a nice editing experience (e.g. Dropbox Paper better). Ran into UX problems embedding a spreadsheet etc.
 - Strong backing and customer testimonials (e.g. Instacart, Strip, 53, …).
 - Expect they are well aware of the inertia and switching costs they face but sequencing features accordingly. Taking steps to allow users to test product out for a single project.
 - Emulating Slack signup experience, e.g. https://cased.quip.com/
 - Do offer desktop apps

## Value proposition
Documenting how the competitor positions themselves to customers.

 - **Problems being solved**
   - That people are "[drowning in email, interrupted by meetings, and getting nothing done](https://quip.com/about/why)"
   - That it's difficult to consistently document and discussion plans and decisions
 - **Core messaging**
   - `Quip is where you create, document, discuss, and organize the stuff that your team works on.`
   - `The living document`–going after the stagnation of Google Docs
   - `Quip gives executive teams the tools they need to work together, personally connect with employees, and lead by collaborative example.`
   - `Quip drives your product teams from concept to launch – brainstorm ideas, define requirements, and track projects through to completion.`
   - `Where's that file? What decision was made? Why did that project stall?`
     > Billions of dollars are spent on productivity, collaboration and communication tools for teams each year. Yet – all you do is manage your inbox and attend meetings.

## Onboarding experience
Overview of first time user experience.

| Screenshot | Notes |
|:-----------|:------|
| ![signup](https://cloud.githubusercontent.com/assets/311182/17089681/ee087702-51dd-11e6-995c-52ee68bb82bb.png)| :information_source: Key to note Slack option. |
|![slack-interface](https://cloud.githubusercontent.com/assets/311182/17089699/07e5ab68-51de-11e6-8600-05feb39040be.png)| |
|![confirm-authorization](https://cloud.githubusercontent.com/assets/311182/17089711/227067c0-51de-11e6-9ab2-f19991424d05.png)| |
|![customize-experience](https://cloud.githubusercontent.com/assets/311182/17089715/3563b422-51de-11e6-8017-2064034c6f92.png)| |
|![inertia-promo](https://cloud.githubusercontent.com/assets/311182/17089735/6a073956-51de-11e6-8426-3f0f951701e5.png)| |
|![project-created](https://cloud.githubusercontent.com/assets/311182/17089749/90a27706-51de-11e6-854a-1e96cbdda914.png)| |
|![add-to-slack](https://cloud.githubusercontent.com/assets/311182/17089775/d7f7d5ba-51de-11e6-9ba7-f600dce0c6b0.png)| |
|![example-doc](https://cloud.githubusercontent.com/assets/311182/17090132/810e1102-51e2-11e6-9188-3dd832730418.png)| |
|![products-on-offer](https://cloud.githubusercontent.com/assets/311182/17090529/23a607dc-51e6-11e6-9078-c86b68d04e89.png)| |
|![app-sales](https://cloud.githubusercontent.com/assets/311182/17090585/c5d6d43c-51e6-11e6-8b66-5fc4415ff8ad.png)| How they pitch their native desktop experience. |
|![two-workspaces](https://cloud.githubusercontent.com/assets/311182/17090668/9679c4f0-51e7-11e6-8384-f9c4f3cd77ca.png)| Has a similar concept to Slack or GitHub, personal space and that of a company. e.g. I created a personal account but also linked it to Cased Slack team. |
|![finishing-slack-setup](https://cloud.githubusercontent.com/assets/311182/17090803/0fc87c9c-51e9-11e6-8ef1-dabb83e1f457.png)| |

## Business model
Standard offering:
 - $30 per month for a team of five
 - $10 per month for each additional person
Quip Enterprise:
 - $25 per month per person
 - Include single-sign-on support
 - Dedicated account manager

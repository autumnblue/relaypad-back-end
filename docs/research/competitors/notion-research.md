# Notion research
Combines documents, tasks and wikis into one package. Four person team, investors include Naval Ravikant, First Round Capital. Seems to have [launched Sept 2015](https://www.producthunt.com/tech/notion-4), but no info in Crunchbase/Angel List.

https://www.notion.so/

| Contents |
|:---------|
| [Research summary](#research-summary) |
| [Value proposition](#value-proposition) |
| [Onboarding experience](#onboarding-experience) |
| [Business model](#business-model) |

## Research summary

**TL;DR:**
- Very open-ended platform that tries to combine docs, task lists and wikis into one environment.
- They've built a ton of front-end features which makes for a pretty cluttered UI and experience, but some interesting ideas in there.
- Not much traction yet, only 500 followers on Twitter

## Value proposition
They want to be an all-purpose app for everything outside of chat. In the marketing and onboarding, they show how to use it for product specs, sprint planning, product roadmaps, and a wiki knowledge base.

- **Problems being solved**
  - "Traditional doc/task/wiki tools are too slow"
  - "More powerful than Google Docs, with better organization" - you can link to any page, turn any content into a new page, rich embeds…
- **Core messaging**
  - "Combined with Slack, it's all you need to run a team."
  - "A unified workspace for docs, wikis, and tasks – designed for modern teams."
  - "A tool for your knowledge and thoughts"

## Onboarding experience
They've got a pretty decent onboarding flow. When you sign in via Google, you get a live site pre-populated with content to play with. A little odd that you then have to delete it to start using the tool with your own content, but I could see what Notion did pretty quickly without starting with a blank state.

| Screenshot | Notes |
|:-----------|:------|
|![marketing](https://www.dropbox.com/s/8nzhv1fwlzqbx9u/notion.png?raw=1)| Logged-out marketing page. Uses Google accounts for user admin |
|![onboarding-1](https://www.dropbox.com/s/styquztn8rugfha/notion-onboarding-1.png?raw=1)|Onboarding flow features tooltips over a live doc|
|![onboarding-2](https://www.dropbox.com/s/tsz0atefv1i8l7h/notion-onboarding-2.png?raw=1)| |
|![onboarding-3](https://www.dropbox.com/s/sfi5xe44vqudtaz/notion-onboarding-3.png?raw=1)| |
|![onboarding-4](https://www.dropbox.com/s/y3q1nbrphtd9um6/notion-onboarding-4.png?raw=1)| |
|![onboarding-5](https://www.dropbox.com/s/rvv5n9iuj8hyhdf/notion-onboarding-5.png?raw=1)| |
|![onboarding-6](https://www.dropbox.com/s/0uekvxuxozkw75m/notion-onboarding-6.png?raw=1)| |
|![onboarding-7](https://www.dropbox.com/s/ldufcbdugv3wj5h/notion-onboarding-7.png?raw=1)| |
|![onboarding-8](https://www.dropbox.com/s/ttscrpvu09xs3tf/notion-onboarding-8.png?raw=1)| |
|![sidebar](https://www.dropbox.com/s/u4hx2ftl4gsfb8d/sidebar.png?raw=1)|Sidebar that lets you drag various elements into a page|
|![roadmap](https://www.dropbox.com/s/k8jmouiy8rqxy2h/roadmap.png?raw=1)|Sample roadmap doc|
|![tasks](https://www.dropbox.com/s/107ki3ste42st90/tasks.png?raw=1)|Sample task list|
|![product-spec](https://www.dropbox.com/s/i1b20l8aw1p02j7/product_spec.png?raw=1)|Sample product spec|
|![document](https://www.dropbox.com/s/juyt30fgcjlxln6/doc.png?raw=1)|Sample document|

## Business model
- Free - Up to 5 active collaborators, 5MB per file upload limit, 500 Slack notifications
- $5/user/month - unlimited file size for uploads and unlimited Slack notifications

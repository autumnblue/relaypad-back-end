# Lattice research
Lattice received funding from the Slack Fund, focused on encouraging startups to integrate with the service. It received attention in [this article](https://medium.com/slack-developer-blog/investing-in-the-future-of-work-d4ffb87f985#.e7zyhrlub), with the description:

> helps you establish goals, OKRs (Objective and Key Results) weekly check-in and continuous feedback with your Slack team.

| Contents |
|:---------|
| [Research summary](#research-summary) |
| [Value proposition](#value-proposition) |
| [Onboarding experience](#onboarding-experience) |
| [Business model](#business-model) |

## Research summary

**TL;DR:**
 - Going after a niche other slack integrators are not talking about, goals and OKRs
 - Falls victim to the same problems of entire team needs to adopt tool
   - e.g. there's a `people` section to product, not sure how that is easily populated or maintained
 - Each goal has an activity feed, without tight third-party Slack integration not sure how that's useful
 - `Check-ins` section geared towards bringing structure to weekly manager checkins
 - `Insights` section includes some interesting progress tracking

## Value proposition
Documenting how the competitor positions themselves to customers.

 - **Problems being solved**
   - That it's difficult for companies and teams to stay aligned around goals
   - That goal progress tracking gets stale quickly
   - That often times there are no central places for goal tracking
 - **Core messaging**
   - `Performance management made easy`
   - `Meet your goals`
   - `Align your company`
   - `Aim higher together`

## Onboarding experience
Overview of first time user experience.

| Screenshot | Notes |
|:-----------|:------|
|![home](https://cloud.githubusercontent.com/assets/311182/17079856/de71b19e-50d1-11e6-878c-816513d3ea81.png) | |
|![pricing](https://cloud.githubusercontent.com/assets/311182/17079893/b83ac30c-50d2-11e6-9167-0f34b3b78f26.png)| |
|![example-company-goals](https://cloud.githubusercontent.com/assets/311182/17079867/3569efc0-50d2-11e6-96fe-45775fb0e001.png)|There's an example instance that can be browsed before user has to create their own content.|
|![create-first-goal](https://cloud.githubusercontent.com/assets/311182/17079861/10e49812-50d2-11e6-80c9-657eef0a735b.png) ||
|![metrics](https://cloud.githubusercontent.com/assets/311182/17079878/5b86da38-50d2-11e6-9b6f-0a4a03cdfcb9.png)|Some generic progress metrics are shared. Again it's a stretch to think these are going to be useful unless entire company is updating plus third-party info is imported. |

## Business model
Off the bat there just doesn't feel like enough value add here to merit a company spending, basically more than they do for GitHub to track these goals.

 - $8/user/month (billed annually)
   - $10 per user billed monthly
 - Custom enterprise pricing available

# Flow research
A look at [Flow](https://www.getflow.com/), a player in the project and task management space, albeit a lesser known player.

| Contents |
|:---------|
| [Research summary](#research-summary) |
| [Value proposition](#value-proposition) |
| [Onboarding experience](#onboarding-experience) |
| [Business model](#business-model) |

## Research summary

**TL;DR:**
 - Trying to be a combination of Slack, JIRA and Google Calendar
 - Difficult to differentiate this from Asana
 - Yet another tool that requires complete adoption from teams. It tries to do too much (e.g. offers chat solution) and will be held back by the inertia of teams not wanting to add a completely new tool to their workflow.

Flow have been around since 2011 ([initial press coverage](http://thenextweb.com/apps/2011/03/22/say-hello-to-flow-probably-the-most-beautiful-task-management-app-yet/#gref)) and was initially focused on small businesses, a redesign in 2013 allowed it to target larger companies and now boasts some "major organizations" as customers (e.g. Adobe and Tesla). A detailed review of the product is available [here](https://www.merchantmaverick.com/reviews/flow-review/). A consistent theme in positive reviews focuses on Flow's design and ease of use.

The onboarding experience of Flow can lead to immediate comparisons to other project management tools, such as a [Asana](https://asana.com). They are definitely trying to be another tool in the workflow and suffer from chicken-in-egg problems, meaning that all staff need to commit to the tool for it to be effective. On an individual basis it could be used as a task list manager but suspect users will opt for alternatives.

## Value proposition
Documenting how the competitor positions themselves to customers.

 - **Problems being solved**
   - Task management
   - Project management
 - **Core messaging** *Generic market speak used*
   - `Not just project management. Mission control`
   - `Visualize entire projects with Kanban Boards`
   - `Plan ahead with calendars`

## Onboarding experience
Overview of first time user experience.

| Screenshot | Notes |
|:-----------|:------|
|![marketing-page](https://cloud.githubusercontent.com/assets/311182/17066897/021eaba8-4ffe-11e6-80d3-161504c4ef74.png) ||
|![complete-profile](https://cloud.githubusercontent.com/assets/311182/17066928/25ec743e-4ffe-11e6-83b4-67663e9e4664.png) ||
|![team-setup](https://cloud.githubusercontent.com/assets/311182/17066948/4890ecd6-4ffe-11e6-8de7-da5f0d7b9453.png)||
|![dashboard](https://cloud.githubusercontent.com/assets/311182/17066977/6d6f0d80-4ffe-11e6-9ca6-b6026b4ff1c7.png)||

## Business model
 - 15 day free trial
 - Starter plan of $19/month (for up to 3 peope)
 - Corporate plan of $399/month (for up to 50 people)

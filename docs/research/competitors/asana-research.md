# Asana research
Founded in 2009 and has received [$88.2M in funding so far](https://www.crunchbase.com/organization/asana#/entity), latest round a series-C in March 2016. Certainly one of the highest profile early stage project management services but hasn't achieved breakout success. Terms of series-C raise some suspicion that company can't breakout and get on IPO path. Additionally [a major rebrand](https://blog.asana.com/2015/09/the-new-asana/) was completed in 2015.

> Asana is the easiest way for teams to track their work.

>…

> Asana was started after Dustin Moskovitz and Justin Rosenstein built an internal tool at Facebook that changed how the company coordinated work. They realized this technology could help every organization achieve their goals. Now, Asana is a team of over 200 peers across three offices in SF, NYC, and Dublin, with investors including Benchmark Capital, Andreessen Horowitz, Founders Fund, and Peter Thiel.

| Contents |
|:---------|
| [Research summary](#research-summary) |
| [Value proposition](#value-proposition) |
| [Onboarding experience](#onboarding-experience) |
| [Business model](#business-model) |

## Research summary
Despite a lot of hype and good traction with some companies it feels like Asana is just another player in an already crowded space. As with many tools in the space, constantly battling inertia when trying to acquire new customers but it does help them to retain teams.

**TL;DR:**
 - Most succinct description of service is that it's a collaborative task list manager
 - Continues to come across as overly complex with a steep learning curve
 - Motivation for [rebrand in 2015](https://blog.asana.com/2015/09/the-new-asana/) was to simplify experience
 - Another tool that falls victim to bulk of value only being delivered if entire company 100% commits to tool
   - One pitch includes: `Never use an internal mailing list again.`

## Value proposition
Documenting how the competitor positions themselves to customers.

 - **Problems being solved**
   - That team collaboration and project management remains inconsistent and complicated
 - **Core messaging**
   - `Asana is the easiest way for teams to track their work–and get results`
   - `Track projects from start to finish`
   - `Progress at a glance`–burndown charts
   - `Make ideas happen`–conversations in product can be turned into tasks easily

## Onboarding experience
Overview of first time user experience.

| Screenshot | Notes |
|:-----------|:------|
|![home](https://cloud.githubusercontent.com/assets/311182/17085955/3c5b5e68-519a-11e6-90e5-e92f97d8ab49.png)| |
|![dashboard](https://cloud.githubusercontent.com/assets/311182/17086192/c38ef5ce-51a0-11e6-977f-ee49db8463d5.png)||

## Business model
 - Free for teams up to 15
 - Premium product: $8.33/user/month (billed annually)
   - Admin controls
   - Single-sign-on support
   - … features geared at Enterprises

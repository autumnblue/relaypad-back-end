# Huddle research
Huddle is on shaking ground (per grapevine) but pitches itself as "secure cloud collaboration" and wanted to capture some notes.

| Contents |
|:---------|
| [Research summary](#research-summary) |
| [Value proposition](#value-proposition) |
| [Onboarding experience](#onboarding-experience) |
| [Business model](#business-model) |

## Research summary

**TL;DR:**
 - Tried to go after cloud collaboration by augmenting office with Box and Dropbox type features
 - Feels like a jack of all trades and hard to imagine long term success (e.g. Box with project management)
 - On shaking ground, per internal connection, could generate leads but couldn't close deals. Focused on "government and enterprise".

## Value proposition
Documenting how the competitor positions themselves to customers.

 - **Problems being solved**
   - That collaboration across teams at large enterprises should be easier
   - That regulated industries should have Dropbox type experiences
 - **Core messaging**
   - `Work better together`
   - `Secure cloud collaboration for government and enterprise`
   - `Huddle manages files, tasks and team communication in one place`

## Onboarding experience
Generic marketing content...

![homepage](https://cloud.githubusercontent.com/assets/311182/17080070/ac8313de-50d7-11e6-877a-3f97335c6acd.png)

## Business model
Was definitely focused on larger enterprise companies and regulated government industries.
 - Listing pricing started at $20/user/month (wow :scream:)
 - Custom pricing for government and public sector companies

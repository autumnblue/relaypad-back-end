# [P2Theme from Wordpress](https://p2theme.com/)
> Blogging at the speed of thought

Renowned as a great tool for internal communications at Wordpress.

## Onboarding experience
| Screenshot | Notes |
|:-----------|:------|
| **Landing** ![render](https://cloud.githubusercontent.com/assets/311182/19131338/a7b86e68-8b14-11e6-92b8-7f593adbb078.png) | Generic landing page nudging user to write first post. Item that jumped out immediately was asking the user to confirm what type of content they wanted to post (e.g. link or blog). |
| **Some content** ![render](https://cloud.githubusercontent.com/assets/311182/19131579/91c54058-8b15-11e6-8293-5fa501bcae5b.png) | Interesting to see some content rendered. |
| **Stats** ![render](https://cloud.githubusercontent.com/assets/311182/19131826/9d27eb0c-8b16-11e6-8b61-cc4d515e0848.png) | Some interesting stats to consider once some content is attained. |
| **Admin settings** ![render](https://cloud.githubusercontent.com/assets/311182/19131856/bce69a6a-8b16-11e6-92c2-23752599efb2.png) | Cluster f**k of settings... |


### Product Documentation Jobs

| Role | Job Story | Current Solution | Satisfaction (1-5) | Importance (1-5) | Notes |
| ---- |---------- | ---------------- |  ----------------- | ---------------- | ----- |
| PM | Document the requirements for a product feature before development begins | Google Doc, PowerPoint, wiki | 3 | 5 | Document creation is ok today, but still lots of room for improvement (templates, ease of formatting, etc) |
| PM | Socialize a project's requirements before and during development | PowerPoint, email, meetings | 2 | 5 | |
| Team leadership | Approve a project's requirements during relevant check-ins before release | meetings | 4 | 3 | |
| PM | Collect input from relevant stakeholders on a product spec before it's finalized | Google Doc comments, email, meetings, slack | 2 | 4 | |
| PM | Keep an archive of lessons learned during design, development, and rollout of a new feature  | wiki, email, post-mortem meeting | 3 | 3 | Not a technical problem, more of a cultural problem |
| PM | Keep product spec up to date during development and post-launch | Google Doc, wiki, email | 1 | 5 | |
| Developer | See in-progress product specs and have an opportunity to contribute feedback  | Google Doc, PowerPoint, wiki | 2 | 4 | |
| Developer | Remember why a product decision was made in the past (e.g. _"Why did we rate limit this API service when we wrote it in 2012?"_) | wiki, code comments, finding who wrote the original code and hoping they remember | 2 | 3 | |
| PM | Share results of experiments, rollouts, or new feature launches  | Analytics dashboards, PowerPoint, meetings, email | 3 | 3 | |
| Marketer | Understand a new feature before marketing efforts begin  | email, meetings | 3 | 2 | |
| Coworker | Understand what everyone else is working on  | meetings, project management software | 2 | 2 | |
| Coworker | Understand who contributed to a specific project  | product spec documentation, git commits | 2 | 2 | |
| Developer | Have an understanding of the rationale behind product and business requirements for a project (e.g. _why a feature was removed from scope, constraints that drove certain decisions, etc._ ) | email, finding who wrote the original code and hoping they remember | 1 | 2 | |
| New employee | Learn history behind product decisions, best practices, coding standards, etc. | wiki, slack, reading the codebase | 2 | 3 | |

### Company News Jobs

| Role | Job Story | Current Solution | Satisfaction (1-5) | Importance (1-5) | Notes |
| ---- |---------- | ---------------- |  ----------------- | ---------------- | ----- |
| Team leadership | Distribute information to the team efficiently | intranet, email, slack, meetings | 4 | 5 | [Wordpress uses an internal blog system called P2](https://ma.tt/2009/05/how-p2-changed-automattic/), Github uses Team (@irishbryan can confirm) |
| Team member | Have a consistent place to find important news  | intranet, email, slack | 2 |  3 | |
| Team member | Have the ability to react to company news with questions & comments | email, slack | 3 | 2 | |
| New employee | Learn company history, culture, norms, etc. | Google Doc, "Welcome to Company X" email, meetings | 3 | 3 | |

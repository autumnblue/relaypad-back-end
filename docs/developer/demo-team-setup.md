# Demo Team Setup
To facilitate testing and demonstrations we have the ability to create a demo team across environments. Fun premise is that the team is `Delos Destinations`, the fictional company behind `\W/estworld`.

## Bootstrapping environment
A class capable of creating a demo team, and initial members is located in `core.utils.seeder` module. To generate team, follow these instructions.

 - Add `DEMO_MEMBER_PASSWORD` to `/.env` file (should not be committed to source control)
   - Value for staging can be accessed in heroku config variables.
 - From a terminal with `env` activated, type `script/console`
 - Then:
   - `from core.utils import seeder`
   - `seeder = seeder.Seeder()`
   - `seeder.create_westworld()`

Output should be a team with slug `delos` and a number of members, list of which can be seen within django admin site.

## Demo content
:construction: ...

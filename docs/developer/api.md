# API reference
RelayPad clients connect to the back-end through a REST API (facilitated by [Django Rest Framework](http://www.django-rest-framework.org/) (DRF)). This documents provides an overview of endpoints that are in use. All endpoints listed here assume a prefix of `/api/<api-version>` and source for API can be found in [relaypad core django application](/relaypad/core).

:information_source: Ultimate source of truth is [`urls.py`](/relaypad/core/urls.py) and [Browsable API from DRF](http://www.django-rest-framework.org/topics/browsable-api/) (only available in non-production environments, dev version [here](http://localhost:8000/api/v1/) if server running ) but goal of this document is to provide initial orientation.

| Content |
|:--------|
| [Authentication](#authentication) |
| [Teams](#teams) |
| [Members](#members) |
| [Posts](#posts) |
| [Tags](#tags) |
| [Comments](#comments) |
| [Bookmarks](#bookmarks) |
| [Subscriptions](#subscriptions) |
| [Notifications](#notifications) |
| [Pagination](#pagination) |
| [Permissions](#permission) |

## Authentication
Auth is offloaded to [Auth0](https://auth0.com), which is responsible for redirecting user to `Sign in with Slack` flow and providing the client with a JSON Web Token (`JWT`) that will be used to authenticate user on API side. All API requests pass through `RelayPadJWTAuthentication`, which decodes JWT and populates request with necessary membership information.

## Teams
| Endpoint | Method | Status | Notes |
|:---------|:-------|:-------|:------|
| `/teams` | GET, POST | :white_check_mark: | Retrieve or create team, which current user is/will be an active member of. |
| `/teams/:slug` | GET, PUT, PATCH, DELETE | :white_check_mark: | Retrieve, update or delete specified team. |

## Members
| Endpoint | Method | Status | Notes |
|:---------|:-------|:-------|:------|
| `/members` | GET | :white_check_mark: | Return list of members for current team |
| `/members/current` | GET | :white_check_mark: | Retrieve critical information about current member based on auth_token. |
| `/members/:slug` | GET, PUT, PATCH, DELETE | :white_check_mark: | Retrieve information about specified user (for profile pages). |

## Posts
| Endpoint | Method | Status | Notes |
|:---------|:-------|:-------|:------|
| `/posts` | GET, POST | :white_check_mark: | Retrieve posts or create post for current team. |
| `/posts/:slug` | GET, PUT, PATCH, DELETE | :white_check_mark: | |
| `/drafts` | GET | :white_check_mark: | Returns all draft posts for current member. |

#### Available post filters
| Filter | Parameters | Status | Notes |
|:-------|:-----------|:-------|:------|
| Tag | `?tag=<tag-slug>` | :white_check_mark: | Retrieve all published posts with specified tag. |
| Author | `?author=<username>` | :white_check_mark: | Retrieve all published posts written by specified author. |

## Tags
| Endpoint | Method | Status | Notes |
|:---------|:-------|:------|:------|
| `/tags` | GET | :white_check_mark: | Retrieve all tags in use within current team. |
| `/tags/:slug` | GET, PUT, PATCH, DELETE | :white_check_mark: | Retrieve, update or delete a specific tag. |

## Comments
| Endpoint | Method | Status | Notes |
|:---------|:-------|:-------|:------|
| `/posts/:slug/comments` | GET, POST | :white_check_mark: | Retrieve comments or create comments for specified post. |
| `/comments/:id` | GET, PATCH, DELETE | :white_check_mark: | Retrieve, update or delete specified comment. |

## Bookmarks
| Endpoint | Method | Status | Notes |
|:---------|:-------|:-------|:------|
| `/bookmarks` | GET | :white_check_mark: | Retrieve all bookmarks for current user. |
| `/posts/:slug/bookmark` | POST | :white_check_mark: | Current user bookmarks specified post. |
| `/posts/:slug/bookmark` | DELETE | :white_check_mark: | Remove bookmark for specified post. |

## Subscriptions
| Endpoint | Method | Status | Notes |
|:---------|:-------|:-------|:------|
| `/subscriptions` | GET | :white_check_mark: | Retrieve all subscriptions for current user. |
| `/tags/:slug/subscribe` | POST | :white_check_mark: | Subscribe current user to specified tag. |
| `/tags/:slug/subscribe` | DELETE | :white_check_mark: | Unsubscribe current user from specified tag. |

## Reactions

:warning: Using PUT for reaction removal since we need to pass reaction type in body and DELETE doesn't support that. :warning:

| Endpoint | Method | Status | Notes |
|:---------|:-------|:-------|:------|
| `/posts/:slug/react` | POST, PUT | :white_check_mark: | Create or delete specified reaction on post, expects reaction `type`. |
| `/comments/:id/react` | POST, PUT | :white_check_mark: | Create or delete specified reaction on comment, expects reaction `type`. |

## Pagination
All list routes are returning [limit offset based pagination](http://www.django-rest-framework.org/api-guide/pagination/).

## Permissions

| Endpoint | Method | Team Member | Content Owner |
|:---------|:-------|:------------|:--------------|
| `/teams` | GET | :white_check_mark: | `n/a` |
| `/teams` | POST | :x: | :x: |
| `/teams/:slug` | PUT, PATCH | :white_check_mark: | `n/a` |
| `/teams/:slug` | DELETE | :x: | `n/a` |
| `/members` | GET | :white_check_mark: | `n/a` |
| `/members` | POST, PUT, PATCH, DELETE | :x: | `n/a` |
| `/members/current` | GET | :white_check_mark: | `n/a` |
| `/members/:username` | GET | :white_check_mark: | `n/a` |
| `/members/:username` | POST, PUT, PATCH | :x: | :white_check_mark: |
| `/members/:username` | DELETE | :x: | :x: |
| `/posts` | GET | :white_check_mark: | `n/a` |
| `/posts` | POST | :white_check_mark: | `n/a` |
| `/posts/:slug` | GET | :white_check_mark: | `n/a` |
| `/posts/:slug` | POST, PUT, PATCH | :x: | :white_check_mark: |
| `/posts/:slug` | DELETE | :x: | ✅  |
| `/posts/:slug/bookmark` | POST | :white_check_mark: | `n/a` |
| `/posts/:slug/bookmark` | DELETE | :white_check_mark: | `n/a` |
| `/posts/:slug/react` | POST | :white_check_mark: | `n/a` |
| `/posts/:slug/react` | PUT | :white_check_mark: | `n/a` |
| `/posts/:slug/comments` | GET | :white_check_mark: | `n/a` |
| `/posts/:slug/comments` | POST | :white_check_mark: | `n/a` |
| `/tags` | GET | :white_check_mark: | `n/a` |
| `/tags` | POST | :white_check_mark: | `n/a` |
| `/tags/:slug` | GET | :white_check_mark: | `n/a` |
| `/tags/:slug` | POST, PUT, PATCH | :x: | `n/a` |
| `/tags/:slug` | DELETE | :x: | `n/a` |
| `/tags/:slug/subscribe` | POST | :white_check_mark: | `n/a` |
| `/tags/:slug/subscribe` | DELETE | :white_check_mark: | `n/a` |
| `/tags/:slug/subscribers` | GET | :white_check_mark: | `n/a` |
| `/comments` | GET | :white_check_mark: | `n/a` |
| `/comments/:id` | GET | :white_check_mark: | `n/a` |
| `/comments/:id` | DELETE | :x: | :white_check_mark: |
| `/comments/:id/react` | POST | :white_check_mark: | `n/a` |
| `/comments/:id/react` | PUT | :white_check_mark: | `n/a` |
| `/bookmarks` | GET | :white_check_mark: | `n/a` |
| `/drafts` | GET | :white_check_mark: | `n/a` |
| `/subscriptions` | GET | :white_check_mark: | `n/a` |
| `/notifications` | GET | :white_check_mark: | `n/a` |
| `/notifications` | PUT | :white_check_mark: | `n/a` |

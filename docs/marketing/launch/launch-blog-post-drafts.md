# Launch blog post drafts
Exercise to draft launch blog posts before development began.

| Contents |
|:---------|
| [@marktron's draft](#marktrons-draft) |
| [@irishbryan's draft](#irishbryans-draft) |

## @marktron's draft

### A Modern Publishing Platform for Your Team

If you work on a team, you get cc'd on emails. A lot of emails. Right now in your inbox, there might be a company-wide announcement from HR about the new vacation policy. An Excel spreadsheet from your analytics team with results of a recent A/B experiment. An ongoing email thread among team members debating the specifics of a project you aren't even directly involved with. And don't forget the countless emails you filed away _just in case_ you need to reference them at some point in the future. The inbox has become your team's shared knowledge base, and it's not doing a great job keeping up with everything.

Blueprint makes your team's internal communication more effective by making it more findable, shareable and collaborative.

#### A Platform Built for Participation
Unlike most services built for enterprise or team use, publishing on Blueprint is actually an enjoyable experience. Use your favorite text editor or Blueprint's advanced built-in editor to create clear, beautiful documents that are shared instantly with the people who need to see them most. Commenting and feedback options take discussion out of closed email threads and engage a larger team, ensuring that everyone is on the same page.

#### Create a Lasting Home for Your Team's Announcements and Documents
When a new team member joins or someone switches rolls, it's easy to get up to speed since every article published to Blueprint is available to your team. Your team's Blueprint is secure and private, and viewable only by people already on your Slack team.

#### Works Great with Your Existing Tools and Workflows
Blueprint integrates seamlessly with Slack and most popular project management software to provide context across apps. Personalized email digests of Blueprint activity are available and mobile apps for iOS and Android make it possible to stay up to date wherever your team works.

Blueprint was designed for teams of all sizes, and is even more powerful if you have a departments or groups who don't always get a lot of day-to-day interaction. Using Blueprint to share your team's weekly status updates, launch announcements, product requirements, new hire introductions, or anything else you previously would have previously sent a group email for. You'll find your whole company more informed and engaged, and spending less time digging through their email archives.

----

## @irishbryan's draft

*Took more of a press release format*

SAN FRANCISCO, `<date>` -- Blueprint today announced the launch of its internal communications publishing platform, aimed at simplifying how companies publish and consume longer form internal communications, such as company news, product roadmaps and employee handbooks. While many advances have been made to support the authoring of content, such as real-time collaboration in Google Docs, little has changed about how the internal communications are published. Critical communications are often shared in ephemeral ways, such as email, lowering reader engagement and making future referencing difficult. Blueprint brings consistency, transparency, and discoverability to internal communications, improving publishing and reading for leaders and employees alike. Blueprint aims to be a natural complement to Slack’s messaging platform, allowing users to access content using their Slack credentials and integrating with the platform to drive discussions about internal communications.

“Inconsistent internal communications -- via email, wikis and intranets -- at companies we’ve previously worked motivated us to create Blueprint. We’ve seen first hand how poor internal communication can negatively impact team output and morale, particularly as teams become more distributed across geographies and time zones”, said Bryan Byrne and Mark Allen, Blueprint’s co-founders. “We believe company communications should have a permanent home and that authors and readers should be able to transparently engage with each other. Leaders and employees now have a better way to openly discuss critical announcements and projects”, Byrne and Allen added.

Blueprint is designed to integrate seamlessly with existing workflows. For example, an executive team may collaborate on a company announcement in Microsoft Office 365 before deciding to publish it on Blueprint. Once published, the appropriate Slack channels can be notified and team members will be able to read and engage with the announcement using both web and mobile app experiences. Blueprint features include:

 - Simple publishing flow, which works well with existing authoring workflows.
 - Reader engagement features, such as commenting, liking and following.
 - Team sections, so employees can stay apprised of department news most pertinent to them.
 - Ability to search entire archive of internal communications.
 - Slack integration, for both user authentication and notification.
 - Engagement monitoring, so that publishers can improve future internal communications.

Blueprint’s business follows a traditional SaaS (Software as a Service) model, with teams paying $25 per month for the first 25 team members and an additional $2 per month for each additional team member. Teams new to the service can try out the service for 7 days free of charge.

#### About Blueprint
Blueprint was created in September 2016, with the goal of improving how internal communications and published and consumed. Blueprint was started by technology industry veterans Bryan Byrne and Mark Allen, who met while working together during Yelp’s earlier stages. Byrne has also worked in product and engineering roles at GitHub, Zillow and Hotwire, while Allen has worked in product and design roles for MyFitnessPal and Ancestry. Byrne and Allen have funded Blueprint themselves to this point.

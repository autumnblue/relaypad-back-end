# Member onboarding
Documenting how we'll successfully onboard a new member of a team.

:information_source: Prerequisite for this flow is that another member has already created team and written a `Welcome to RelayPad` post, per [team onboarding spec](./team-onboarding.md).

| Content |
|:--------|
| [Goals](#goals) |
| [Overview](#overview) |
| [Emails](#emails) |

## Goals
 1. User reads/comments/reacts to `Welcome to RelayPad` post
 2. User writes a post -or- completes `Add to Slack` flow for team

## Overview
This is spec is output of discussion at SF May 2017 meetup. Flow will be:

 1. User clicks link to RelayPad
 1. User completes `Sign in with Slack` flow
 1. User lands on `Welcome to RelayPad` post
    - Possible they land on dashboard but more likely that they come to RelayPad because of a link shared by first user.
 1. User reads post
 1. User presented with `Write first post` and `Add to Slack` promotions
 1. Ideally we'll also be able to enourate user to read RelayPad blog content

Supporting white board content follows:

![render](https://cloud.githubusercontent.com/assets/311182/25914319/c332ff1a-3572-11e7-8f23-6287e8dbaa2d.png)

## Emails
We will send two types of emails to members:

 1. `Member welcome`
   - Sent immediately following Slack auth. Should act as confirmation to sign up and include educational content about `Why RelayPad`.
 2. `Onboarding series`
   - We will create compelling content about why using RelayPad `to gather and deliver team news` will improve company transparency and execution.
   - Possible this content will be included in `Daily Briefing` email.

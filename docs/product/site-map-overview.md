# Site-map overview
Overview of the various views and URLs RelayPad will consist of.

| Contents |
|:---------|
| [Marketing](#marketing) |
| [Application](#application) |

## Marketing
| View | URL | Notes |
|:-----|:----|:------|
| Home | `/` | | |
| Use-cases | `/<use-case>` | Landing page to elaborate on how various teams can use RelayPad, e.g. `/human-resources` |
| Features | `/features` | |
| Pricing | `/pricing` | |
| Blog | `/blog` | Likely should invest in creating content that shows our subject matter expertise in the area of internal communications and where we believe it is going. |
| Help | `/help` | General |
| Privacy | `/privacy-policy` | |
| Terms of service | `/terms-of-service` | |

## Application

### Basic

| URL | Notes |
|:----|-------|
| `/signin` | |
| `/signout` | |

### Team specific
Views tied to core experience for a specific team.

| URL | Notes |
|:----|-------|
| `/<team-slug>` | Default landing view for an authenticated user, displays feed of posts and facilitates exploration (tags). |
| `/<team-slug>/post/<post-slug>` | Published post detail page. |
| `/<team-slug>/tag/<tag-slug>` | Listing of all posts matching specific tag. |
| `/<team-slug>/tags` | Listing of all tags active for team. |
| `/<team-slug>/members` | Ability to view all members of a given team, along with some meta data (maybe org structure). |
| `/<team-slug>/@<username>` | Profile for a specific team member. |
| `/<team-slug>/admin` | Admin specific area for team, only available to team admins. |
| `/<team-slug>/admin/<area>` | Display and allow changes to specific team level settings (e.g. billing information). |
| `/<team-slug>/account` | Member specific settings for a given team (e.g. ability to change profile picture or notifications). |
| `/<team-slug>/stats` | :thought_balloon: Stats for a given team. |

### User specific

| URL | Notes |
|:----|-------|
| `/profile/` » `/<team-slug>/<@username>` | Current member profile. |
| `/posts` » `/<team-slug>/posts` | Current user's posts for current team. |
| `/drafts` » `/<team-slug>/drafts` | Current user's drafts for current team. |
| `/search` » `/<team-slug>/search?q=query` | Search results for current user's current team. |
| `/bookmarks` » `/<team-slug>/bookmarks` | Current user's bookmarks for current team. |
| `/notifications` » `/<team-slug>/notifications` | Notifications associated with current user's current team. |
| `/new-post` » `/<team-slug>/new-post` | New post form for current team, redirects to `/<team-slug>/post/:id/edit` once record created and added in draft mode. |
| `/<team-slug>/post/:id/edit` | Provides ability to edit posts. |

# Slack Integration
Documentation on how RelayPad will become more integrated with Slack.

| Contents |
|:---------|
| [Problem Summary](#problem-summary) |
| [Schema Changes](#schema-changes) |
| [1.0 Features & Requirements](#10-features--requirements) |
| [Research](#research) |
| [Future Ideas](#future-ideas) |

## Problem Summary
To drive adoption we need to drive sharing and consumption of RelayPad content within a team's Slack experience. As outlined in [road to 10 customers](https://beta.relaypad.com/relaypad/post/road-to-10-customers):

> Our biggest competitor will be inertia around how things currently work, sending mass emails or simply not documenting meaningful news.

## Schema Changes
To enable Slack API calls we need to store additional slack information about teams and members. We'll also import additional member information (e.g. their timezone).

 - `Team` model changes
   - Add `slack_id` column
   - Add `slack_bot_token` column
 - `Member` model changes
   - Add `slack_id` column
   - Add `timezone` column
   - Add `timezone_label` column
   - Add `timezone_offset` column
   - Add `is_slack_admin` column

## 1.0 Features & Requirements
Our initial Slack integration will focus on the following deliverables:
 1. [Unfurling](https://api.slack.com/docs/message-link-unfurling) RelayPad URLs posted in Slack messages.
 1. Allow authors to automatically post to Slack channel when publishing.
 1. Retrieve additional team and member information from Slack

| Sections |
|:---------|
| [Unfurling URLs](#unfurling-urls) |
| [Publishing Flow Integration](#publishing-flow-integration) |
| [Retrieve Team and Member Information](#retrieve-team-and-member-information) |
| [Permission Scopes](#permission-scopes) |
| [Event API Subscriptions](#event-api-subscriptions) |

### Unfurling URLs
Slack [recently released the ability to customize unfurls for any link in Slack](https://medium.com/slack-developer-blog/all-will-be-revealed-ebcad7c531f0):

> With app unfurls, you can add context to links with message attachments and facilitate micro-workflows using message buttons, turning links into an opportunity to give people the best possible experience with your app.

This is a nice development for us, since unfurling RelayPad content requires authentication, something that is now supported:

> Our events API will send you an event which you can act on by kicking off an OAuth path for your app to recognize the link sharer as a user.

##### Event Sequence
 - Slack Events API will send RelayPad a `link_shared` event
 - RelayPad will `200` acknowledge that event
 - Using the information provided, RelayPad will call `chat.unfurl` to attach custom unfurl behavior to the links provided.

### Publishing Flow Integration
This feature will allow authors to automatically share a post they publish to Slack channel(s) of their choice.

```cucumber
Given author is viewing publish dialog
 And post is currently in draft status
 And RelayPad has necessary permissions to post as user to any of their channels
When user clicks publish
 And user has selected a Slack channel to automatically share post with
Then post a message to Slack channel as user that includes link to post
 And confirm that post is unfurled
```

### Retrieve Team and Member Information
The `Sign in with Slack` flow gives us limited identity information. We need additional permissions, namely `users:read`, `team:read`, to really leverage Slack as our identity provider. For example, we want to send our `Daily Briefing` email to users at 08:00 in the morning and can get the user's time zone from Slack.

##### Initial Data Retrieval
Once a team member completes `Add to Slack` flow, granting us the required permission, we should kickoff an asynchronous task to update team and member information.

  - Additional Team Information
    - No op here since `Sign in with Slack` flow's `identity.team` scope gives us all the team information we need.
  - Member Information
    - Slack API's `users.list` method will return a listing of all team members, uniquely identified by combination of `id` and `team_id` values, and we'll persist following data for all members returned:
      - "Actual username", returned in `name` field (something `Sign in with Slack` doesn't :frowning:)
      - `tz`
      - `tz_label`
      - `tz_offset`
      - `is_admin`: allow us to surface calls to actions to Slack admins if/when we need changes to our Slack integration.

:information_source: We'll create RelayPad member entries for all Slack team users, even if they haven't logged into RelayPad yet. This will enable us to more accurately report on readership stats in the future, and provide `@-mention` support etc.

##### Keeping Data In-Sync
The [Slack Events API](https://api.slack.com/events-api) allows us to subscribe to events that inform us of changes to either team or member information, they are `team_rename`, `team_domain_changed` and `user_change`. If we receive either of those events, we will make an explicit call to get all information for the respective team or member, and update our information accordingly.

:warning: We may punt on supporting `team_rename` and `team_domain_changed` since that would result in us having to `301` redirect URLs that pointed to the old team information.

### Permission Scopes
Listing of [`Permission Scopes`](https://api.slack.com/docs/oauth-scopes) that RelayPad will request and their purpose.

| Scope | Event(s) Access | Method(s) Access | RelayPad usage |
|:------|:----------------|:-----------------|:---------------|
| `users:read` | `team_join` | `users.list`, `users.info`, `users.getPresence` | Access and monitor user information. May not use `users.getPresence` initially, but will be useful. |
| `team:read` | `team_domain_changed`, `team:rename` | | Access and monitor team information. |
| `channels:read` | | `channels.list` | Get a list of channels, simplifying author's ability to share post. |
| `chat:write:bot` | | `chat.postMessage` | Send daily briefing to users. |
| `links:read` | `link_shared` | | Monitor for URLs to unfurl. |
| `links:write` | | `chat.unfurl` | Unfurl RelayPad URLs mentioned in messages. |

### Event API Subscriptions
Slack offers developers at [Events API](https://api.slack.com/events-api), "a streamlined, easy way to build apps and bots that respond to activities in Slack". To make use of it, we need to subscribe to the events that we care about. We can subscribe to `all or none` of the events to which we have access to, with access governed by the OAuth scope system.

> For example, if your app has access to files through the files:read scope, you can choose to subscribe to any or none of the file-related events like file_created and file_deleted.

Here's a guide of the events we will be interested in, either at launch or in the not to distant future. While we can probably image use cases for all events listed, we'll limit our scope requests to features we are likely to release in the near to medium term. We want to avoid users becoming paranoia about RelayPad if we request too many permissions. Events for which we have a short-term interest include:

| Event | Trigger | Scope Required | RelayPad Action |
|:------|:--------|:---------------|:----------------|
| [`link_shared`](https://api.slack.com/events/link_shared) | A message containing one of more links registered to our app was posted. | `links:read` | We'd use this event to send unfurl data back to Slack. |
| [`team_domain_changed`](https://api.slack.com/events/team_domain_change) | The team domain has changed. | `team:read` | Should attempt to update RelayPad team information. |
| [`team_join`](https://api.slack.com/events/team_join) | A new team member has joined | `users:read` | Opportunity for us to welcome new team member and provide them with easy access to historical archives and best posts. |
| [`team_rename`](https://api.slack.com/events/team_rename) | The team name changed | `team:read` | Update team information. |
| [`user_change`](https://api.slack.com/events/user_change) | A team member's data has changed | `users:read` | Refresh users data from Slack (e.g. new profile picture). |

## Research
Slack offers a wide range of integration options and [have committed heavily to the Slack platform](https://medium.com/slack-developer-blog/the-slack-platform-roadmap-34067b054177), with a high-level view of their [platform roadmap publicly available](https://trello.com/b/ZnTQyumQ/slack-platform-roadmap-for-developers).

##### Available APIs:

 - [Web API](https://api.slack.com/web):
   - Ability to interact with Slack in more complex ways than the integrations provided out of the box. The Web API consists of HTTP RPC-style methods, all of the form `https://slack.com/api/METHOD`.
   - **Our usage:** Deliver content to users and unfurl URLs.
 - [Real Time Messaging  API](https://api.slack.com/rtm)
   - "The Real Time Messaging API is a WebSocket-based API that allows you to receive events from Slack in real time and send messages as users. It's sometimes referred to as simply the "RTM API"."
   - This is more complicated to implement, due to its WebSocket-based nature and we will try to avoid having to implement it initially.
 - [Events API](https://api.slack.com/events-api)
   - "The Events API is a streamlined, easy way to build apps and bots that respond to activities in Slack. All you need is a Slack app and a secure place for us to send your events"
   - **Our usage:** Subscribe to events, listed [here](#events-subscriptions) that enable a better RelayPad experience.

:information_source: We were initially going to pursue an additional option, [`Incoming Webhooks`](https://api.slack.com/incoming-webhooks) which would have provided us with a URL per team, to which we could `POST` updates and have them displayed in a channel selected by the user. While this integration is great for alerts (e.g. Heroku deployment alerts in our own `#log` channel), it is designed for lightweight apps and since we'll need to `Unfurl URLs` making use of incoming webhooks doesn't make a lot of sense, from [docs](https://api.slack.com/docs/slack-button):

> **Note:** the incoming-webhook scope is designed to allow you to request permission to post content into the user's Slack team. It intentionally does not include any read privileges, making it perfect for services that want to send posts or notifications into Slack teams that might not want to give read access to messages. For this reason, it cannot be added alongside the read, post, and client scopes.

##### Installation
At time of writing there are two ways to integrate with Slack, `Add to Slack` vs. `Sign in with Slack`. To this point we've used `Sign in with Slack` for authentication via Auth0 but it only provides identity information and does not allow us to directly interact with a Slack team (post a notification etc.). We now need to make use of the `Add to Slack` flow, requesting additional [`permission scopes`](https://api.slack.com/docs/oauth-scopes). Ideally we can get a team admin to complete the `Add to Slack` flow, and have other users go through the simpler `Sign in with Slack` flow.

## Future Ideas
There are countless ways with which we can improve our Slack integration in the long term, sequencing this work to drive engagement at first will be critical to our success. Some ideas:

### Daily Briefing
User receives a `Daily Briefing` message from RelayPad if new content is available. Ultimately we want this briefing to be personalized but for starters it will simply summarize newly added team content.

Will be built using the Web API's [`chat.postMessage`](https://api.slack.com/methods/chat.postMessage) method, which requires `chat:write:bot` scope, since we want the [`authorship`](https://api.slack.com/methods/chat.postMessage#authorship) or our messages to come from `RelayPad`. Authorship is generally determined by the type of token used to post message so we'll want to pay particular attention to the token used on these method calls.

##### User Story

```cucumber
Given 08:00 local time for user
 And at least one new post was published to team's RelayPad in the past 24 hours
Then send a send a daily news briefing IM to user
 And include up to three posts, sorted reverse chronologically, that were published in past 24 hours
 And include a link encouraging user to share their own news
```

:thought_balloon: Improvement to consider is to send briefing only after user becomes active on particular day.

##### Example
![render](https://cloud.githubusercontent.com/assets/311182/24591428/d5c4e326-17b5-11e7-9235-917f506ad72c.png)

##### Slack Web API Calls
 - `users.list` may be used if we need to confirm user's slack id, but we should have it by this point.
 - `im.open` opens a direct message channel with user and returns `channel_id` to use
 - `chat.postMessage` sends daily news briefing to user.


### Other Ideas
 - Useful [Slash commands](https://api.slack.com/slash-commands):
   - search for past news from within Slack (by tag, channel, user, etc.)
   - pull in recent Slack messages to a new RelayPad draft (e.g. capture a decision)
 - [Bot user](https://api.slack.com/bot-users)
   - Onboarding of new employees (we notice a new team member and start them on an onboarding path).

# Pricing model
Outline of initial pricing model.

| Content |
|:--------|
| [Goals](#goals) |
| [Free Trial](#free-trial) |
| [Paid team plan](#paid-team-plan)|
| [Research](#research) |

## TL;DR
 - Team based pricing, tiered based on number of users
 - No trial to start

## Goals
 - Simple and compelling pricing, without leaving too much value on the table.

## Free trial

**TL;DR:** No free trial to start, may provide a demo site. Decision reached since trials often result in a lack of engagement, RelayPad requires motivation by team to embrace the service (skin in the game).

#### Initial trial plan
During our initial pricing discussion in September, we felt having a free single-user case would act as an effective trial for users who wanted to kick the tires before committing their whole team to a paid product. We have since realized a key feature of RelayPad is the network effect, and a free single-user tier would not fully showcase RelayPad's value.

#### Trial models explored
- Time based trial: All features enabled for the whole team, but only for 30 days. After that the team's site goes into read-only mode. This is the most traditional free trial model and easiest for customers to understand.
- Usage based trial: Teams can post a limited number of articles before the site goes into read-only mode. Teams with a large number of users would likely have a smaller trial duration than small teams, and this could cause a negative feeling of resource scarcity if your team "only" has a few posts left, resulting in lowered engagement during the trial.
- Feature constrained trial: Certain features (e.g. search) are limited or missing in the free trial. Given that V1 is already pretty lean feature-wise, this didn't seem like a viable option right now.
- No trial: What if there was no trial at all, teams just started paying from day one? We'd definitely have lower total numbers of users, but it could be by a lower factor than we assume. It would also lower support costs since we'd have fewer users (and they'd all be paying customers already).

#### Conclusions
The "No trial" option was surprisingly interesting and we've opted to pursue that option since it means less work to implement and initial users will be generating revenue and more motivated to start. Ultimately we will likely experiment with a free, time-based, trial option.

## Paid team plan
We've decided to go with a per team pricing model, with tiers based on number of users. Initial pricing tiers:

| Number of users | Monthly plan | Yearly plan |
|:----------------|:-------------|:------------|
| 1-50 | $99/month | $1,000/year |
| 51-200 | $199/month | $2,000/year |
| 201-500 | $499/month | $5,000/year |
| 501-1000 | $999/month | $10,000/year |
| >1000 | Contact Us | Contact Us |

## Research

| Service | Pricing screenshot | Notes |
|:--------|:-------------------|:------|
| Slack | ![render](https://cloud.githubusercontent.com/assets/311182/17598428/b1c1ad12-5fb7-11e6-913f-841ab613587c.png) | - Perpetual free tier w/limited functionality. <br> - Offers per `active` users allowing companies to save money if user not engaged. <br> - annual billing discount <br> - premium product with SSO support. |
| Quip | ![render](https://cloud.githubusercontent.com/assets/311182/17598482/080d6ada-5fb8-11e6-891e-67253a5565b4.png) | - Perpetual free tier <br> - $30 min fee for first 5 users <br> - More expensive per user after first 5 users. |
| Asana | ![render](https://cloud.githubusercontent.com/assets/311182/17598559/49fb3080-5fb8-11e6-972c-23b06e8387bf.png) | - Free for 15 users <br> - Complicated discounting for smaller teams, gets more expensive per user as team size grows. |
| GitHub | ![render](https://cloud.githubusercontent.com/assets/311182/17598633/9e46774e-5fb8-11e6-881b-d861f9365d86.png) | - For orgs there's a flat fee of $25 to start for first 5 users <br> $9/user/month after that. |
| Atlassian | ![render](https://cloud.githubusercontent.com/assets/311182/17598667/cd4a0f42-5fb8-11e6-858b-ed4d98aeb32e.png) | - Free for 7 days <br> - $10 per month up to 10 users <br> - Various tiers then depending on size of team. |
| Basecamp |![render](https://cloud.githubusercontent.com/assets/867615/19493247/7db46b96-953f-11e6-960f-bdb84ea4f7fe.png) | - $99 per month, no user limits |

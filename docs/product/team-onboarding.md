# Team Onboarding
Documenting how we'll successfully onboard a new team and encourage first member to complete setup.

| Content |
|:--------|
| [Goals](#goals) |
| [Overview](#overview) |
| [Emails](#emails) |

## Goals
 1. User has published `Welcome to RelayPad` post
    - Hopefully sharing link with team.
 2. User completes `Add to Slack` flow for team

## Overview
This spec is output of discussion at SF May 2017 meetup. Flow will be:

 1. Sign up from marketing landing page
 2. Complete Slack Auth flow
 3. Land on blank dashboard with promotion to complete `Welcome to RelayPad` draft
   - Will will add this draft to user's account at time of signup.
 4. Edit `Welcome to RelayPad` post
 5. Publishing takes user to post page with special promotion to share link on Slack.
    - Unit will include `Add to Slack` promotion.

Supporting white board content follows:

![render](https://cloud.githubusercontent.com/assets/311182/25913501/c94d5db2-356f-11e7-9f89-0ccad64ff65d.png)

## Emails
We will send first user on a team up to two emails to help them complete team configuration.

 1. `Team welcome`
   - Email sent immediately after sign in completed
   - Includes checklist of items to complete:
      - Write `Welcome to RelayPad` post
      - `Add to Slack`
 2. `Add to Slack Reminder`
   - Sent to first user on team after three days if `Add to Slack` flow hasn't been completed.

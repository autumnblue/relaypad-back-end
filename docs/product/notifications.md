# Notifications Spec

## Problem Summary

We do not yet have any tool to pull you back to RelayPad. Unless you remember to go look at it, content you’ve posted or care about is practically invisible.

## Goals

- Increase retention by notifying users when content they care about is posted.
- Reward users who create content by letting them know when people interact with their posts.

## Requirements for in-app Notifications

1. As a user, I should have a centralized area in the app where I can see all of my unseen notifications.
2. As a user, I should receive an in-app notification when someone:
	- Publishes a post with a tag I’m following
	- Publishes a post from an author I’m following
	  - _Note: Following a user isn’t something we support right now, but is a future use case we should account for while planning our notifications system. May be best to implement by migrating all feeds to getstream.io_
	- Comments on a post I authored
	- Reacts to a post I authored
	- Reacts to a comment I made
	- Comments on a post I also commented on
	- Edits a post I commented on
	  - _Note: Punted past 1.0_
3. I should see a visual indicator that a notification is new and unseen.

## Requirements for Email Notifications

1. As a user, I should receive a daily email with a summary of the following activity:
	- New posts with tags I’m following
	- Comments and reactions to posts I’ve authored
	- Comments on posts I’ve commented on or bookmarked
2. As a user, I should be able to opt-out of these emails.

## UI for in-app Notifications

**No new notifications**
![default state](https://cloud.githubusercontent.com/assets/867615/21949060/d6f31a5e-d9b4-11e6-97dc-7b00dfa79d84.png)

**No new notifications, panel open**
![default state open](https://cloud.githubusercontent.com/assets/867615/21949058/d6e58eb6-d9b4-11e6-9e4f-4134ddf25e71.png)

**New notifications**
![unseen notifications](https://cloud.githubusercontent.com/assets/867615/21949059/d6f2da44-d9b4-11e6-95b2-d556269b8ccf.png)

**New notifications, panel open**
![unseen notifications open](https://cloud.githubusercontent.com/assets/867615/21949061/d6f5a0da-d9b4-11e6-8638-76406009b40e.png)

## Open Questions

- How do we integrate with Slack? It seems that the ideal way requires use of a Slack bot in order to directly message a user, but we need to do some more research to confirm.

## Writing

When I create a document, I want to work in an writing environment I am comfortable in so I can focus on the content, not the tool.

## Collaboration

When I write something that will be published to a larger audience, I want to collaborate with people who can help make the end product better.

When someone asks me to help contribute to a document they are going to publish, I want to be able to do it quickly and easily so I don't slow down the process.

## Publishing

When I publish a document, I want it to be presented in a way that is easy to consume so that people are more likely to read it.

When I publish an important document, I want to know that people have seen it so I can be confident that my team is all on the same page.

When I publish a document, I want it to be easily found in the future so I can be confident this knowledge is not lost as teams change.

When I publish a document, I want to team members to share feedback so the content becomes better.

When I publish a document, I want to know if people liked it so I can feel validation that I'm doing a good job.

When I publish a document, I want to know if people reference the document in the future so I can know about new initiatives I may be able to contribute to.

## Reading

When I want to see what a team is working on, I want to find the most recently published content so I can quickly get a feel for what the team is doing.

When someone publishes a document to a group or area that I follow, I want to be notified so I don't miss it.

When I want historical context about a project or event, I want to be able to search and browse different facets so I can find the relevant materials.

When I am reading a document, I want to be able to let the author know how I felt about the doc so they can feel motivated to write good content in the future.

When I am reading a document, I want to ask questions or make comments about specific parts so I can help make the document better.

When I am reading a document, I want to easily access related material (e.g. bug tracking issues referenced in the doc, data dashboards, etc) so I can have a better understanding of the content.

When I am reading a document, I want to know more about the author so I can have fuller context about why they wrote the doc.

import logging
from django.test import TestCase
from rest_framework import status

from reaction import factories
from reaction import models

import core.tests
from comment import factories as comment_factories
from post import factories as post_factories
from post import models as post_models
from member import factories as member_factories

logger = logging.getLogger(__name__)


class ReactionTestCase(core.tests.AuthenticatedTestCase):
    """
    Team members can react to post and comment content. This class tests
    api operations related to those reactions.
    """

    def setUp(self):
        """
        Set up some post and comment content to react to.
        """
        super(ReactionTestCase, self).setUp()
        self.second_member = member_factories.MemberFactory(
            team=self.member.team)

        self.post = post_factories.PostFactory(
            member=self.member,
            team=self.member.team,
            status=post_models.Post.PUBLISHED)

        self.post_reaction = factories.PostReactionFactory(
            member=self.member, post=self.post)

        self.comment = comment_factories.CommentFactory(
            member=self.second_member, post=self.post)

        self.comment_reaction = factories.CommentReactionFactory(
            member=self.member, comment=self.comment)

    def test_post_reaction_retrieval(self):
        response = self.client.get('/api/v1/posts/%s/' % self.post.slug)
        self.assertEquals(response.data['reactions'][0]['count'], 1)

    def test_post_current_member_reaction_retrieval(self):
        response = self.client.get('/api/v1/posts/%s/' % self.post.slug)
        self.assertEquals(
            response.data['reactions'][0]['current_member_reaction'], True)

    def test_post_reaction_creation(self):
        response = self.client.post('/api/v1/posts/%s/react/' % self.post.slug,
                                    {'type': factories.get_reaction_type()})
        self.assertEquals(response.data['post'], self.post.id)

    def test_react_to_another_teams_post(self):
        a_post = post_factories.PostFactory()
        response = self.client.post('/api/v1/posts/%s/react/' % a_post.slug,
                                    {'type': factories.get_reaction_type()})
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_reaction_deletion(self):
        response = self.client.put('/api/v1/posts/%s/react/' % self.post.slug,
                                   {'type': self.post_reaction.type})
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_comment_reaction_retrieval(self):
        response = self.client.get('/api/v1/posts/%s/comments/' % self.post.slug)
        self.assertEquals(response.data['results'][0]['reactions'][0]['count'], 1)

    def test_comment_reaction_creation(self):
        response = self.client.post('/api/v1/comments/%s/react/' % self.comment.id,
            {'type': factories.get_reaction_type()})
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_react_to_another_teams_comment(self):
        a_comment = comment_factories.CommentFactory()
        response = self.client.post('/api/v1/comments/%s/react/' % a_comment.id,
            {'type': factories.get_reaction_type()})
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_comment_reaction_deletion(self):
        response = self.client.put('/api/v1/comments/%s/react/' %
                                   self.comment.id,
                                   {'type': self.comment_reaction.type})
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

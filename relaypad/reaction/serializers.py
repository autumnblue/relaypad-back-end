from rest_framework import serializers

from reaction import models
import member.serializers
import post.serializers
import comment.serializers


class ReactionSerializer(serializers.ModelSerializer):
    """
    Reaction serializer used on GET requests, to return additional information
    about the member that reacted (i.e. name and username). Note: we use ReactionWriteSerializer
    when on endpoints that trigger reaction creation.
    """
    member = member.serializers.MinimalMemberSerializer(read_only=True)

    class Meta:
        model = models.Reaction
        fields = ('id', 'type', 'member')


class ReactionWriteSerializer(serializers.ModelSerializer):
    """
    Version of reaction serializer that doesn't specify a related serializer for
    member, this is due to a DRF limitation where we want more than just 'id' returned
    on GET requests, but only want to specify 'id' with POST request.
    """

    class Meta:
        model = models.Reaction

class ReactionNotificationSerializer(serializers.ModelSerializer):
    """
    Used to serialize reaction data for notifications endpoint. Needs to provide
    additional data (so front-end can display post/comment information)
    """
    post = post.serializers.PostNotificationSerializer(read_only=True, required=False)
    comment = comment.serializers.CommentNotificationSerializer(read_only=True, required=False)

    class Meta:
        model = models.Reaction

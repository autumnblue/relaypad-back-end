import random
import factory
from faker import Faker

from reaction import models

# Utility to generate random fake content.
faker = Faker()

def get_reaction_type():
    "Return random reaction type from model choices"
    reaction_type_choices = [x[0] for x in models.Reaction.REACTION_CHOICES]
    return random.choice(reaction_type_choices)


class PostReactionFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Reaction

    type = factory.LazyFunction(get_reaction_type)
    member = factory.SubFactory('member.factories.MemberFactory')
    post = factory.SubFactory(
        'post.factories.PostFactory', member=member)


class CommentReactionFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Reaction

    type = factory.LazyFunction(get_reaction_type)
    member = factory.SubFactory('member.factories.MemberFactory')
    comment = factory.SubFactory(
        'comment.factories.CommentFactory', member=member)

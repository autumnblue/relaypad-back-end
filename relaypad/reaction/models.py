import logging
from django.db import models
from stream_django.feed_manager import feed_manager
from stream_django.activity import Activity, create_model_reference

logger = logging.getLogger(__name__)

class Reaction(models.Model, Activity):
    SMILE = 'SMILE'
    THUMBS_UP = 'THUMBS_UP'
    COOL = 'COOL'
    APPLAUSE = 'APPLAUSE'
    HEART = 'HEART'
    THINKING = 'THINKING'
    SAD = 'SAD'
    FIRE = 'FIRE'
    ROCKET = 'ROCKET'
    TADA = 'TADA'

    REACTION_CHOICES = (
        (SMILE, 'Smile'),
        (THUMBS_UP, 'Thumbs up'),
        (COOL, 'Cool'),
        (APPLAUSE, 'Applause'),
        (HEART, 'Heart'),
        (THINKING, 'Thinking'),
        (SAD, 'Sad'),
        (FIRE, 'Fire'),
        (ROCKET, 'Rocket'),
        (TADA, 'Tada'),
    )

    post = models.ForeignKey('post.Post', on_delete=models.CASCADE, related_name="reactions", null=True)
    comment = models.ForeignKey('comment.Comment', on_delete=models.CASCADE, related_name="reactions", null=True)
    member = models.ForeignKey('member.Member', on_delete=models.CASCADE, related_name="reactions")
    type = models.CharField(max_length=50, choices=REACTION_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'reaction'

    def __str__(self):
        return self.type

    @property
    def activity_actor_attr(self):
        return self.member

    @property
    def activity_verb(self):
        return 'react'

    @property
    def activity_object_serializer_class(self):
        """
        Stream integration code uses this to programmatically get serializer
        for this model.
        """
        from .serializers import ReactionNotificationSerializer
        return ReactionNotificationSerializer

    @property
    def activity_notify(self):
        targets = []
        if self.post and self.post.member != self.member:
            # Notify author of post of reaction.
            targets.append(feed_manager.get_notification_feed(self.post.member.id))
        elif self.comment and self.comment.member != self.member:
            targets.append(feed_manager.get_notification_feed(self.comment.member.id))
        return targets

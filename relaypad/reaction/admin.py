from django.contrib import admin

from reaction import models


class ReactionsInline(admin.TabularInline):
    model = models.Reaction
    can_delete = True
    extra = 1
    verbose_name = "Reactions"
    verbose_name_plural = "Reactions on this content"

from html import parser
import string

from django.conf import settings

from relaypad import web_urls


class TagViewModel(object):
    def __init__(self, tag):
        self.tag = tag

    def name(self):
        return self.tag.name

    def url(self):
        return web_urls.frontend_url("/{}/tag/{}".format(self.tag.team.slug,
                                                         self.tag.slug))


def trim_content(content, length):
    """Trim our content, but do it nicely"""
    ndx = length
    clean = False

    # First we're going to get something that works in our limits just based
    # on whitespace.
    while ndx > 0:
        if content[ndx] in string.whitespace:
            break

        ndx -= 1

    if ndx == 0:
        # We got nothing, just trim the string.
        return length

    # Now let's see if there is some nearby punctuation we can trim on
    # instead.
    jdx = ndx
    while jdx > 0 and ndx - jdx < 25:
        if content[jdx] in ".?!,;:-":
            clean = True
            ndx = jdx + 1
            break

        jdx -= 1

    return ndx, clean


class IntroParser(parser.HTMLParser):
    # This class parses our post content to try to pick out a good
    # introduction.
    # The basic rule is that we are looking for our first paragraph, and
    # don't include any special formatted content in that paragraph (links,
    # images, etc.)

    MAX_LENGTH = 255

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._content = ""
        self._record = True

    def handle_starttag(self, tag, attrs):
        if self._record and tag == "p":
            # First paragraph tag is acceptable
            pass
        else:
            self._record = False

    def handle_endtag(self, tag):
        self._record = False

    def handle_data(self, data):
        if self._record:
            self._content += data

    def content(self):
        content = self._content.strip()
        if len(content) > self.MAX_LENGTH:
            ndx, clean = trim_content(content, self.MAX_LENGTH)
            result = content[0:ndx].strip()
            if not clean:
                result += "..."

            return result

        return content


class PostViewModel(object):
    """A proxy object to prepare a post for rendering"""

    def __init__(self, post):
        self.post = post

    def title(self):
        return self.post.title

    def intro(self):
        parser = IntroParser()
        parser.feed(self.post.content)
        return parser.content()

    def url(self):
        # NOTE: old format from v1 "news" code. Keeping it around in case I break anything because of it - Mark
        # return web_urls.frontend_url("/{}/post/{}".format(self.post.team.slug,
        #                                                   self.post.slug))
        return web_urls.frontend_url("/{}/notes/@{}/{}".format(self.post.team.slug,
                                                          self.post.member.username,
                                                          self.post.slug))

    def member_url(self):
        return web_urls.frontend_url("{}/@{}".format(
            self.post.team.slug, self.post.member.username))

    def member_name(self):
        return " ".join(
            (self.post.member.first_name, self.post.member.last_name))

    def member_avatar(self):
        if self.post.member.avatar_photo:
            return "https://res.cloudinary.com/{}/image/upload/c_thumb,g_faces,r_max,w_100,h_100/{}".format(
                settings.CLOUDINARY_NAME,
                self.post.member.avatar_photo.public_id)
        else:
            # TODO: empty avatar
            return "#"

    def tags(self):
        return [TagViewModel(t) for t in self.post.tags.all()]

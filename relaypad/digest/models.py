import datetime

from django.db import models
from django.utils import timezone


class DigestManager(models.Manager):
    def due_for_team(self, team):
        interval_dt = timezone.now() - datetime.timedelta(
            hours=Digest.DIGEST_INTERVAL_HOURS)
        return super().get_queryset().filter(
            member__team=team, last_sent_at__lte=interval_dt)


class Digest(models.Model):
    DIGEST_INTERVAL_HOURS = 24

    email_enabled = models.BooleanField()
    slack_enabled = models.BooleanField()
    last_sent_at = models.DateTimeField(null=True)

    member = models.OneToOneField('member.Member', models.CASCADE)

    class Meta:
        db_table = 'digest'

    objects = DigestManager()

    def _is_due(self):
        return self.last_sent_at is None or self.last_sent_at <= timezone.now(
        ) - datetime.timedelta(hours=self.DIGEST_INTERVAL_HOURS)

    def is_email_due(self):
        return self.email_enabled and self._is_due()

    def is_slack_due(self):
        return self.slack_enabled and self._is_due()

    def next_start_dt(self):
        """Datetime to start digest"""
        today = timezone.now()
        yesterday = today - datetime.timedelta(hours=self.DIGEST_INTERVAL_HOURS)

        if self.last_sent_at is None:
            return yesterday
        else:
            return min(yesterday, self.last_sent_at)

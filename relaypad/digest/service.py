import logging
import datetime
import pytz
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils import timezone

from member import models as member_models
from digest import models
from digest import views
from post import service as post_service
from relaypad import web_urls
from slack import tz

logger = logging.getLogger(__name__)


def build_digest_email(digest, welcome=False):
    if welcome:
        digest_since = timezone.now() - datetime.timedelta(days=30)
    else:
        digest_since = digest.next_start_dt()

    posts = post_service.top_posts_since(digest.member, digest_since)
    if len(posts) == 0 and not welcome:
        logger.info("No posts available for digest %r", digest)
        return None

    if welcome and len(posts) < 3:
        # With very few posts, we'll consider this a pretty new team so a
        # new user has already read everything of interest. We can fall back
        # to our default welcome text.
        posts = []

    from_addr = "RelayPad <team@relaypad.com>"

    if welcome:
        subject = "Welcome to RelayPad for {}".format(digest.member.team.name)
    else:
        subject = "{} Daily Briefing for {}".format(
            digest.member.team.name, timezone.now().strftime('%A, %B %d, %Y'))

    context = {
        "welcome": welcome,
        "team_name": digest.member.team.name,
        "read_more_url": web_urls.frontend_url(digest.member.team.slug),
        "today": timezone.now(),
        "posts": [views.PostViewModel(p) for p in posts[:3]],
        "notification_settings_url": web_urls.frontend_url('/settings'),
    }

    html_content = loader.render_to_string('digest_email.html', context)
    #text_content = loader.render_to_string('digest_email.txt', context)
    text_content = "HTML Only"

    email = EmailMultiAlternatives(
        subject,
        text_content,
        from_addr, [digest.member.email, ],
        reply_to=['team@relaypad.com'])
    email.attach_alternative(html_content, "text/html")

    return email


DIGEST_SEND_HOUR = 8


def welcome_digest_for_member(member):
    digest = subscribe_digest(member, email_enabled=True)

    email = build_digest_email(digest, welcome=True)
    if email is None:
        logger.info("Skipping welcome digest for member %s, no content", member)
        return

    email.send()


def subscribe_digest(member, email_enabled=False, slack_enabled=False):
    tz_name = member.timezone or 'Pacific Standard Time'
    tzinfo = tz.slack_tzinfo(tz_name)

    now = datetime.datetime.now(tzinfo)
    if now.hour < DIGEST_SEND_HOUR:
        # We're not past the send time for the day, try yesterday
        now = now - datetime.timedelta(hours=24)

    first_digest_dt = now.replace(
        hour=DIGEST_SEND_HOUR, minute=0, second=0, microsecond=0)

    try:
        d = member.digest
    except member_models.Member.digest.RelatedObjectDoesNotExist:
        d = models.Digest.objects.create(
            member=member,
            email_enabled=email_enabled,
            slack_enabled=slack_enabled,
            last_sent_at=first_digest_dt)
    else:
        d.last_sent_at = first_digest_dt
        d.email_enabled = email_enabled
        d.slack_enabled = slack_enabled
        d.save()

    return d

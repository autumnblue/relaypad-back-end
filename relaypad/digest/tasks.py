import logging
import datetime
from django.utils import timezone

from relaypad.celery import app
import team.models as team_models
import member.models as member_models
from digest import models
from digest import service

logger = logging.getLogger(__name__)


@app.task(bind=True)
def enqueue_digests(self):
    logger.info("Finding teams to process digests to send")

    for t in team_models.Team.objects.all():
        enqueue_digests_for_team.delay(t.id)


@app.task(bind=True)
def enqueue_digests_for_team(self, team_id):
    t = team_models.Team.objects.get(id=team_id)

    logger.info("Finding digest members for team %s (%s)", t.name, team_id)

    for d in models.Digest.objects.due_for_team(t):
        send_digest.delay(d.id)



@app.task(bind=True, max_retries=3, default_retry_delay=5)
def send_digest(self, digest_id):
    d = models.Digest.objects.get(id=digest_id)

    if not d.is_email_due():
        logger.warning("Digest for member %s not due, skipping", d.member)
        return

    logger.info("Generating digest for member %s", d.member)
    email = service.build_digest_email(d)

    if email is not None:
        try:
            email.send()
        except Exception:
            self.retry()
    else:
        logger.info("Skipping digest for member %s, no content", d.member)

    d.last_sent_at = timezone.now()
    d.save()


@app.task(bind=True)
def welcome_digest(self, member_id):
    m = member_models.Member.objects.get(id=member_id)

    service.welcome_digest_for_member(m)

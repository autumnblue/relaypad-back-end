import pytz
import factory
import datetime

from django.test import TestCase
from django.utils import timezone

import member.factories
import post.factories
from digest import tasks
from digest import models
from digest import views
from digest import service


class DigestFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Digest

    email_enabled = True
    slack_enabled = False
    member = factory.SubFactory('member.factories.MemberFactory')


class EnqueueTaskTestCase(TestCase):
    def setUp(self):
        self.before = timezone.now() - datetime.timedelta(hours=25)
        self.digest = DigestFactory(last_sent_at=self.before)

        p = post.factories.PostFactory(
            team=self.digest.member.team,
            member=self.digest.member,
            published_at=timezone.now())

    def test_enqueue(self):
        tasks.enqueue_digests()
        self.digest.refresh_from_db()
        self.assertGreater(self.digest.last_sent_at, self.before)


class WelcomeTestCase(TestCase):
    def setUp(self):
        self.member = member.factories.MemberFactory(timezone='Eastern Standard Time')

    def test(self):
        service.welcome_digest_for_member(self.member)

        dt = self.member.digest.last_sent_at
        self.assertGreater(timezone.now(), dt)
        self.assertEqual(dt.hour, 8)


class IntroParserTest(TestCase):
    def test_empty(self):
        parser = views.IntroParser()
        parser.feed("")

        self.assertEqual(parser.content(), "")

    def test_nocontent(self):
        parser = views.IntroParser()
        parser.feed("hello world")

        self.assertEqual(parser.content(), "hello world")

    def test_firstparagraph(self):
        parser = views.IntroParser()
        parser.feed("<p>hello world</p>")
        self.assertEqual(parser.content(), "hello world")

    def test_endonformatting(self):
        parser = views.IntroParser()
        parser.feed("<p>hello <a href=\"#\">world</a></p>")

        self.assertEqual(parser.content(), "hello")

    def test_trimwhitespace(self):
        parser = views.IntroParser()
        parser.feed("hello  " + ("a" * 255))

        self.assertEqual(parser.content(), "hello...")

    def test_trimsentence(self):
        parser = views.IntroParser()
        parser.feed("hello world.  " + ("a" * 255))

        self.assertEqual(parser.content(), "hello world.")

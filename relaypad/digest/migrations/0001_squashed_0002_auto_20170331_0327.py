# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-31 03:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    replaces = [('digest', '0001_initial'), ('digest', '0002_auto_20170331_0327')]

    initial = True

    dependencies = [
        ('member', '0005_member_onboarded'),
    ]

    operations = [
        migrations.CreateModel(
            name='Digest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_sent_at', models.DateTimeField(null=True)),
                ('member', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='member.Member')),
            ],
            options={
                'db_table': 'digest',
            },
        ),
    ]

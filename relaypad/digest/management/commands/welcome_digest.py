from django.core.management.base import BaseCommand, CommandError

from digest import service
from digest import models
import member.models



class Command(BaseCommand):
    help = 'Send welcome digest for specified member'

    def add_arguments(self, parser):
        parser.add_argument('team_member', metavar="team/member")

    def handle(self, *args, **options):
        team_name, member_name = options['team_member'].split('/')

        m = member.models.Member.objects.get(team__slug=team_name, username=member_name)

        self.stdout.write("Welcoming member {}".format(m))
        service.welcome_digest_for_member(m)

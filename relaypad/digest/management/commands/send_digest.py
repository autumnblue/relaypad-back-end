from django.core.management.base import BaseCommand, CommandError

from digest import tasks
from digest import models
import member.models



class Command(BaseCommand):
    help = 'Send digest for specified member'

    def add_arguments(self, parser):
        parser.add_argument('team_member', metavar="team/member")

    def handle(self, *args, **options):
        team_name, member_name = options['team_member'].split('/')

        m = member.models.Member.objects.get(team__slug=team_name, username=member_name)

        try:
            d = m.digest
        except member.models.Member.digest.RelatedObjectDoesNotExist:
            d = models.Digest.objects.create(member=m)

        if not d.is_email_due():
            d.last_sent_at = None
            d.email_enabled = True
            d.save()

        self.stdout.write("Sending digest {}".format(m.digest.id))

        tasks.send_digest(m.digest.id)

from django.contrib import admin

from comment import models
import reaction.admin


class CommentReactionsInline(reaction.admin.ReactionsInline):
    fields = ('type', 'member')


@admin.register(models.Comment)
class CommentAdmin(admin.ModelAdmin):
    inlines = (CommentReactionsInline, )

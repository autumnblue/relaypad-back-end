from rest_framework import serializers
from core.helpers.serializer_helpers import ReactionListField

from comment import models
import member.serializers
import post.serializers


class CommentSerializer(serializers.ModelSerializer):
    member = member.serializers.SummaryMemberSerializer(read_only=True)
    reactions = ReactionListField(read_only=True)

    class Meta:
        model = models.Comment
        fields = ('id', 'content', 'post', 'member', 'reactions', 'created_at',
                  'updated_at')

class CommentNotificationSerializer(serializers.ModelSerializer):
    """
    Reduced functionality serializer to support serializer of notification
    data (i.e. only used to provide limited data to notification components)
    """
    post = post.serializers.PostNotificationSerializer(read_only=True)

    class Meta:
        model = models.Comment

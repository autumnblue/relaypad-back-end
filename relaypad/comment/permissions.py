import logging
from rest_framework import permissions

logger = logging.getLogger(__name__)

class isAuthorOrReadOnly(permissions.BasePermission):
    """
    Only a comment author should be able to edit/delete comment.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.member == request.user.current_membership

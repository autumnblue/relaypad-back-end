import logging
from django.test import TestCase
from rest_framework import status
from faker import Faker

import core.tests
from comment import factories
from comment import models
from post import models as post_models
from post import factories as post_factories

logger = logging.getLogger(__name__)
faker = Faker()

class CommentTestCase(core.tests.AuthenticatedTestCase):
    """
    RelayPad posts can be commented on.
    """

    def setUp(self):
        super(CommentTestCase, self).setUp()
        self.post = post_factories.PostFactory(
            member=self.member,
            team=self.member.team,
            status=post_models.Post.PUBLISHED)
        self.comment = factories.CommentFactory(
            post=self.post,
            member=self.member)

    def test_commenting_on_own_post(self):
        a_post = post_factories.PostFactory(
            member=self.member,
            team=self.member.team,
            status=post_models.Post.PUBLISHED
        )
        response = self.client.post(
            '/api/v1/posts/%s/comments/' % a_post.slug,
            {'content': faker.text()})
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_commenting_on_another_members_post(self):
        another_members_post = post_factories.PostFactory(
            team=self.member.team,
            status=post_models.Post.PUBLISHED)
        response = self.client.post(
            '/api/v1/posts/%s/comments/' % another_members_post.slug,
            {'content': faker.text()})
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_comment_retrieval(self):
        response = self.client.get('/api/v1/comments/%s/' % self.comment.id)
        self.assertEquals(response.data['content'], self.comment.content)

    def test_comment_editing(self):
        response = self.client.patch(
            '/api/v1/comments/%s/' % self.comment.id,
            {'content': faker.text()})
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_own_comment(self):
        response = self.client.delete('/api/v1/comments/%s/' % self.comment.id)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_deleting_another_members_comment(self):
        another_members_comment = factories.CommentFactory(post=self.post)
        response = self.client.delete('/api/v1/comments/%s/' % another_members_comment.id)
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_deleting_another_teams_comment(self):
        another_teams_comment = factories.CommentFactory()
        response = self.client.delete('/api/v1/comments/%s/' % another_teams_comment.id)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

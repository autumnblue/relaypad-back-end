from django.shortcuts import render
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from comment import models
from comment import serializers
from comment import permissions
import reaction.serializers
import reaction.models


class CommentViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.DestroyModelMixin,
                     viewsets.GenericViewSet):
    """
    Viewset for handling all Comment specific actions.
    """
    serializer_class = serializers.CommentSerializer
    permission_classes = (IsAuthenticated, permissions.isAuthorOrReadOnly,)

    def get_queryset(self):
        return models.Comment.objects.filter(
            post__team=self.request.user.current_membership.team)

    @detail_route(methods=['post', 'put'], permission_classes=(IsAuthenticated,))
    def react(self, request, pk=None):
        """
        Handle reacting or unreacting to specific comment
        """
        if (request.META['REQUEST_METHOD'] == 'POST'):
            comment = self.get_object()
            serializer = reaction.serializers.ReactionWriteSerializer(data={
                'comment': comment.id,
                'member': request.user.current_membership.id,
                'type': request.data['type']
            })
            if (serializer.is_valid(raise_exception=True)):
                serializer.save()
                return Response(
                    serializer.data, status=status.HTTP_201_CREATED)
        elif (request.META['REQUEST_METHOD'] == 'PUT'):
            # Using PUT for reaction removal since we need to pass reaction type in body (DELETE prevents that).
            comment = self.get_object()
            try:
                a_reaction = reaction.models.Reaction.objects.get(
                    comment=comment,
                    member=request.user.current_membership,
                    type=request.data['type'])
            except reaction.models.Reaction.DoesNotExist:
                logger.error("Attempt to delete a reaction that doesn't exist")
                raise exceptions.NotFound('The reaction specified was not found.')
            self.perform_destroy(a_reaction)
            return Response(status=status.HTTP_204_NO_CONTENT)

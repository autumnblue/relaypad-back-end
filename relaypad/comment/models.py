import logging
from django.db import models
from stream_django.feed_manager import feed_manager
from stream_django.activity import Activity, create_model_reference

logger = logging.getLogger(__name__)

class Comment(models.Model, Activity):
    content = models.TextField()
    parent = models.IntegerField(null=True, blank=True)
    post = models.ForeignKey('post.Post', on_delete=models.CASCADE, related_name="comments")
    member = models.ForeignKey('member.Member', on_delete=models.CASCADE, related_name="comments")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'comment'

    def __str__(self):
        return "%s comment (by @%s)" %(self.post.title, self.member.username)

    @property
    def activity_actor_attr(self):
        return self.member

    @property
    def activity_object_serializer_class(self):
        """
        Stream integration code uses this to programmatically get serializer
        for this model.
        """
        from .serializers import CommentNotificationSerializer
        return CommentNotificationSerializer

    @property
    def activity_notify(self):
        targets = []
        if (self.member != self.post.member):
            # Notify author of all comments made by other members.
            targets.append(feed_manager.get_notification_feed(self.post.member.id))
        for comment in self.post.comments.all():
            if comment.member != self.member and comment.member != self.post.member:
                # Notify other, non-author, commenters of comment.
                targets.append(feed_manager.get_notification_feed(comment.member.id))
        return targets

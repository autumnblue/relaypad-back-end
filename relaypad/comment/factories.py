import factory
from faker import Faker

from comment import models

# Utility to generate random fake content.
faker = Faker()


class CommentFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Comment

    content = factory.LazyAttribute(lambda obj: faker.text())
    member = factory.SubFactory('member.factories.MemberFactory')
    post = factory.SubFactory(
        'post.factories.PostFactory', member=member)

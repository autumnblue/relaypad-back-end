import factory
import uuid

from photo import models


class PhotoFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Photo

    public_id = factory.LazyAttribute(lambda obj: uuid.uuid4().hex)

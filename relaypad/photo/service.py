import cloudinary.uploader

from photo import models


def create_photo_from_path(path):
    image = cloudinary.uploader.upload(path)

    return models.Photo.objects.create(public_id=image['public_id'])

from django.db import models
import cloudinary


class Photo(models.Model):
    public_id = models.CharField(
        max_length=255, null=False, blank=False, unique=True)

    def cloudinary_image(self):
        return cloudinary.CloudinaryImage(self.public_id)

    class Meta:
        db_table = 'photo'

    def __str__(self):
        return "Photo (public_id = %s)" % self.public_id

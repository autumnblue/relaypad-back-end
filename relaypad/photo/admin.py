from django.contrib import admin

from photo import models

@admin.register(models.Photo)
class PhotoAdmin(admin.ModelAdmin):
    pass

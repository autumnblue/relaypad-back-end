from django.core.management.base import BaseCommand, CommandError

import photo.service
import photo.models
import member.models


class Command(BaseCommand):
    help = 'Upload photo and set as avatar for the specified user'

    def add_arguments(self, parser):
        parser.add_argument('team_member', metavar="team/member")
        parser.add_argument('path')

    def handle(self, *args, **options):
        team_name, member_name = options['team_member'].split('/')

        m = member.models.Member.objects.get(team__slug=team_name, username=member_name)

        p = photo.service.create_photo_from_path(options['path'])
        m.avatar_photo = p
        m.save()

        self.stdout.write("Created avatar photo {}".format(p.public_id))

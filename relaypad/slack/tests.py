import logging
import os
import pytz
from django.test import TestCase
from django.conf import settings
from rest_framework import status
from rest_framework.test import APIClient

import core.tests
from post import factories as post_factories
from post import models as post_models
from tag import models as tag_models
from team import factories as team_factories
from member import models as member_models
from member import factories as member_factories
from tag import factories as tag_factories
from digest import views as digest_views
from slack import tasks as slack_tasks
from slack import tz

logger = logging.getLogger(__name__)

class SlackTestCase(core.tests.AuthenticatedTestCase):
    """
    Tests Slack integration functionality (e.g. syncing tags to public Slack channels)
    """

    def setUp(self):
        super(SlackTestCase, self).setUp()

    def test_link_new_tag_to_channel(self):
        # Fake data we'd get from Slack API.
        # TODO: Abstract this fake data creation.
        channel_info = {
            "created": 1493854241,
            "creator": "U24JQCF5Y",
            "id": "FAKE_CHANNEL_ID",
            "is_archived": True,
            "is_channel": True,
            "is_general": False,
            "is_member": False,
            "is_org_shared": False,
            "is_shared": False,
            "members": [],
            "name": "unit-test-channel",
            "name_normalized": "unit-test-channel",
            "num_members": 0,
            "previous_names": [
                "second-slack-test"
            ],
            "purpose": {
                "creator": "",
                "last_set": 0,
                "value": ""
            },
            "topic": {
                "creator": "",
                "last_set": 0,
                "value": ""
            }
        }

        slack_tasks._link_tag_to_slack_channel(self.member.team, channel_info)
        tag = tag_models.Tag.objects.get(team=self.member.team, slug=channel_info['name'])
        self.assertEquals(tag.slack_channel_id, channel_info['id'])

    def test_auto_member_subscribe_to_channels_tag(self):
        """
        We want channel members to be auto subscribed to corresponding RelayPad tag.
        """
        tag = tag_factories.TagFactory(team=self.member.team)

        # Fake data we'd get from Slack API.
        # TODO: Abstract this fake data creation.
        channel_info = {
            "created": 1493854241,
            "creator": "U24JQCF5Y",
            "id": "FAKE_CHANNEL_ID",
            "is_archived": True,
            "is_channel": True,
            "is_general": False,
            "is_member": False,
            "is_org_shared": False,
            "is_shared": False,
            "members": [
                self.member.slack_id
            ],
            "name": "unit-test-channel",
            "name_normalized": "unit-test-channel",
            "num_members": 0,
            "previous_names": [
                "second-slack-test"
            ],
            "purpose": {
                "creator": "",
                "last_set": 0,
                "value": ""
            },
            "topic": {
                "creator": "",
                "last_set": 0,
                "value": ""
            }
        }

        slack_tasks._subscribe_channel_members(self.member.team, tag, channel_info)
        subscription = tag_models.Subscription.objects.get(
            tag=tag,
            member=self.member)
        self.assertIsNotNone(subscription)

    def test_post_unfurl_data_retrieval(self):
        post = post_factories.PostFactory(status=post_models.Post.PUBLISHED)
        post_unfurl_data = slack_tasks._get_post_unfurl_data(post)
        self.assertEquals(post.title, post_unfurl_data['title'])

    def test_updating_existing_member_from_slack(self):
        member = member_factories.MemberFactory()

        # TODO: Improve generating of this fake data.
        # Emulate data retrieved from Slack.
        slack_member_response = {
            "id": member.slack_id,
            "name": "SLACK_PROVIDED_USERNAME",
            "deleted": False,
            "color": "9f69e7",
            "tz_label": "Pacific Daylight Time",
            "profile": {
                "avatar_hash": "ge3b51ca72de",
                "current_status": ":mountain_railway: riding a train",
                "first_name": "Bobby",
                "last_name": "Tables",
                "real_name": "Bobby Tables",
                "email": "bobby@slack.com",
                "skype": "SLACK_PROVIDED_SKYPE",
                "phone": "+1 (123) 456 7890"
            },
            "is_admin": True,
            "is_bot": False,
            "is_owner": True,
            "updated": 1490054400,
            "has_2fa": False
        }

        slack_tasks._persist_member_slack_information(
            slack_member_response,
            member.team,
            False)

        # Refresh member and compare.
        refreshed_member = member_models.Member.objects.get(id=member.id)
        self.assertEquals(refreshed_member.slack_id, member.slack_id)
        self.assertEquals(refreshed_member.username, 'SLACK_PROVIDED_USERNAME')
        self.assertEquals(refreshed_member.skype_handle, 'SLACK_PROVIDED_SKYPE')
        self.assertEquals(refreshed_member.is_slack_admin, True)

    def test_creating_new_member_based_on_slack(self):
        team = team_factories.TeamFactory()

        # TODO: Improve generating of this fake data.
        # Emulate data retrieved from Slack.
        slack_member_response = {
            "id": "TEST_SLACK_ID",
            "name": "SLACK_PROVIDED_USERNAME",
            "deleted": False,
            "color": "9f69e7",
            "tz_label": "Pacific Daylight Time",
            "profile": {
                "avatar_hash": "ge3b51ca72de",
                "current_status": ":mountain_railway: riding a train",
                "first_name": "Bobby",
                "last_name": "Tables",
                "real_name": "Bobby Tables",
                "email": "bobby@slack.com",
                "skype": "SLACK_PROVIDED_SKYPE",
                "phone": "+1 (123) 456 7890"
            },
            "is_admin": True,
            "is_bot": False,
            "is_owner": True,
            "updated": 1490054400,
            "has_2fa": False
        }

        slack_tasks._persist_member_slack_information(
            slack_member_response,
            team,
            False)

        a_member = member_models.Member.objects.get(
            slack_id=slack_member_response['id'],
            team=team)

        self.assertEquals(a_member.slack_id, 'TEST_SLACK_ID')
        self.assertEquals(a_member.username, 'SLACK_PROVIDED_USERNAME')
        self.assertEquals(a_member.skype_handle, 'SLACK_PROVIDED_SKYPE')
        self.assertEquals(a_member.is_slack_admin, True)

class SlackEventTestCase(TestCase):
    """
    Test our response to Slack event that we've subscribed to (e.g. `link_shared` for unfurling)
    """

    def setUp(self):
        """
        Create an unauthorized API client to call public endpoint available to Slack.
        """
        self.client = APIClient()
        self.post = post_factories.PostFactory(status=post_models.Post.PUBLISHED)
        self.supported_slack_events = [
            'link_shared',
            'team_join',
            'team_domain_change',
            'team_rename',
            'user_change',
            'channel_created',
            'channel_deleted',
            'channel_rename',
            'channel_archive',
            'channel_unarchive']

    def test_slack_challenge(self):
        """
        Confirm we're capable of handling Slack URL verification challenge.
        """
        response = self.client.post('/api/v1/slack/listening/',
            {'challenge': 'SLACK_CHALLENGE_TEST'})
        self.assertEquals(response.data, 'SLACK_CHALLENGE_TEST')

    def test_invalid_slack_token(self):
        response = self.client.post('/api/v1/slack/listening/',
            {'token': 'INVALID_TOKEN'})
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_invalid_event_request(self):
        response = self.client.post('/api/v1/slack/listening/',
            {'token': settings.SLACK_VERIFICATION_TOKEN})
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_slack_event_acknowledgement(self):
        for slack_event in self.supported_slack_events:
            response = self.client.post('/api/v1/slack/listening/',
                {'token': settings.SLACK_VERIFICATION_TOKEN,
                 'event': {'type': slack_event}},
                 format='json')
            self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_unfurling_of_valid_post(self):
        post_render_proxy = digest_views.PostViewModel(self.post)
        unfurls = slack_tasks._unfurl_links(self.post.team, [{'url': post_render_proxy.url()}])
        self.assertEquals(unfurls[post_render_proxy.url()]['title'], post_render_proxy.title())

    def test_invalid_post_unfurling_attempt(self):
        """
        We should NEVER unfurl a post tied to another team.
        """
        team = team_factories.TeamFactory()
        post_render_proxy = digest_views.PostViewModel(self.post)
        unfurls = slack_tasks._unfurl_links(team, [{'url': post_render_proxy.url()}])
        self.assertEquals(len(unfurls), 0)

    def test_channel_rename_handling(self):
        team = team_factories.TeamFactory()
        tag = tag_factories.TagFactory(team=team)

        # Emulate data we expect from Slack for this event.
        # TODO: Abstract this fake data creation.
        event_data = {
            "channel": {
                "name": "new_tag_name",
                "id": tag.slack_channel_id
            }
        }

        slack_tasks.rename_slack_channel_tag(team, event_data)
        tag = tag_models.Tag.objects.get(team=team, slack_channel_id=tag.slack_channel_id)
        self.assertEquals(tag.name, "new_tag_name")

    def test_channel_unlinking(self):
        """
        If we receive a `channel_deleted` or `channel_archived` event, we want to unlink tag.
        """
        team = team_factories.TeamFactory()
        tag = tag_factories.TagFactory(team=team)

        # Emulate data we expect from Slack for this event.
        # TODO: Abstract this fake data creation.
        event_data = {
            "channel": tag.slack_channel_id
        }

        slack_tasks.unlink_slack_channel(team, event_data)
        tag = tag_models.Tag.objects.get(team=team, slug=tag.slug)
        self.assertIsNone(tag.slack_channel_id)


class TZTestCase(TestCase):
    def test(self):
        tzinfo = tz.slack_tzinfo("Zulu Time Zone")
        self.assertEquals(tzinfo, pytz.UTC)

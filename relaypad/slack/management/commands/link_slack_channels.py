from django.core.management.base import BaseCommand, CommandError

from slack import tasks
from team import models as team_models


class Command(BaseCommand):
    help = "Sync slack channel(s) for specified team"

    def add_arguments(self, parser):
        parser.add_argument(
            'team',
            metavar="team")

    def handle(self, *args, **options):
        try:
            a_team = team_models.Team.objects.get(slug=options['team'])
        except team_models.Team.DoesNotExist:
            raise CommandError("Team %s does not exist." % options['team'])
        else:
            self.stdout.write("Syncing channels for {}".format(a_team.slug))
            tasks.link_tags_to_slack_channels(a_team)

from django.core.management.base import BaseCommand, CommandError

from slack import tasks
from team import models as team_models
from member import models as member_models


class Command(BaseCommand):
    help = "Sync slack member information for specified team"

    def add_arguments(self, parser):
        parser.add_argument('team', metavar="team")

        # Named (optional) arguments
        parser.add_argument(
            '--member',
            help="Specify an individual member to sync")

    def handle(self, *args, **options):
        try:
            a_team = team_models.Team.objects.get(slug=options['team'])
        except team_models.Team.DoesNotExist:
            raise CommandError("Team %s does not exist." % options['team'])
        else:
            if options['member']:
                # Only sync an individual member.
                try:
                    a_member = member_models.Member.objects.get(
                        team=a_team,
                        username=options['member'])
                except member_models.Member.DoesNotExist:
                    raise CommandError("User %s not a member of %s." %
                        (options['member'], options['team']))
                else:
                    self.stdout.write("Syncing {} member information...".format(a_member.username))
                    tasks.sync_member_slack_information(a_member.slack_id, a_team)
            else:
                self.stdout.write("Syncing members for {}...".format(a_team.slug))
                tasks.sync_all_member_slack_information(a_team)

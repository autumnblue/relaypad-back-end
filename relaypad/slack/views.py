import json
import logging

from django.conf import settings
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response

from slack import tasks
from team import models as team_models

logger = logging.getLogger(__name__)

@api_view(['POST'])
@permission_classes([])
@authentication_classes([])
def event_listener(request):
    """
    Listens for incoming event requests from Slack, verifies authentication
    of request and then hands off handling of event before acknowledging receipt.
    """

    # Make sure we always handle Slack url verification challenge
    # See https://api.slack.com/events/url_verification
    if 'challenge' in request.data:
        logger.info("Responding to url verification challenge from Slack.")
        return Response(request.data['challenge'], status=status.HTTP_200_OK)

    # Verify that request is indeed coming from Slack
    if settings.SLACK_VERIFICATION_TOKEN != request.data.get('token'):
        logger.warning("Invalid slack verification token provided, received '%s'",
            request.data.get('token'))
        return Response(status=status.HTTP_403_FORBIDDEN)

    if 'event' in request.data:
        try:
            _event_handler(request.data)
        finally:
            # Important to think of events as pings and always acknowledge them.
            return Response(status=status.HTTP_200_OK)

    return Response("[NO EVENT IN SLACK REQUEST] These are not the droids you're looking for.",
        status=status.HTTP_400_BAD_REQUEST)

def _event_handler(request_data):
    """
    Event handler that should be prepared to handle all Slack
    events that we support. Success will trigger an async task.
    """
    logger.debug("Handling Slack event: %s", request_data)
    team = _get_team_for_slack_event(request_data['team_id'])

    try:
        event_data = request_data['event']
        event_type = event_data['type']
    except KeyError:
        logger.error("Poorly formed Slack event, data proided: %s", request_data)

    if event_type == 'link_shared':
        tasks.unfurl_url(team, event_data)
    elif event_type == 'channel_created' or event_type == 'channel_unarchive':
        tasks.link_tags_to_slack_channels(team)
    elif event_type == 'channel_deleted' or event_type == 'channel_archive':
        tasks.unlink_slack_channel(team, event_data)
    elif event_type == 'channel_rename':
        tasks.rename_slack_channel_tag(team, event_data)
    elif event_type == 'user_change':
        tasks.sync_member_slack_information(event_data['user']['id'], team)
    else:
        logger.warning("Received '%s' slack event, which we don't support.", event_type)

def _get_team_for_slack_event(team_slack_id):
    try:
        team = team_models.Team.objects.get(slack_id=team_slack_id)
    except team_models.Team.DoesNotExist:
        logger.error("Unable to find team associated with slack event, event team_id = %s",
            team_slack_id)
    else:
        return team

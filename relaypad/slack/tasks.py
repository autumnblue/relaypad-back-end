import logging
import requests
import json
import cloudinary.api

from urllib.parse import urlparse
from django.utils.dateformat import format
from slackclient import SlackClient

from relaypad import web_urls
from relaypad.celery import app
from digest import views as digest_views
from post import models as post_models
from tag import models as tag_models
from member import models as member_models
from member import service as member_service
from core import tasks as core_tasks

logger = logging.getLogger(__name__)

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def unfurl_url(self, team, event_data):
    """
    Attempts to unfurl a URL within Slack.
    """
    try:
        unfurls = _unfurl_links(team, event_data['links'])
    except KeyError:
        logger.error("Problem parsing Slack `link_shared` event data: %s", event_data)
    else:
        if len(unfurls) > 0:
            # We managed to unfurl some urls, send info to Slack.
            slack_client = SlackClient(team.slack_app_access_token)

            try:
                slack_client.api_call(
                    "chat.unfurl",
                    channel=event_data['channel'],
                    ts=event_data['message_ts'],
                    unfurls=json.dumps(unfurls)
                )
            except Exception as e:
                logger.error("Problem sending unfurl data to Slack: %s" % traceback.format_exc())

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def sync_slack_team(self, team):
    """
    Helper method to kickoff all Slack syncing operations
    """
    sync_all_member_slack_information(team)
    link_tags_to_slack_channels(team)

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def link_tags_to_slack_channels(self, team):
    """
    Attempts to sync team tags with their respective slack channels.
    e.g. `Sales` tag would be mapped to `#sales` in Slack.
    """
    logger.debug("Attempting to link slack channels for %s team", team.slug)
    slack_client = SlackClient(team.slack_app_access_token)
    response = slack_client.api_call(
            "channels.list",
            exclude_archived=1)

    if response['ok']:
        for channel in response['channels']:
            _link_tag_to_slack_channel(team, channel)
    else:
        logger.error("Problem retrieving channel list from Slack. Error = %s", response['error'])

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def unlink_slack_channel(self, team, event_data):
    """
    Unlink Slack channel from topic in response to a Slack event.
    """
    logger.debug("Attempting to unlink %s slack channel for %s team",
        event_data['channel'],
        team.slug)
    try:
        a_tag = tag_models.Tag.objects.filter(
            team=team,
            slack_channel_id=event_data['channel']).update(
                slack_channel_id=None)
    except tag_models.Tag.DoesNotExist:
        logger.info("No tag found for %s slack channel in %s team",
            event_data['channel'],
            team.slug)

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def rename_slack_channel_tag(self, team, event_data):
    """
    A Slack channel was renamed, we'll rename the tag linked to that channel.
    Opted to rename tag slug (and hence break URLs to topics page) seeing that value
    here is to keep our tags in sync with Slack. Should be an infrequent use case.
    """
    logger.debug("Attempting to rename slack channel to %s for %s team",
        event_data['channel']['name'],
        team.slug)
    try:
        a_tag = tag_models.Tag.objects.filter(
            team=team,
            slack_channel_id=event_data['channel']['id']).update(
                name=event_data['channel']['name'],
                slug=event_data['channel']['name'])
    except tag_models.Tag.DoesNotExist:
        logger.info("No tag found for %s slack channel in %s team",
            event_data['channel'],
            team.slug)

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def share_post(self, post):
    """
    Triggered when a post is published and responsible for sharing a post
    with slack channels linked to its tags.
    """
    logger.debug("Checking to see if post (%s) should be shared with %s's slack channels",
        post.slug,
        post.team.slug)

    slack_client = SlackClient(post.team.slack_app_access_token)

    for tag in post.tags.all():
        if tag.slack_channel_id is not None:
            # Try to post message to linked slack channel.
            slack_formatted_post_data = _get_post_unfurl_data(post)
            slack_formatted_post_data['pretext'] = "@{} published a new post".format(post.member.username)
            response = slack_client.api_call(
                "chat.postMessage",
                channel=tag.slack_channel_id,
                attachments=[slack_formatted_post_data])
            if response['ok']:
                logger.info("Shared %s post with %s's #%s slack channel",
                    post.slug, post.team.slug, tag.slug)
            else:
                logger.warning("Problem sharing %s post with %s's #%s slack channel",
                    post.slug, post.team.slug, tag.slug)

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def sync_all_member_slack_information(self, team):
    logger.debug("Attempting to sync slack members for %s team", team.slug)

    slack_client = SlackClient(team.slack_app_access_token)
    response = slack_client.api_call("users.list")

    if response['ok']:
        for slack_member_info in response['members']:
            _persist_member_slack_information(slack_member_info, team)
    else:
        logger.error("Problem retrieving users list from Slack. Error = %s", response['error'])

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def sync_member_slack_information(self, member_slack_id, team):
    """
    Syncs an individual team member's Slack information. Generally triggered by
    `user_change` Slack event.
    """

    slack_client = SlackClient(team.slack_app_access_token)
    response = slack_client.api_call(
        "users.info",
        user=member_slack_id)

    if response['ok']:
        _persist_member_slack_information(response['user'], team)
    else:
        logger.error("Problem retrieving users info from Slack. Error = %s", response['error'])

def _persist_member_slack_information(slack_member_response, team, refresh_member_photo=True):
    if (slack_member_response['id'] != 'USLACKBOT' and
        not slack_member_response['is_bot'] and
        not slack_member_response['deleted']):
        # Want to avoid syncing bots or deleted users.

        logger.debug("Syncing %s member information for %s team: %s",
            slack_member_response['name'],
            team.slug,
            slack_member_response)

        try:
            member = member_models.Member.objects.get(
                team=team,
                slack_id=slack_member_response['id'])
        except member_models.Member.DoesNotExist:
            member = member_service.create_membership(slack_member_response, team, info_source="SLACK")

        # Update member with latest Slack specific info (e.g. skype handle if available).
        slack_member_attributes = member_service.extract_member_info_from_slack_response(slack_member_response)
        member_models.Member.objects.update_or_create(
            id=member.id,
            defaults=slack_member_attributes)

        if refresh_member_photo and slack_member_response['profile'].get('image_192', None):
            # Refresh member avatar photo.
            try:
                core_tasks.upload_avatar_photo(member.id, slack_member_response['profile']['image_192'])
            except:
                logger.exception("Failed to upload avatar for %s/%s during Slack installation. Moving on.",
                    member.team.slug,
                    member.username)

    else:
        logger.info("Ignoring bot or deleted user (%s), no information being synced.", slack_member_response)

def _unfurl_links(team, links):
    """
    Unfurls provided links, if supported possible, and returns unfurl
    a
    """
    for link in links:
        unfurls = {}
        logger.info("Attempting to unfurl %s", link['url'])
        parsed_url = urlparse(link['url'])
        try:
            # TODO: Support additional URLs.
            # Parse post URLs (/:team/notes/:username/:post_slug)
            url_team_slug = parsed_url.path.split('/')[1]
            url_post_slug = parsed_url.path.split('/')[4]
        except IndexError:
            logger.info("Unsupported URL unfurl requested: %s", link)
        else:
            if url_team_slug == team.slug:
                # Security note: Only unfurl urls tied to slack team_id provided.
                try:
                    post = post_models.Post.objects.get(
                        team__slug=url_team_slug,
                        slug=url_post_slug,
                        status=post_models.Post.PUBLISHED)
                except post_models.Post.DoesNotExist:
                    logger.info("Couldn't find published post to unfurl for %s",
                        link['url'])
                else:
                    unfurls[link['url']] = _get_post_unfurl_data(post)

    return unfurls

def _get_post_unfurl_data(post):
    post_render_proxy = digest_views.PostViewModel(post)
    unfurl_data = {
        'color': '#6699cc', # Controls color of side bar beside unfurl in Slack.
        'fallback': post_render_proxy.title(),
        'author_name': post_render_proxy.member_name(),
        'author_link': post_render_proxy.member_url(),
        'author_icon': post_render_proxy.member_avatar(),
        'title': post_render_proxy.title(),
        'title_link': post_render_proxy.url(),
        'text': post_render_proxy.intro(),
        'ts': format(post.published_at, 'U')
    }
    return unfurl_data

def _link_tag_to_slack_channel(team, channel):
    """
    Creates/updates tag based on channel information provided by Slack.
    """
    a_tag, is_new_tag = tag_models.Tag.objects.update_or_create(
        team=team,
        slug=channel['name'],
        defaults={'slack_channel_id': channel['id'], 'name': channel['name']})

    if is_new_tag:
        logger.info("Created %s tag to pair with #%s Slack channel for %s team.",
            a_tag.slug,
            channel['name'],
            team.slug)

    _subscribe_channel_members(team, a_tag, channel)

def _subscribe_channel_members(team, tag, channel_info):
    """
    Subscribe channel members to linked Relaypad tag.
    """

    for member_slack_id in channel_info['members']:
        try:
            a_member = member_models.Member.objects.get(
                team=team,
                slack_id=member_slack_id)
        except member_models.Member.DoesNotExist:
            logger.warning("Slack channel member (%s) not found for %s team. Time to sync Slack users?",
                member_slack_id,
                team.slug)
        else:
            a_subscription, subscription_created = tag_models.Subscription.objects.get_or_create(
                member=a_member,
                tag=tag
            )
            if subscription_created:
                logger.info("Auto subscribed @%s from %s team to %s topic", a_member.username, team.slug, tag.slug)
            else:
                logger.info("@%s already subscribed to %s's %s topic", a_member.username, team.slug, tag.slug)

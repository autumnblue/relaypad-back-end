import pytz

# For whatever reason, slack's API lists timezone names as something that pytz doesn't understand.
# This dataset provides the translation. This data is borrowed from
# datetime_tz. That library doesn't appear to have an API to provide this
# functionality.
_tznames = {
    u'Alaska Daylight Time': 'US/Alaska',
    u'Alaska Standard Time': 'US/Alaska',
    u'Alpha Time Zone': 'Etc/GMT-1',
    u'Atlantic Daylight Time': 'America/Halifax',
    u'Atlantic Standard Time': 'America/Halifax',
    u'Australian Central Daylight Time': 'Australia/Adelaide',
    u'Australian Central Standard Time': 'Australia/Adelaide',
    u'Australian Eastern Daylight Time': 'Australia/Sydney',
    u'Australian Eastern Standard Time': 'Australia/Sydney',
    u'Australian Western Daylight Time': 'Australia/West',
    u'Australian Western Standard Time': 'Australia/West',
    u'Bravo Time Zone': 'Etc/GMT-2',
    u'Brasilia Time': 'Etc/GMT-3',
    u'British Summer Time': 'Europe/London',
    u'Central Daylight Time': 'US/Central',
    u'Central European Daylight Time': 'Etc/GMT+2',
    u'Central European Summer Time': 'Etc/GMT+2',
    u'Central European Time': 'Etc/GMT+1',
    u'Central Standard Time': 'US/Central',
    u'Charlie Time Zone': 'Etc/GMT-2',
    u'Christmas Island Time': 'Indian/Christmas',
    u'Coordinated Universal Time': "UTC",
    u'Delta Time Zone': 'Etc/GMT-2',
    u'Eastern Daylight Time': 'US/Eastern',
    u'Eastern European Daylight Time': 'Etc/GMT+3',
    u'Eastern European Summer Time': 'Etc/GMT+3',
    u'Eastern European Time': 'Etc/GMT+2',
    u'Eastern Standard Time': 'US/Eastern',
    u'Echo Time Zone': 'Etc/GMT-2',
    u'Foxtrot Time Zone': 'Etc/GMT-6',
    u'Golf Time Zone': 'Etc/GMT-7',
    u'Greenwich Mean Time': "UTC",
    u'Hawaii Daylight Time': 'Pacific/Honolulu',
    u'Hawaii Standard Time': 'Pacific/Honolulu',
    u'Hawaii-Aleutian Daylight Time': 'Pacific/Honolulu',
    u'Hawaii-Aleutian Standard Time': 'Pacific/Honolulu',
    u'Hotel Time Zone': 'Etc/GMT-8',
    u'India Time Zone': 'Etc/GMT-9',
    u'Irish Summer Time': 'Europe/Dublin',
    u'Kilo Time Zone': 'Etc/GMT-10',
    u'Lima Time Zone': 'Etc/GMT-11',
    u'Mike Time Zone': 'Etc/GMT-12',
    u'Moscow Daylight Time': 'Europe/Moscow',
    u'Moscow Standard Time': 'Europe/Moscow',
    u'Mountain Daylight Time': 'US/Mountain',
    u'Mountain Standard Time': 'US/Mountain',
    u'Newfoundland Daylight Time': 'America/St_Johns',
    u'Newfoundland Standard Time': 'America/St_Johns',
    u'Norfolk (Island) Time': 'Pacific/Norfolk',
    u'November Time Zone': 'Etc/GMT+1',
    u'Oscar Time Zone': 'Etc/GMT+2',
    u'Pacific Daylight Time': 'US/Pacific',
    u'Pacific Standard Time': 'US/Pacific',
    u'Papa Time Zone': 'Etc/GMT+3',
    u'Quebec Time Zone': 'Etc/GMT+4',
    u'Romeo Time Zone': 'Etc/GMT+5',
    u'Sierra Time Zone': 'Etc/GMT+6',
    u'Tango Time Zone': 'Etc/GMT+7',
    u'Uniform Time Zone': 'Etc/GMT+8',
    u'Victor Time Zone': 'Etc/GMT+9',
    u'Western Daylight Time': 'Australia/West',
    u'Western European Daylight Time': 'Etc/GMT+1',
    u'Western European Summer Time': 'Etc/GMT+1',
    u'Western European Time': "UTC",
    u'Western Standard Time': 'Australia/West',
    u'Whiskey Time Zone': 'Etc/GMT+10',
    u'X-ray Time Zone': 'Etc/GMT+11',
    u'Yankee Time Zone': 'Etc/GMT+12',
    u'Zulu Time Zone': "UTC"
}

def slack_tzinfo(name):
    """Load a tzinfo object based on the long tzname"""
    try:
        return pytz.timezone(_tznames[name])
    except KeyError:
        return None

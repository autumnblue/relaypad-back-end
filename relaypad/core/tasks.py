import logging

import cloudinary.api
from relaypad.celery import app
import member.models as member_models
import team.models as team_models
import photo.service as photo_service

logger = logging.getLogger(__name__)

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def upload_avatar_photo(self, member_id, photo_url):
    logger.info("uploading avatar photo for member %d", member_id)

    member = member_models.Member.objects.get(id=member_id)

    try:
        member.avatar_photo = photo_service.create_photo_from_path(photo_url)
    except cloudinary.api.Error as e:
        logger.exception("Failed to upload")
        raise self.retry()

    member.save()

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def upload_team_logo(self, team_id, photo_url):
    logger.info("uploading team logo for team %d", team_id)

    team = team_models.Team.objects.get(id=team_id)

    try:
        team.logo = photo_service.create_photo_from_path(photo_url)
    except cloudinary.api.Error as e:
        logger.exception("Failed to upload")
        raise self.retry()

    team.save()

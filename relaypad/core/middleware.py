import logging

import member.models

logger = logging.getLogger(__name__)


class RelayPadMembershipMiddleware(object):
    """ Custom middleware that sets current member info for all authenticated requests.

    Takes advantages of django middleware functionality to reference existing user
    and make Member information available to views. In practice this is only used for
    internal purposes (like browsable django rest framework API).

    User facing functionality relies on REST API auth using auth_token,
    which adds membership information to redux state.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        # Attempt to get an active membership for user.
        if not request.user.is_anonymous():
            try:
                a_member = member.models.Member.objects.filter(
                    user=request.user,
                    status=member.models.Member.ACTIVE
                ).first()
            except member.models.Member.DoesNotExist:
                logger.error(
                    "Couldn't find active team membership for this user.")
                a_member = None

            request.user.current_membership = a_member

        return self.get_response(request)

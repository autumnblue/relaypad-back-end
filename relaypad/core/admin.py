from django.contrib import admin
from django.contrib.auth.admin import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

import member.admin

class UserAdmin(BaseUserAdmin):
    list_display = ('username', 'last_name', 'first_name', 'is_superuser', 'is_staff')
    list_filter = ('groups', 'is_active', 'is_superuser')
    ordering = ('last_name', 'first_name')

    # Customize form
    fieldsets = (
        (None, {
            'fields': ('username', 'email', ('first_name', 'last_name'), 'password'),
        }),
        ('Advanced information', {
            'classes': ('collapse',),
            'description': 'This section is for reference purposes only at present.',
            'fields': ('is_staff', 'is_superuser', 'date_joined', 'last_login', 'user_permissions', 'groups'),
        }),
    )
    inlines = (member.admin.MembersInline,)

# Register our version of user admin model.
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

import logging
import requests
from django.conf import settings
from rest_framework import status

logger = logging.getLogger(__name__)

def get_user_slack_access_token(member_slack_id):
    """
    Retrieves the slack access token stored in auth0 for the specified slack user.
    Currently slack app tokens are tied to the slack user that completed the installation.
    """
    audience = settings.AUTH0_MGMT_AUDIENCE
    auth0_user_id_formatter = 'oauth2%7Cadd-to-slack%7C{member_slack_id}'
    auth0_access_token = _get_auth0_access_token()

    # Request user information from Auth0.
    response = requests.get(
        audience + 'users/' + auth0_user_id_formatter.format(member_slack_id=member_slack_id),
        headers={'Authorization': 'Bearer ' + auth0_access_token},
        timeout=2
    )

    response.raise_for_status()
    if response.status_code == requests.codes.ok:
        # Parse user information returned from Auth0
        for identity in response.json()['identities']:
            if identity['connection'] == 'add-to-slack':
                return identity['access_token']
            else:
                raise Exception["No slack access code returned from auth0", response]
    else:
        raise Exception("Unknown response from auth0", response)


def _get_auth0_access_token():
    """
    Retrieves an access_token that will be used to access Auth0's management API.
    This token will be used to retrieve user specific information that Auth0 is storing.
    """
    audience = settings.AUTH0_MGMT_AUDIENCE
    mgmt_api_url = settings.AUTH0_MGMT_API_URL
    mgmt_client_id = settings.AUTH0_MGMT_NON_INTERACTIVE_CLIENT_ID
    mgmt_client_secret = settings.AUTH0_MGMT_NON_INTERACTIVE_CLIENT_SECRET

    payload = {
        'audience': audience,
        'client_id': mgmt_client_id,
        'client_secret': mgmt_client_secret,
        'grant_type': 'client_credentials'
    }

    response = requests.post(mgmt_api_url, data=payload, timeout=2)
    if (response.status_code == requests.codes.ok):
        return response.json()['access_token']
    else:
        response.raise_for_status()

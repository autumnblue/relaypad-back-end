import logging
import jwt
from nameparser import HumanName
from rest_framework import authentication
from rest_framework import exceptions
from django.conf import settings
from django.contrib.auth.models import User

import member.models as member_models
import member.service as member_service
import team.models as team_models
import photo.service as photo_service
from core import tasks

logger = logging.getLogger(__name__)


class RelayPadJWTAuthentication(authentication.BaseAuthentication):
    """
    JWT (JSON Web Token) based authentication. RelayPad offloads the authentication flow to Auth0, which returns to
    client an RS256 signed JWT after authorization. Embedded in this token is user and team member informaton.
    This JWT should be included in all API request headers:
        Authorization = Bearer <token>
    """

    def authenticate(self, request):
        try:
            token = request.META['HTTP_AUTHORIZATION'].split('Bearer ')[1]
        except (KeyError, IndexError) as err:
            logger.info("Invalid Authorization header provided")
            raise exceptions.AuthenticationFailed(
                "Invalid Authorization header.")

        try:
            parsed_token_payload = _parse_token_payload(
                jwt.decode(
                    token,
                    settings.AUTH0_CERT,
                    algorithms=['RS256'],
                    audience=settings.AUTH0_CLIENT))
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed("Expired token")
        except jwt.InvalidTokenError:
            logger.exception("bad auth token")
            raise exceptions.AuthenticationFailed("Invalid token provided")

        try:
            member = member_models.Member.objects.select_related('user', 'team', 'avatar_photo').get(
                user__username=member_service.get_django_username(
                    parsed_token_payload['slack_id'],
                    parsed_token_payload['team']['slack_id']),
                team__slug=parsed_token_payload['team']['slug'])
        except member_models.Member.DoesNotExist:
            # Since JWT is verified by now, we're safe to create a membership with information
            # provided. This except block will retrieve/create the team and user entities needed
            # to authenticate this request with a valid membership.
            logger.info("No membership found for user (%s) in team (%s)." %
                        (parsed_token_payload['username'], parsed_token_payload['team']['slug']))

            team = _get_or_create_team(parsed_token_payload)
            member = _create_membership(parsed_token_payload, team)

            if not member:
                raise exceptions.AuthenticationFailed("Problem creating new membership")

        # Want to confirm we've correct setup membership based on token payload.
        _validate_configuration(member, parsed_token_payload)

        if member.is_first_login():
            member_service.first_login_for_member(member)

        # Make current membership and team available in requests via user object.
        member.user.current_membership = member
        return (member.user, None)


def _get_or_create_team(parsed_token_payload):
    try:
        team = team_models.Team.objects.get(
            slug=parsed_token_payload['team']['slug'])
    except team_models.Team.DoesNotExist:
        # TODO: prevent creation of teams with reserved slugs (e.g. "pricing" "legal", etc.)
        team = team_models.Team.objects.create(
            slug=parsed_token_payload['team']['slug'],
            name=parsed_token_payload['team']['name'],
            slack_id=parsed_token_payload['team']['slack_id'])

        logger.info("Created %s team (slug=%s)",
            parsed_token_payload['team']['name'],
            parsed_token_payload['team']['slug'])

    return team

def _create_membership(parsed_token_payload, team):
    return member_service.create_membership(parsed_token_payload, team)

def _parse_token_payload(payload):
    return {
        'slack_id': payload['user_id'].split("|")[2],
        'username': payload['nickname'],
        'email': payload['email'],
        'first_name': HumanName(payload['name']).first,
        'last_name': HumanName(payload['name']).last,
        'avatar_url': payload.get('avatar', None),
        'team': {
            'slack_id': payload['team']['id'],
            'slug': payload['team']['domain'],
            'name': payload['team']['name'],
            'logo_url': payload['team'].get('logo', None)
        }
    }

def _validate_configuration(member, parsed_token_payload):
    """
    Validates membership and populates required data that may be currently missing
    e.g. will trigger upload of avatar or team logo if it hasn't been done previously.
    @param: member The actual membership object of this request.
    @param: parsed_token_payload The data contained in the JWT for this request.
    """
    if member.status != member_models.Member.ACTIVE:
        raise exceptions.AuthenticationFailed("Inactive team member")

    if not member.avatar_photo:
        tasks.upload_avatar_photo.delay(member.id, parsed_token_payload.get('avatar_url'))

    if not member.team.logo:
        tasks.upload_team_logo.delay(member.team.id, parsed_token_payload['team'].get('logo_url'))

    if not member.slack_id:
        # Extract member slack_id from JWT.
        # TODO: Can be removed in the future since we're now populating member
        # slack_id at time of team creatino. Query to confirm this is safe to remove:
        # `if select count(*) from member where slack_id is null == 0: remove this`
        member.slack_id = parsed_token_payload['slack_id']
        member.save()

    if not member.team.slack_id:
        # Extract team slack_id from JWT.
        # TODO: Can be removed in the future since we're now populating team
        # slack_id at time of team creatino. Query to confirm this is safe to remove:
        # `if select count(*) from team where slack_id is null == 0: remove this`
        member.team.slack_id = parsed_token_payload['team']['slack_id']
        member.team.save()

"""
Helper classes and functions to optimize Django Rest Framework serializers
e.g. customize output of a specific field.
"""
import logging

from rest_framework import serializers

logger = logging.getLogger(__name__)

class ReactionListField(serializers.Field):

    def to_representation(self, obj):
        """
        Returns a dictionary summarizing existing reactions, keyed by reaction type
        """
        current_member = self.context['request'].user.current_membership
        existing_reactions = {}

        for reaction in obj.all():
            if reaction.type not in existing_reactions:
                # This reaction isn't accounted for yet, create new reaction summary.
                existing_reactions[reaction.type] = {
                    'type': reaction.type,
                    'count': 1,
                    'names' : [],
                    'current_member_reaction': reaction.member == current_member
                }
                if reaction.member != current_member:
                    # Only add non current member information.
                    if reaction.member.first_name is None or reaction.member.last_name is None:
                        existing_reactions[reaction.type]['names'].append(
                            '@{}'.format(reaction.member.username))
                    else:
                        existing_reactions[reaction.type]['names'].append(
                            '%s %s' %(reaction.member.first_name, reaction.member.last_name))
            else:
                # Update existing reaction summary.
                reaction_summary = existing_reactions[reaction.type]
                for key in reaction_summary:
                    if key == 'count':
                        reaction_summary[key] = reaction_summary[key] + 1
                    if key == 'names' and reaction.member != current_member:
                        # Only add non current member information.
                        if reaction.member.first_name is None or reaction.member.last_name is None:
                            reaction_summary[key].append('@{}'.format(reaction.member.username))
                        else:
                            reaction_summary[key].append(
                                '%s %s' %(reaction.member.first_name, reaction.member.last_name))
                    if key == 'current_member_reaction' and reaction_summary[key] != True:
                        reaction_summary[key] = reaction.member == current_member

        return list(existing_reactions.values())

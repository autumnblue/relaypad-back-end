import os
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from core import views
import post.views
import tag.views
import team.views
import member.views
import comment.views
import bookmark.views
import slack.views
import preferences.views

router = routers.DefaultRouter()

# Register endpoints associated with Django Rest Framework ViewSets
router.register(r'^v1/posts', post.views.PostViewSet, base_name='posts')
router.register(r'^v1/tags', tag.views.TagViewSet, base_name='tags')
router.register(r'^v1/teams', team.views.TeamViewSet, base_name='teams')
router.register(r'^v1/members', member.views.MemberViewSet, base_name='members')
router.register(r'^v1/comments', comment.views.CommentViewSet, base_name='comments')

urlpatterns = [
    url(r'^v1/$', views.api_root),
    url(r'^v1/members/current/$', member.views.current_member, name='member-current'),
    url(r'^v1/notifications/$', member.views.notifications, name='notifications'),
    url(r'^v1/drafts/$', post.views.Drafts.as_view(), name='drafts'),
    url(r'^v1/notes/$', post.views.Notes.as_view(), name='notes'),
    url(r'^v1/bookmarks/$', bookmark.views.Bookmarks.as_view(), name='bookmarks'),
    url(r'^v1/subscriptions/$', tag.views.Subscriptions.as_view(), name='subscriptions'),
    url(r'^v1/search/$', post.views.Search.as_view(), name='search'),
    url(r'^v1/slack/listening/$', slack.views.event_listener),
    url(r'^v1/preferences/notifications/$', preferences.views.NotificationsView.as_view(), name='preferences-notifications'),
    url(r'^', include(router.urls)) # Include all endpoints constructed via Django Rest Framework ViewSets
]

if (os.environ['DEPLOY_ENVIRONMENT'] != 'production'):
    # Enable browsable API on non-production environments.
    dango_rest_framework = url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
    urlpatterns += (dango_rest_framework,)

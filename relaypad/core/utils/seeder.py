import logging
import sys, os

from django.contrib.auth.models import User
from core import models

logger = logging.getLogger(__name__)

class Seeder(object):
    """
    A class designed to simplify the creation of a demonstration team on RelayPad. The ultimate output
    being a team that can be logged in to, to fully demonstrate and test the product.
    """

    TEAM_NAME = 'Delos Destinations'
    TEAM_SLUG = 'delos'
    TEAM_MEMBERS = {
        'ford': {
            'first_name': 'Robert',
            'last_name': 'Ford',
            'username': 'ford',
            'role': 'Creative Director',
            'location': 'Westworld',
            'bio': 'Co-creator and designer of Westworld. Constantly working to make the hosts more lifelike and recently shipped reveries code.'
        },
        'bernard': {
            'first_name': 'Bernard',
            'last_name': 'Lowe',
            'username': 'bernard',
            'role': 'Head of Engineering',
            'location': 'Westworld',
            'bio': 'Took over engineering from Arnold and works very closely with @ford.'
        },
        'stubbs': {
            'first_name': 'Ashley',
            'last_name': 'Stubbs',
            'username': 'stubbs',
            'role': 'Head of Security',
            'location': 'Westworld',
            'bio': 'In charge of monitoring host and human interactions and ensuring the safety of the guests.'
        },
        'elsie': {
            'first_name': 'Elsie',
            'last_name': 'Hughes',
            'username': 'elsie',
            'role': 'Engineer',
            'location': 'Westworld',
            'bio': 'Graduated from MIT and is now focused on remedying odd behavior in hosts, when problems occur.'
        },
        'felix': {
            'first_name': 'Felix',
            'last_name': 'Lutz',
            'username': 'felix',
            'role': 'Technician',
            'location': 'Westworld',
            'bio': 'Repairs damaged hosts. Works long hours and that results in limited free time.'
        },
        'theresa': {
            'first_name': 'Theresa',
            'last_name': 'Cullen',
            'username': 'theresa',
            'role': 'Senior VP, Quality Assurance',
            'location': 'Westworld',
            'bio': 'Focused on efficiency and keeping the trains running on time.'
        },
        'bryce': {
            'first_name': 'Bryce',
            'last_name': 'Leonard',
            'username': 'bryce',
            'role': 'Director, North America Group Sales',
            'location': 'Westworld',
            'bio': ''
        },
        'charlotte': {
            'first_name': 'Charlotte',
            'last_name': 'Hale',
            'username': 'charlotte',
            'role': 'Board of Directors, Delos Destinations',
            'location': 'Westworld',
            'bio': ''
        },
        'destin': {
            'first_name': 'Destin',
            'last_name': 'Levy',
            'username': 'destin',
            'role': 'Technician Team Lead',
            'location': 'Westworld Mesa Hub',
            'bio': ''
        },
        'lee': {
            'first_name': 'Lee',
            'last_name': 'Sizemore',
            'username': 'lee',
            'role': 'Narrative Lead',
            'location': 'Westworld',
            'bio': ''
        }


    }

    def create_westworld(self):
        """
        Creates a team for `Delos Destinations`, the fictional company behond `Westworld`.
        """
        logger.info("Creating Westworld...")
        self.team = self._create_team()

        if self.team:
            self._create_team_members(self.team)
        else:
            # Team wasn't created, defer to log messages and bail out.
            return

    def _create_team(self):
        if (self._is_team_available(self.TEAM_SLUG)):
            logger.info("Creating %s team..." %self.TEAM_NAME)
            try:
                return models.Team.objects.create(name=self.TEAM_NAME,
                                                  slug=self.TEAM_SLUG)
            except Exception as e:
                logger.error("Problem creating team: %s" %e)

        else:
            logger.info("A team with slug '%s' already exists. Delete it and re-run." %self.TEAM_SLUG)

    def _create_team_members(self, team):
        logger.info("Creating team members...")

        for username in self.TEAM_MEMBERS:
            self._populate_django_user_info(username)
            try:
                models.Member.objects.create(user=self.TEAM_MEMBERS[username]['user_object'],
                                             team=team,
                                             first_name=self.TEAM_MEMBERS[username]['first_name'],
                                             last_name=self.TEAM_MEMBERS[username]['last_name'],
                                             username=self.TEAM_MEMBERS[username]['username'],
                                             email='%s@delos.com' %self.TEAM_MEMBERS[username]['username'],
                                             role=self.TEAM_MEMBERS[username]['role'],
                                             bio=self.TEAM_MEMBERS[username]['bio'],
                                             location=self.TEAM_MEMBERS[username]['location'],
                                             twitter_handle=self.TEAM_MEMBERS[username]['username'],
                                             instagram_handle=self.TEAM_MEMBERS[username]['username'],
                                             github_handle=self.TEAM_MEMBERS[username]['username'])
            except Exception as e:
                logger.error("Problem creating member: %s" %e)

    def _populate_django_user_info(self, username):
        """
        Until OAuth solutions are integrated, team members must have a corresponding django auth user
        entry for authentication.

        TODO: remove this once team members can exist without having a corresponding django_auth user.
        """
        try:
            user_object = User.objects.get(username = username)
        except User.DoesNotExist:
            logger.info("Creating new user for %s..." %username)
            user_object = User.objects.create_user(username, '%s@example.com' %username, os.environ['DEMO_MEMBER_PASSWORD'])

        self.TEAM_MEMBERS[username]['user_object'] = user_object
        logger.info("Django user info now available = %s" %self.TEAM_MEMBERS[username])

    def _is_team_available(self, slug):
        try:
            models.Team.objects.get(slug=slug)
        except models.Team.DoesNotExist:
            return True
        return False

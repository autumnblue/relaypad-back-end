import factory
from faker import Faker

from django.contrib.auth.models import User
from core import models

# Utility to generate random fake content.
faker = Faker()

class DjangoUserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    @factory.sequence
    def username(n):
        username = faker.user_name()
        return '%s-%d' %(username, n)

    first_name = factory.LazyAttribute(lambda obj: faker.first_name())
    last_name = factory.LazyAttribute(lambda obj: faker.last_name())
    email = factory.LazyAttribute(lambda obj: '%s@example.com' %obj.username)

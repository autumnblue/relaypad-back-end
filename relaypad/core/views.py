import logging
import sys

from rest_framework import views, viewsets, generics, permissions, status, exceptions
from rest_framework.decorators import api_view, detail_route, list_route
from rest_framework.response import Response
from rest_framework.reverse import reverse

logger = logging.getLogger(__name__)

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'member-current': reverse(
            'member-current', request=request, format=format),
        'teams': reverse(
            'teams', request=request, format=format),
        'posts': reverse(
            'posts', request=request, format=format),
        'bookmarks': reverse(
            'bookmarks', request=request, format=format),
    })

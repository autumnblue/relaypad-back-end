from django.utils import timezone
from core.tests import AuthenticatedTestCase

import member.factories
from digest import service

class NotificationsTest(AuthenticatedTestCase):
    def setUp(self):
        self.member = member.factories.MemberFactory(timezone='Eastern Standard Time')
        self.client = self.createAuthenticatedClient(self.member)

    def test_check_subscribe_true(self):
        service.welcome_digest_for_member(self.member)

        response = self.client.get('/api/v1/preferences/notifications/')
        self.assertEquals(response.data['digest_email'], True)

    def test_check_subscribe_false(self):
        response = self.client.get('/api/v1/preferences/notifications/')
        self.assertEquals(response.data['digest_email'], False)

    def test_subscribe(self):
        response = self.client.post('/api/v1/preferences/notifications/', {'digest_email': True})

        self.member.refresh_from_db()
        self.assertLess(self.member.digest.last_sent_at, timezone.now())
        self.assertTrue(self.member.digest.email_enabled)

    def test_unsubscribe(self):
        service.welcome_digest_for_member(self.member)
        response = self.client.post('/api/v1/preferences/notifications/', {'digest_email': False})

        self.member.digest.refresh_from_db()
        self.assertFalse(self.member.digest.email_enabled)

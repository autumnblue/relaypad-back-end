import logging
from django.conf import settings
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from team.permissions import IsTeamMember
from digest import service
from member import models as member_models

logger = logging.getLogger(__name__)

class NotificationsView(APIView):
    permission_classes = [IsAuthenticated, IsTeamMember]

    def _default_settings(self):
        return {
            'digest_email': False,
            'digest_slack': False,
        }

    def _get_settings(self, request):
        settings = {}
        try:
            digest = self.request.user.current_membership.digest
        except member_models.Member.digest.RelatedObjectDoesNotExist:
            settings = self._default_settings()
        else:
            settings['digest_email'] = digest.email_enabled
            settings['digest_slack'] = digest.slack_enabled

        return settings


    def get(self, request, format=None):
        return Response(self._get_settings(request))

    def post(self, request, format=None):
        settings = self._get_settings(request)

        for a_setting in request.data:
            try:
                settings[a_setting] = request.data[a_setting]
            except KeyError:
                return Response({"error": "bad setting value"}, status=status.HTTP_400_BAD_REQUEST)

        service.subscribe_digest(
            self.request.user.current_membership,
            email_enabled=settings['digest_email'],
            slack_enabled=settings['digest_slack'])

        return Response(settings)

import logging
from django.contrib.auth.models import User
from django.conf import settings
from django.utils import timezone

import member.models as member_models
from post import service as post_service
from team import tasks as team_tasks
from digest import tasks as digest_tasks

logger = logging.getLogger(__name__)


def create_membership(member_info, team, **kwargs):
    """
    Creates a membership based on information provided. Source of information
    will be either JWT from auth.py or response from Slack API.
    """
    if kwargs.get("info_source") == "SLACK":
        # Indicates we're creating membership based on Slack API data.
        # Need to do additional parsing.
        member_info = extract_member_info_from_slack_response(member_info)

    try:
        django_user = User.objects.get(username=get_django_username(member_info['slack_id'], team.slack_id))
    except User.DoesNotExist:
        django_user = User.objects.create_user(
            username=get_django_username(member_info['slack_id'], team.slack_id),
            first_name=member_info.get('first_name', ''),
            last_name=member_info.get('last_name', ''),
            password=User.objects.make_random_password())
        logger.info("Created new Django user with username = %s", get_django_username(member_info['slack_id'], team.slack_id))

    try:
        member = member_models.Member.objects.create(
            user=django_user,
            team=team,
            first_name=member_info.get('first_name'),
            last_name=member_info.get('last_name'),
            email=member_info.get('email'),
            slack_id=member_info['slack_id'],
            username=member_info['username'])

        # Do some onboarding magic.
        post_service.create_welcome_note(member)
        # Temporarily disable sending Welcome email.
        # team_tasks.send_welcome_email.delay(team.id)

        return member
    except:
        logger.exception(
            "Problem creating membership for user (%s) in team (%s)", django_user.username, team)
        return None


def first_login_for_member(member):
    member.first_login_at = timezone.now()
    member.save()

    if settings.ENABLE_DIGEST and member.team.members.count() > 1:
        digest_tasks.welcome_digest.delay(member.id)


def get_django_username(user_slack_id, team_slack_id):
    """
    We make use of django.contrib.auth.models.User model and they have a 1:1 relationship to Member model.
    We set the username value to a unique combination of user and team information as returned by OAuth0
    e.g. Slack OAuth would be "oauth2|slack|<user_id>|<team_id>"
    """
    return "oauth2|slack|{}|{}".format(user_slack_id, team_slack_id)

def extract_member_info_from_slack_response(slack_response):
    """
    Return a dictionary of member values that we received from Slack.
    Created to handle the fact that attributes we receive from Slack varies by user
    and we can't blanketly update existing members with values.
    """
    logger.info("Extracting member info from slack response: %s", slack_response)

    member_info = {
        'username': slack_response['name'],
        'slack_id': slack_response['id'],
        'is_slack_admin': slack_response.get('is_admin', False),
        'timezone': slack_response.get('tz_label', None),
    }

    # Extract data that is not always returned.
    if slack_response['profile'].get('email') != None:
        member_info['email'] = slack_response['profile']['email']
    if slack_response['profile'].get('first_name') != None:
        member_info['first_name'] = slack_response['profile']['first_name']
    if slack_response['profile'].get('last_name') != None:
        member_info['last_name'] = slack_response['profile']['last_name']
    if slack_response['profile'].get('skype') != None:
        member_info['skype_handle'] = slack_response['profile']['skype']
    if slack_response['profile'].get('phone') != None:
        member_info['phone'] = slack_response['profile']['phone']

    return member_info

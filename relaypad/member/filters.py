import logging
import django_filters
from django.db.models import Q
from member import models as member_models

logger = logging.getLogger(__name__)

class MemberFilterSet(django_filters.rest_framework.FilterSet):
    startswith = django_filters.CharFilter(method='filter_startswith')

    def filter_startswith(self, queryset, name, value):
        """
        ?startswith=<string> queries will return a list of matching users.
        """
        return queryset.filter(
            Q(first_name__istartswith=value) |
            Q(last_name__istartswith=value) |
            Q(username__istartswith=value))

    class Meta:
        model = member_models.Member
        fields = ['startswith']

import factory
from faker import Faker

from member import models

faker = Faker()


class MemberFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Member

    user = factory.SubFactory('core.factories.DjangoUserFactory')
    team = factory.SubFactory('team.factories.TeamFactory')
    first_name = factory.LazyAttribute(lambda obj: faker.first_name())
    last_name = factory.LazyAttribute(lambda obj: faker.last_name())
    username = factory.LazyAttribute(lambda obj: faker.user_name())
    avatar_photo = factory.SubFactory('photo.factories.PhotoFactory')

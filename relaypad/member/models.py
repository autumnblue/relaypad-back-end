import os
import binascii

from django.db import models
from django.contrib.auth.models import User

class Member(models.Model):
    ACTIVE = 'ACTIVE'
    INACTIVE = 'INACTIVE'
    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (INACTIVE, 'Inactive'),
    )

    team = models.ForeignKey('team.Team', on_delete=models.CASCADE, related_name='members')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='memberships')
    slack_id = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=ACTIVE)
    onboarded = models.BooleanField(default=False)
    first_name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    last_name = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    username = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    location = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    joined_at = models.DateTimeField(null=True, blank=True)
    role = models.CharField(max_length=100, null=True, blank=True)
    bio = models.TextField(null=True, blank=True)
    facebook_handle = models.CharField(max_length=50, null=True, blank=True)
    github_handle = models.CharField(max_length=50, null=True, blank=True)
    instagram_handle = models.CharField(max_length=50, null=True, blank=True)
    snapchat_handle = models.CharField(max_length=50, null=True, blank=True)
    twitter_handle = models.CharField(max_length=50, null=True, blank=True)
    dribbble_handle = models.CharField(max_length=50, null=True, blank=True)
    linkedin_handle = models.CharField(max_length=50, null=True, blank=True)
    medium_handle = models.CharField(max_length=50, null=True, blank=True)
    skype_handle = models.CharField(max_length=50, null=True, blank=True)
    personal_website = models.URLField(null=True, blank=True)
    timezone = models.CharField(max_length=100, null=True, blank=True)
    is_slack_admin = models.BooleanField(default=False)

    avatar_photo = models.ForeignKey('photo.Photo', on_delete=models.CASCADE, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    first_login_at = models.DateTimeField(null=True)

    class Meta:
        db_table = 'member'
        unique_together = (('team', 'user'),
                           ('team', 'username'))

    def __str__(self):
        return "@%s (%s)" %(self.username, self.team.name)

    def is_first_login(self):
        return self.first_login_at is None

    @property
    def activity_object_serializer_class(self):
        """
        Stream integration code uses this to programmatically get serializer
        for this model.
        """
        from .serializers import MinimalMemberSerializer
        return MinimalMemberSerializer

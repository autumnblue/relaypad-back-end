import logging
from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse

import core.tests
from member import models
from member import service
from member import factories

logger = logging.getLogger(__name__)

class MemberTestCase(core.tests.AuthenticatedTestCase):
    """
    Tests API endpoints related to team members.
    """

    def setUp(self):
        super(MemberTestCase, self).setUp()

        # Add inactive member to team.
        inactive_member = factories.MemberFactory(
            team=self.member.team,
            status=models.Member.INACTIVE
        )

        # Add member to another team (tests confirm we don't have cross team retrieval)
        self.other_team_member = factories.MemberFactory()

    def test_current_member_retrieval(self):
        response = self.client.get(reverse('member-current'))
        self.assertEquals(response.data['username'], self.member.username)

    def test_current_member_update(self):
        response = self.client.put(reverse('member-current'), {'username': 'reject'})
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_current_member_deletion(self):
        response = self.client.delete(reverse('member-current'))
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_member_list(self):
        response = self.client.get('/api/v1/members/')
        self.assertEquals(
            response.data['count'],
            models.Member.objects.filter(
                team=self.member.team,
                status=models.Member.ACTIVE).count())

    def test_member_retrieval(self):
        response = self.client.get('/api/v1/members/%s/' % self.member.username)
        self.assertEquals(response.data['username'], self.member.username)

    def test_member_edit(self):
        response = self.client.patch('/api/v1/members/%s/' % self.member.username,
                                     {'member': {'role': 'API unit tester'}},
                                     format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_edit_other_member(self):
        other_member = factories.MemberFactory(team=self.member.team)
        response = self.client.patch('/api/v1/members/%s/' % other_member.username,
                                     {'member': {'role': 'Change to be rejected'}},
                                     format='json')
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_edit_other_teams_member(self):
        response = self.client.patch('/api/v1/members/%s/' % self.other_team_member.username,
                                     {'member': {'role': 'Change to be rejected'}},
                                     format='json')
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_member_deletion(self):
        response = self.client.delete('/api/v1/members/%s/' % self.member.username)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_at_mention_filter(self):
        response = self.client.get('/api/v1/members/?startswith=%s' % self.member.first_name[:2])
        self.assertEquals(response.data['results'][0]['id'], self.member.id)

    def test_cross_team_at_mention_prevention(self):
        strange_team_member = factories.MemberFactory()
        response = self.client.get('/api/v1/members/?startswith=%s' % strange_team_member.first_name[:2])
        for a_member in response.data['results']:
            self.assertNotEqual(a_member['id'], strange_team_member.id)

class MemberFirstLoginTest(TestCase):
    def setUp(self):
        self.member = factories.MemberFactory()

    def test(self):
        self.assertTrue(self.member.is_first_login())

        service.first_login_for_member(self.member)

        self.member.refresh_from_db()

        self.assertFalse(self.member.is_first_login())



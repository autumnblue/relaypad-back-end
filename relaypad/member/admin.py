from django.contrib import admin

from member import models


@admin.register(models.Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('username', 'team', 'first_name', 'last_name', 'email')
    ordering = ('username',)

    readonly_fields = (
        'slack_id',
        'timezone',
        'joined_at',
        'created_at',
        'updated_at', )


class MembersInline(admin.StackedInline):
    model = models.Member
    extra = 0  # Determines if any extra entries are shown.
    can_delete = True
    verbose_name = "Membership"
    verbose_name_plural = 'Memberships'

    readonly_fields = (
        'slack_id',
        'timezone',
        'joined_at',
        'created_at',
        'updated_at', )

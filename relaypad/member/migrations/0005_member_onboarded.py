# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-13 17:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('member', '0004_auto_20170124_1956'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='onboarded',
            field=models.BooleanField(default=False),
        ),
    ]

import logging

from rest_framework import viewsets, mixins, exceptions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.permissions import IsAuthenticated
from stream_django.feed_manager import feed_manager
from stream_django.enrich import Enrich
from member import serializers
from member import models
from member import filters
from member.permissions import IsOwnerOrReadOnly

logger = logging.getLogger(__name__)

@api_view()
def current_member(request):
    """
    Helper view to retrieve information about current member.
    """
    serializer = serializers.MemberSerializer(request.user.current_membership)
    return Response(serializer.data)

class MemberViewSet(mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    """
    Viewset for handling member endpoints.
    """
    lookup_field = 'username'
    serializer_class = serializers.MemberSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    filter_class = filters.MemberFilterSet

    def get_queryset(self):
        return models.Member.objects.filter(
            team=self.request.user.current_membership.team,
            status=models.Member.ACTIVE)

    def update(self, request, *args, **kwargs):
        """
        Overriding to allow data passed to PUT and PATCH requests to be
        wrapped inside a 'team' object. We do something similar for 'team'
        updates.
        TODO: This should probably be implemented via http://www.django-rest-framework.org/api-guide/serializers/#to_internal_valueself-data
        """
        try:
            member = models.Member.objects.get(
                username=kwargs.pop('username', None),
                team=request.user.current_membership.team)
        except models.Member.DoesNotExist:
            raise exceptions.NotFound('The member specified was not found.')

        # Have to explicitly call due to object retrieval being overridden
        self.check_object_permissions(self.request, member)

        if 'member' in request.data:
            data = request.data['member']
        else:
            data = request.data

        serializer = self.get_serializer(member, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

@api_view(['GET', 'PUT'])
def notifications(request):
    """
    Return notifications for this user via stream (getstream.io) integration.
    Much of the logic here is taken from https://github.com/GetStream/stream-django/issues/38
    """
    # TODO: figure out pagination of notifications.
    notification_feed = feed_manager.get_notification_feed(request.user.current_membership.id)

    if (request.META['REQUEST_METHOD'] == 'PUT'):
        # If "PUT" we want to mark notifications as seen.
        # TODO: better way to handle this?
        return Response(_serialize_notifications(notification_feed.get(limit=10, mark_seen=True, mark_read=True)))
    else:
        return Response(_serialize_notifications(notification_feed.get(limit=10)))



def _serialize_activity_object(obj):
    """
    This method looks for activity_object_serializer_class property on
    object passed to it, property will return the appropriate serializer.
    If property not available, returns string representation of object.
    """
    if hasattr(obj, 'activity_object_serializer_class'):
        obj = obj.activity_object_serializer_class(obj).data
    else:
        obj = str(obj)
    return obj

def _serialize_notifications(notification_feed_data):
    """
    Parses notifications returned from getstream.io into format that
    can be returned to API clients.

    Recommended reading: Notification feeds in stream are "aggregated" by a formula
    specified by admin (can aggregate by "verb" and "time range" etc.)
    This is to support behavior similar to Facebook notifications, such as
    "Jane and 5 others liked blank". This complicates parsing of information, a "notification"
    can contain an array of n "activities", hence we need to handle that structure.

    TODO: Is it possible to get a flatter structure from stream?
    """
    response = {}
    response['unread_count'] = notification_feed_data['unread']
    response['unseen_count'] = notification_feed_data['unseen']

    logger.debug("Raw notification data returned = %s", notification_feed_data)
    notifications = []
    for notification in notification_feed_data['results']:
        # Parse notifications returned from stream.
        serialized_notification = {}
        serialized_notification['id'] = notification['id']
        serialized_notification['is_read'] = notification['is_read']
        serialized_notification['is_seen'] = notification['is_seen']
        serialized_notification['created_at'] = notification['created_at']
        serialized_activities = []
        enriched_notification_activities = Enrich().enrich_activities(notification['activities'])
        for enriched_activity in enriched_notification_activities:
            # Parse individual activites associated with this notification.
            serialized_activity = {}
            serialized_activity['verb'] = enriched_activity['verb']
            serialized_activity['actor'] = _serialize_activity_object(enriched_activity['actor'])
            serialized_activity['object'] = _serialize_activity_object(enriched_activity['object'])
            serialized_activities.append(serialized_activity)
            serialized_notification['activities'] = serialized_activities
        notifications.append(serialized_notification)

    response['notifications'] = notifications
    return response

import logging

from rest_framework import serializers
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from member import models
import post.models
import team.serializers

logger = logging.getLogger(__name__)


class MemberSerializer(serializers.ModelSerializer):
    team = team.serializers.TeamSerializer(read_only=True)
    profile_uri = serializers.SerializerMethodField()
    avatar_photo_id = serializers.SerializerMethodField()
    post_count = serializers.SerializerMethodField()
    is_admin = serializers.SerializerMethodField()

    def get_profile_uri(self, obj):
        return "/%s/@%s" % (obj.team.slug, obj.username)

    def get_avatar_photo_id(self, obj):
        if obj.avatar_photo:
            return obj.avatar_photo.public_id
        else:
            return None

    def get_is_admin(self, obj):
        # TODO: this should be based on a flag on member model.
        # For now, first team member is the admin.
        return obj.team.members.filter(status=models.Member.ACTIVE).first().username == obj.username

    def get_post_count(self, obj):
        return post.models.Post.objects.filter(member=obj, status=post.models.Post.PUBLISHED).count()

    class Meta:
        model = models.Member
        exclude = ('user', 'slack_id', 'avatar_photo', 'created_at', 'updated_at', 'timezone')
        lookup_field = 'username'


class SummaryMemberSerializer(MemberSerializer):
    class Meta:
        model = models.Member
        fields = (
            'id',
            'first_name',
            'last_name',
            'username',
            'is_admin',
            'role',
            'email',
            'profile_uri',
            'avatar_photo_id',)


class MinimalMemberSerializer(MemberSerializer):
    class Meta:
        model = models.Member
        fields = (
            'first_name',
            'last_name',
            'profile_uri',
            'avatar_photo_id',
            'username',)

"""Utility functions for web-frontend URLs"""
from django.conf import settings

import urllib.parse

def frontend_url(path):
    return urllib.parse.urljoin(settings.WEB_BASE_URL, path)

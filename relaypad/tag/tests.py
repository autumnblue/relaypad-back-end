import random
from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse

import core.tests
from tag import factories
from tag import models
from post import models as post_models
from post import factories as post_factories


class TagTestCase(core.tests.AuthenticatedTestCase):
    """
    Content on RelayPad can be tagged, this class tests endpoints relating to those capabilities.
    """

    def setUp(self):
        super(TagTestCase, self).setUp()

    def test_tag_creation(self):
        response = self.client.post('/api/v1/tags/',
                                    {'name': 'Unit test created tag'})
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_all_tag_retrieval(self):
        a_tag = factories.TagFactory(team=self.member.team)
        response = self.client.get('/api/v1/tags/')
        self.assertEquals(response.data[0]['slug'], a_tag.slug)

    def test_tag_retrieval(self):
        a_tag = factories.TagFactory(team=self.member.team)
        response = self.client.get('/api/v1/tags/%s/' % a_tag.slug)
        self.assertEquals(response.data['slug'], a_tag.slug)

    def test_other_teams_tag_retrieval(self):
        a_tag = factories.TagFactory()
        response = self.client.get('/api/v1/tags/%s/' % a_tag.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_tag_published_post_count(self):
        a_tag = factories.TagFactory(team=self.member.team)
        a_post = post_factories.PostFactory(team=self.member.team, status=post_models.Post.PUBLISHED)
        factories.PostTagFactory(tag=a_tag, post=a_post)
        response = self.client.get('/api/v1/tags/%s/' % a_tag.slug)
        self.assertEquals(response.data['published_post_count'], 1)

    def test_tag_deletion(self):
        tag = factories.TagFactory(team=self.member.team)
        response = self.client.delete('/api/v1/tags/%s/' % tag.slug)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_other_teams_tag_deletion(self):
        tag = factories.TagFactory()
        response = self.client.delete('/api/v1/tags/%s/' % tag.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

class SubscriptionTestCase(core.tests.AuthenticatedTestCase):
    """
    Team members can opt to follow a specific tag/topic on RelayPad, perhaps to receive
    weekly summary of a particular topic. This class tests subscription management endpoints.
    """

    def test_subscribing_to_tag(self):
        tag = factories.TagFactory(team=self.member.team)
        response = self.client.post('/api/v1/tags/%s/subscribe/' % tag.slug)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_subscribing_to_invalid_tag(self):
        response = self.client.post('/api/v1/tags/%s/subscribe/' %
                                    random.randint(10000, 100000))
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_subscribing_to_other_teams_tag(self):
        a_tag = factories.TagFactory()
        response = self.client.post('/api/v1/tags/%s/subscribe/' % a_tag.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_subscription_deletion(self):
        subscription = factories.SubscriptionFactory(member=self.member)
        response = self.client.delete('/api/v1/tags/%s/subscribe/' %
                                      subscription.tag.slug)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_subscription_retrieval(self):
        subscription_count = models.Subscription.objects.filter(
            member=self.member).count()
        response = self.client.get(reverse('subscriptions'))
        self.assertEquals(response.data['count'], subscription_count)

    def test_retrieve_all_tag_followers(self):
        subscription = factories.SubscriptionFactory()
        subscription = factories.SubscriptionFactory(tag=subscription.tag)
        response = self.client.get('/api/v1/tags/%s/subscribers/' %
                                   subscription.tag.slug)
        self.assertEquals(response.data['count'], 2)

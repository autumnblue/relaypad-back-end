import factory
from faker import Faker

from tag import models

# Utility to generate random fake content.
faker = Faker()


class TagFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Tag

    name = factory.LazyAttribute(lambda obj: faker.slug())
    team = factory.SubFactory('team.factories.TeamFactory')
    slack_channel_id = factory.LazyAttribute(lambda obj: faker.slug())


class SubscriptionFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Subscription

    tag = factory.SubFactory('tag.factories.TagFactory')
    member = factory.SubFactory('member.factories.MemberFactory')


class PostTagFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.PostTag

    tag = factory.SubFactory('tag.factories.TagFactory')
    post = factory.SubFactory('post.factories.PostFactory')

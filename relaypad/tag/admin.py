from django.contrib import admin

from tag import models


@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    readonly_fields = (
        'slack_channel_id',
        'created_at',
        'updated_at', )



@admin.register(models.Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    pass

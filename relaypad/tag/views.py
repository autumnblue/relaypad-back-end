from django.shortcuts import render
from rest_framework import views, viewsets, generics, status, exceptions
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from tag import serializers
from tag import models


class TagViewSet(viewsets.ModelViewSet):
    """
    Viewset for handling all Tag specific actions.
    """
    serializer_class = serializers.FullTagSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return models.Tag.objects.filter(
            team=self.request.user.current_membership.team)

    def perform_create(self, serializer):
        serializer.save(team=self.request.user.current_membership.team)

    def list(self, request, *args, **kwargs):
        """
        Customizing list method to bypass auto pagination, want to return
        all team tags when GET /teams is called (for tag management).
        """
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @detail_route(methods=['post', 'delete'])
    def subscribe(self, request, slug=None):
        """
        Manage subscription of current member to specified tag.
        """
        if (request.META['REQUEST_METHOD'] == 'POST'):
            try:
                tag = self.get_object()
            except models.Tag.DoesNotExist:
                logger.error(
                    "Attempt to create subscription for invalid tag (id=%s)" %
                    slug)
                raise exceptions.NotFound('The tag specified was not found.')
            try:
                subscription = models.Subscription.objects.create(
                    tag=tag, member=request.user.current_membership)
                return Response(status=status.HTTP_201_CREATED)
            except:
                logging.exception('Problem creating new subscription.')
                raise exceptions.APIException(
                    'Subscription could not be created.')
        elif (request.META['REQUEST_METHOD'] == 'DELETE'):
            try:
                subscription = models.Subscription.objects.get(
                    tag__slug=slug, member=request.user.current_membership)
            except models.Subscription.DoesNotExist:
                logger.error(
                    "Attempt to delete member (id=%s) subscription for invalid tag (slug=%s)"
                    % (request.user.current_membership.id, slug))
                raise exceptions.NotFound('The tag specified was not found.')
            self.perform_destroy(subscription)
            return Response(status=status.HTTP_204_NO_CONTENT)

    @detail_route(methods=['get'])
    def subscribers(self, request, slug=None):
        """
        Returns list of subscribers for specified tag.
        """
        subscribers = models.Subscription.objects.filter(tag__slug=slug)
        page = self.paginate_queryset(subscribers)
        if page is not None:
            serializer = serializers.SubscriptionSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = serializers.SubscriptionSerializer(subscribers, many=True)
        return Response(serializer.data)


class Subscriptions(generics.ListAPIView):
    """
    List view to retrieve all subscriptions for current member.
    """
    serializer_class = serializers.SubscriptionSerializer

    def get_queryset(self):
        return models.Subscription.objects.filter(
            member=self.request.user.current_membership)

import logging
from django.db import models
from autoslug import AutoSlugField
from stream_django.feed_manager import feed_manager
from stream_django.activity import Activity

logger = logging.getLogger(__name__)

class Tag(models.Model):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from='name', unique_with=['team'])
    team = models.ForeignKey(
        'team.Team', on_delete=models.CASCADE, related_name='tags')
    slack_channel_id = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'tag'

    def __str__(self):
        return "%s (%s)" % (self.slug, self.team.name)


class PostTag(models.Model, Activity):
    tag = models.ForeignKey(
        Tag, on_delete=models.CASCADE, related_name='posts')
    post = models.ForeignKey(
        'post.Post', on_delete=models.CASCADE, related_name='+')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'post_tag'

    @property
    def activity_verb(self):
        return 'publish'

    @property
    def activity_actor_attr(self):
        return self.post.member

    @property
    def activity_object_serializer_class(self):
        """
        Stream integration code uses this to programmatically get serializer
        for this model.
        """
        from post.serializers import PostTagSerializer
        return PostTagSerializer

    @property
    def activity_notify(self):
        targets = []
        for subscription in self.tag.subscriptions.all():
            if subscription.member != self.post.member:
                targets.append(feed_manager.get_notification_feed(subscription.member.id))
        return targets

class Subscription(models.Model):
    member = models.ForeignKey(
        'member.Member',
        on_delete=models.CASCADE,
        related_name='subscriptions')
    tag = models.ForeignKey(
        Tag, on_delete=models.CASCADE, related_name='subscriptions')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'subscription'

    def __str__(self):
        return "%s (@%s)" % (self.tag.name, self.member.username)

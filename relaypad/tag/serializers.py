from rest_framework import serializers
from tag import models
import member.serializers
import post.models


class TagSerializer(serializers.ModelSerializer):
    feed_uri = serializers.SerializerMethodField()

    def get_feed_uri(self, obj):
        return "/%s/tag/%s" % (obj.team.slug, obj.slug)

    class Meta:
        model = models.Tag
        lookup_field = 'slug'
        exclude = (
            'team',
            'slack_channel_id',
            'created_at',
            'updated_at', )
        extra_kwargs = {
            # This confirms that id is available in validated_data, used to alter tags in redux.
            # See: http://stackoverflow.com/questions/27858184/nested-field-serializer-data-missing
            "id": {
                "read_only": False,
                "required": False
            },
        }


class FullTagSerializer(TagSerializer):
    subscribed = serializers.SerializerMethodField()
    subscriber_count = serializers.SerializerMethodField()
    published_post_count = serializers.SerializerMethodField()
    linked_to_slack_channel = serializers.SerializerMethodField()

    def get_subscribed(self, obj):
        member = self.context['request'].user.current_membership
        if models.Subscription.objects.filter(tag=obj, member=member).exists():
            return True
        else:
            return False

    def get_published_post_count(self, obj):
        return post.models.Post.objects.filter(
            tags=obj,
            status=post.models.Post.PUBLISHED).count()

    def get_linked_to_slack_channel(self, obj):
        return obj.slack_channel_id != None

    def get_subscriber_count(self, obj):
        return models.Subscription.objects.filter(tag=obj).count()


class TagNotificationSerializer(serializers.ModelSerializer):
    """
    Reduced functionality serializer to support serializer of notification
    data (i.e. only used to provide limited data to notification components)
    """
    class Meta:
        model = models.Tag


class SubscriptionSerializer(serializers.ModelSerializer):
    member = member.serializers.SummaryMemberSerializer(read_only=True)
    name = serializers.ReadOnlyField(source='tag.name')
    slug = serializers.ReadOnlyField(source='tag.slug')

    class Meta:
        model = models.Subscription

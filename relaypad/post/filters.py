import logging
import django_filters
from post import models

logger = logging.getLogger(__name__)


class PostFilterSet(django_filters.rest_framework.FilterSet):
    tag = django_filters.CharFilter(method='filter_tag')
    author = django_filters.CharFilter(method="filter_author")

    def filter_tag(self, queryset, name, value):
        """
        ?tag=<tag_slug> queries will return published posts with the specified tag.
        """
        return queryset.filter(tags__slug=value)

    def filter_author(self, queryset, name, value):
        """
        ?author=<username> queries will return published posts written by specified user.
        """
        return queryset.filter(member__username=value)

    class Meta:
        model = models.Post
        fields = ['tag', 'author']

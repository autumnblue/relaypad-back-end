import logging
import operator
from django.template import loader

from post import models
from tag import models as tag_models

logger = logging.getLogger(__name__)


def top_posts_since(member, since_dt):
    top_posts = []
    for p in models.Post.objects.filter(
            team=member.team,
            status=models.Post.PUBLISHED,
            published_at__gt=since_dt).order_by('-published_at'):
        top_posts.append((p, 1.0))

    # Now we can do some scoring
    # TODO

    top_posts.sort(key=operator.itemgetter(1))
    return [p for p, _ in top_posts]


def create_welcome_note(member):
    """
    Onboarding involves creating a Getting Started note for each new user. Limits are placed in the client basd on the slug to prevent this note from being edited or published by the user.
    """
    logger.info(
        "Creating welcome note, authored by %s on %s team.",
        member.username, member.team.slug)

    post_content = loader.render_to_string('relaypad_quick_start.html',
                                           {'team_name': member.team.name})
    post = models.Post(
        member=member,
        team=member.team,
        title='RelayPad Quick Start Guide',
        slug= 'welcome-to-relaypad-' + member.username,
        status=models.Post.DRAFT,
        content=post_content)
    post.save()

    tag = tag_models.Tag(name='RelayPad', team=member.team)
    tag.save()

    post_tag = tag_models.PostTag(post=post, tag=tag)
    post_tag.save()

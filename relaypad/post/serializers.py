from rest_framework import serializers

from core.helpers.serializer_helpers import ReactionListField
from post import models
import comment.models
import reaction.models
import bookmark.models
import member.serializers
import tag.serializers
import tag.models


class PostSerializer(serializers.ModelSerializer):
    member = member.serializers.SummaryMemberSerializer(read_only=True)
    tags = tag.serializers.TagSerializer(required=False, many=True)
    reactions = ReactionListField(read_only=True)
    bookmarked = serializers.SerializerMethodField()
    reaction_count = serializers.SerializerMethodField()
    comment_count = serializers.SerializerMethodField()
    detail_uri = serializers.SerializerMethodField()

    def get_comment_count(self, obj):
        return comment.models.Comment.objects.filter(post=obj).count()

    def get_reaction_count(self, obj):
        # We also include all the raw reactions in this serializer, so we
        # can assume they are pre-fetched.
        return len(obj.reactions.all())

    def get_detail_uri(self, obj):
        return "/%s/notes/@%s/%s" % (obj.team.slug, obj.member.username ,obj.slug)

    def get_bookmarked(self, obj):
        member = self.context['request'].user.current_membership
        if bookmark.models.Bookmark.objects.filter(
                post=obj, member=member).exists():
            return True
        else:
            return False

    def create(self, validated_data):
        """
        Overridden since DRF doesn't support population of nested relationships
        by default. Need to look for tag data and handle separately.
        """
        tags_validated_data = validated_data.pop(
            'tags',
            None)  # Pop tags from validated_data prior to post creation.
        post = models.Post.objects.create(**validated_data)

        if tags_validated_data:
            post = self._update_post_tags(post, tags_validated_data)

        return post

    def update(self, instance, validated_data):
        """
        Overridden due to Django Rest Framework (DRF) not supporting updates
        to nested relationships by default. This implementation looks for tag data and
        handles it separately, before passing remaining data to super update method.
        """

        if 'tags' in validated_data:
            instance = self._update_post_tags(instance, validated_data['tags'])
            validated_data.pop(
                'tags',
                None)  # Remove nested relationship data, already handled.

        return super(PostSerializer, self).update(instance, validated_data)

    def _update_post_tags(self, instance, tags_validated_data):
        """
        Update tags associated with post.
        """
        # Look for tags that have been removed and persist change.
        submitted_tag_ids = [tag['id'] for tag in tags_validated_data]
        for existing_tag in instance.tags.all():
            if existing_tag.id not in submitted_tag_ids:
                tag.models.PostTag.objects.filter(
                    post_id=instance.id, tag_id=existing_tag.id).delete()

        # Look for new tag additions and persist.
        for submitted_tag_id in submitted_tag_ids:
            if submitted_tag_id not in (existing_tag.id
                                        for existing_tag in instance.tags.all()
                                        ):
                tag.models.PostTag.objects.create(
                    post_id=instance.id, tag_id=submitted_tag_id)

        return instance

    class Meta:
        model = models.Post
        fields = ('id', 'slug', 'title', 'content', 'member', 'status',
                  'comment_count', 'reaction_count', 'tags', 'reactions', 'published_at',
                  'created_at', 'updated_at', 'bookmarked',
                  'detail_uri')
        lookup_field = 'slug'

class SummaryPostSerializer(PostSerializer):
    reactions = None

    class Meta:
        model = models.Post
        fields = ('id', 'slug', 'title', 'content', 'member', 'status',
                  'comment_count', 'reaction_count', 'tags', 'published_at', 'created_at',
                  'bookmarked', 'detail_uri')

class PostNotificationSerializer(serializers.ModelSerializer):
    """
    Reduced functionality serializer to support serializer of notification
    data (i.e. only used to provide limited data to notification components)
    """
    detail_uri = serializers.SerializerMethodField()

    def get_detail_uri(self, obj):
        return "/%s/notes/@%s/%s" % (obj.team.slug, obj.member.username ,obj.slug)

    class Meta:
        model = models.Post
        fields = ('slug', 'title', 'detail_uri')

class PostTagSerializer(serializers.ModelSerializer):
    post = PostNotificationSerializer(read_only=True)
    tag = tag.serializers.TagNotificationSerializer(read_only=True)

    class Meta:
        model = tag.models.PostTag

from django.contrib import admin

import tag.models
import comment.models
import reaction.models

import reaction.admin
import tag.admin
from post import models


class PostsInline(admin.TabularInline):
    model = models.Post
    extra = 0
    can_delete = True
    verbose_name_plural = "Posts written by this user"


class PostTagsInline(admin.TabularInline):
    model = tag.models.PostTag
    can_delete = True
    verbose_name = "Tags"
    verbose_name_plural = "Tags"
    extra = 1


class CommentsInline(admin.TabularInline):
    model = comment.models.Comment
    extra = 1
    can_delete = True
    verbose_name = "Comment"
    verbose_name_plural = "Comments"


class PostReactionsInline(reaction.admin.ReactionsInline):
    fields = ('type', 'member')


@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'team',
        'member',
        'status',
        'published_at',
        'created_at')
    date_hierarchy = 'created_at'
    ordering = ('-created_at', )
    inlines = (
        PostReactionsInline,
        CommentsInline,
        PostTagsInline, )
    readonly_fields = (
        'slug',
        'published_at',
        'created_at',
        'updated_at', )

import logging
from django.db.models import Q
from rest_framework import viewsets, generics, status, exceptions
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.postgres.search import SearchRank, SearchVector

from post import filters
from post import models
from post import serializers
from post import permissions
from bookmark import models as bookmark_models
from bookmark import serializers as bookmark_serializers
from comment import models as comment_models
from comment import serializers as comment_serializers
from reaction import models as reaction_models
from reaction import serializers as reaction_serializers

logger = logging.getLogger(__name__)


class PostViewSet(viewsets.ModelViewSet):
    """
    Viewset for handling post specific endpoints.
    """
    lookup_field = 'slug'
    filter_class = filters.PostFilterSet
    serializer_class = serializers.PostSerializer
    permission_classes = (
        IsAuthenticated,
        permissions.IsAuthorOrReadOnly, )

    def get_queryset(self):
        return models.Post.objects.filter(
            status=models.Post.PUBLISHED,
            team=self.request.user.current_membership.team).order_by(
                '-published_at').select_related(
                    'team', 'member', 'member__team',
                    'member__avatar_photo').prefetch_related('reactions')

    def perform_create(self, serializer):
        serializer.save(
            member=self.request.user.current_membership,
            team=self.request.user.current_membership.team)

    def update(self, request, *args, **kwargs):
        """
        Overridden to enable operations against posts that are in 'DRAFT' status.
        """
        try:
            instance = models.Post.objects.get(
                slug=kwargs.pop('slug', None),
                team=request.user.current_membership.team)
        except models.Post.DoesNotExist:
            raise exceptions.NotFound('The post specified was not found.')
        self.check_object_permissions(self.request, instance)
        if 'post' in request.data:
            # Added to simplify updating of post from front-end, just send everything.
            serializer = self.get_serializer(
                instance, data=request.data['post'], partial=True)
        else:
            serializer = self.get_serializer(
                instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """
        Overridden to enable authors to retrieve posts that are in 'DRAFT' status. By default
        only 'PUBLISHED' posts will be returned.
        """
        try:
            # Should retrieve PUBLISHED posts for all, and DRAFT posts if user is author.
            slug = kwargs.pop('slug', None)
            instance = models.Post.objects.get(
                Q(slug=slug,
                  team=request.user.current_membership.team,
                  status=models.Post.PUBLISHED) | Q(
                      slug=slug,
                      team=request.user.current_membership.team,
                      status=models.Post.DRAFT,
                      member=request.user.current_membership))
        except models.Post.DoesNotExist:
            raise exceptions.NotFound('The post specified was not found.')
        self.check_object_permissions(self.request, instance)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @detail_route(
        methods=['post', 'delete'], permission_classes=[IsAuthenticated])
    def bookmark(self, request, slug=None):
        """
        Manage bookmarking of specified post for current member.
        """
        if (request.META['REQUEST_METHOD'] == 'POST'):

            try:
                # TODO: Should not allow bookmarking of other users drafts.
                post = models.Post.objects.get(
                    slug=slug,
                    team=request.user.current_membership.team,
                    status__in=[models.Post.PUBLISHED, models.Post.DRAFT])
            except models.Post.DoesNotExist:
                raise exceptions.NotFound('The post specified was not found.')

            serializer = bookmark_serializers.BookmarkSerializer(data={
                'post': post.id,
                'member': request.user.current_membership.id
            })
            if (serializer.is_valid(raise_exception=True)):
                serializer.save()
                return Response(
                    serializer.data, status=status.HTTP_201_CREATED)
        elif (request.META['REQUEST_METHOD'] == 'DELETE'):
            try:
                bm = bookmark_models.Bookmark.objects.get(
                    post__slug=slug, member=request.user.current_membership)
            except bookmark_models.Bookmark.DoesNotExist:
                logger.error(
                    "Attempt to delete member (id=%s) bookmark for invalid post (id=%s)",
                    request.user.current_membership.id, slug)
                raise exceptions.NotFound('The post specified was not found.')
            self.perform_destroy(bm)
            return Response(status=status.HTTP_204_NO_CONTENT)

    @detail_route(
        methods=['post', 'put'], permission_classes=[IsAuthenticated])
    def react(self, request, slug=None):
        """
        Handle reacting or unreacting to specific post
        """
        if (request.META['REQUEST_METHOD'] == 'POST'):
            post = self.get_object()
            serializer = reaction_serializers.ReactionWriteSerializer(data={
                'post': post.id,
                'member': request.user.current_membership.id,
                'type': request.data['type']
            })
            if (serializer.is_valid(raise_exception=True)):
                serializer.save()
                return Response(
                    serializer.data, status=status.HTTP_201_CREATED)
        elif (request.META['REQUEST_METHOD'] == 'PUT'):
            # Using PUT for reaction removal since we need to pass reaction type in body (DELETE prevents that).
            post = self.get_object()
            try:
                a_reaction = reaction_models.Reaction.objects.get(
                    post=post,
                    member=request.user.current_membership,
                    type=request.data['type'])
            except reaction_models.Reaction.DoesNotExist:
                logger.error("Attempt to delete a reaction that doesn't exist")
                raise exceptions.NotFound(
                    'The reaction specified was not found')
            self.perform_destroy(a_reaction)
            return Response(status=status.HTTP_204_NO_CONTENT)

    @detail_route(
        methods=['get', 'post'], permission_classes=[IsAuthenticated])
    def comments(self, request, slug=None):
        """
        Returns all comments for specified post, and create new ones.
        """
        if (request.META['REQUEST_METHOD'] == 'GET'):
            post = self.get_object()
            comments = comment_models.Comment.objects.filter(post=post)

            # TODO: Got to be a better way to handle pagination.
            page = self.paginate_queryset(comments)
            if page is not None:
                serializer = comment_serializers.CommentSerializer(
                    page, many=True, context={'request': self.request})
                return self.get_paginated_response(serializer.data)
            serializer = serializers.CommentSerializer(
                comments, many=True, context={'request': self.request})
            return Response(serializer.data)

        elif (request.META['REQUEST_METHOD'] == 'POST'):
            post = self.get_object()
            serializer = comment_serializers.CommentSerializer(
                data={
                    'post': post.id,
                    'member': request.user.current_membership.id,
                    'content': request.data['content']
                },
                context={'request': self.request})
            if (serializer.is_valid(raise_exception=True)):
                try:
                    a_comment = comment_models.Comment.objects.create(
                        post=post,
                        member=request.user.current_membership,
                        content=request.data['content'])
                    comment_serializer = comment_serializers.CommentSerializer(
                        a_comment, context={'request': self.request})
                    return Response(
                        comment_serializer.data,
                        status=status.HTTP_201_CREATED)
                except:
                    logging.exception('Problem creating new comment.')
                    raise exceptions.APIException(
                        "Problem creating new comment.")


class Drafts(generics.ListAPIView):
    """
    List view for draft posts associated with current member.
    """
    serializer_class = serializers.PostSerializer

    def get_queryset(self):
        return models.Post.objects.filter(
            member=self.request.user.current_membership,
            status=models.Post.DRAFT).order_by('-created_at')


class Notes(generics.ListAPIView):
    """
    List view for the current member's notes (published and private).
    """
    serializer_class = serializers.PostSerializer

    def get_queryset(self):
        return models.Post.objects.filter(
            member=self.request.user.current_membership).exclude(
            status=models.Post.DELETED).order_by('-updated_at')


class Search(generics.ListAPIView):
    """
    Supports search queries, based on `q` query string parameter.
    """
    MIN_DISPLAY_RANK = 0.01
    serializer_class = serializers.PostSerializer

    def get_queryset(self):
        queryset = models.Post.objects.filter(
            team=self.request.user.current_membership.team,
            status=models.Post.PUBLISHED)
        query = self.request.query_params.get('q', None)
        if query is not None:
            logger.info("Search query (team: %s) = %s" % (
                self.request.user.current_membership.team.slug, query))
            return (models.Post.objects.annotate(rank=SearchRank(
                self._buildSearchVector(), query)).filter(
                    team=self.request.user.current_membership.team,
                    status=models.Post.PUBLISHED,
                    rank__gte=self.MIN_DISPLAY_RANK).order_by('-rank'))
        else:
            # Invalid query, just return zero results.
            return models.Post.objects.none()

    def _buildSearchVector(self):
        """
        Defines all the columns we'll search against and gives some (e.g. post title) a higher weight.
        """
        return (SearchVector(
            'title', weight='A') + SearchVector(
                'content', weight='B') + SearchVector(
                    'member__first_name',
                    'member__last_name',
                    'member__username',
                    weight='A') + SearchVector(
                        'tags__name', weight='A'))

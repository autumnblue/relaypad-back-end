import logging
from rest_framework import permissions

logger = logging.getLogger(__name__)

class IsAuthorOrReadOnly(permissions.BasePermission):
    """
    Only a post author should be able to edit content.
    """

    def has_object_permission(self, request, view, obj):

        if request.method in permissions.SAFE_METHODS:
            return True
        elif (request.method == 'POST' and request.path.endswith('comments/')):
            # TODO: Maybe move this to comment permissions? Depends on comment creation endpoint.
            # Need to allow team members to comment on other people's posts.
            if (obj.team == request.user.current_membership.team):
                return True

        return obj.member == request.user.current_membership

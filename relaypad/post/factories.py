import factory
from faker import Faker

from post import models

faker = Faker()


class PostFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Post

    title = factory.LazyAttribute(lambda obj: faker.sentence()[0:69])
    content = factory.LazyAttribute(lambda obj: faker.text())
    member = factory.SubFactory('member.factories.MemberFactory')
    team = factory.SubFactory('team.factories.TeamFactory')

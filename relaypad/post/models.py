import logging
from django.db import models
from django.utils import timezone
from autoslug import AutoSlugField

from slack import tasks as slack_tasks

logger = logging.getLogger(__name__)

class Post(models.Model):
    PUBLISHED = 'PUBLISHED'
    DRAFT = 'DRAFT'
    DELETED = 'DELETED'
    STATUS_CHOICES = (
        (PUBLISHED, 'Published'),
        (DRAFT, 'Draft'),
        (DELETED, 'Deleted'),
    )

    title = models.CharField(max_length=70)
    content = models.TextField(null=True)
    member = models.ForeignKey('member.Member')
    team = models.ForeignKey('team.Team')
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=DRAFT)
    slug = AutoSlugField(populate_from='title', unique_with=['team'])
    # FIXME: reverse this
    tags = models.ManyToManyField('tag.Tag', through='tag.PostTag')
    published_at = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if self.status == self.PUBLISHED and self.published_at is None:
            # Post is being published for first time, set published_at date.
            self.published_at = timezone.now()

            if self.team.slack_app_access_token:
                # Slack integrated, see if we need to post anything to channels.
                slack_tasks.share_post(self)

        return super(Post, self).save(*args, **kwargs)

    class Meta:
        db_table = 'post'

    def __str__(self):
        return self.title

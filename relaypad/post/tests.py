import logging
import datetime
from django.test import TestCase
from django.utils import timezone
from rest_framework import status
from faker import Faker

import core.tests
from post import factories
from post import models
from post import service
from member import factories as member_factories
from tag import factories as tag_factories
from comment import factories as comment_factories

faker = Faker()
logger = logging.getLogger(__name__)

class PostModelTestCase(TestCase):

    def test_new_post_has_no_published_date(self):
        post = factories.PostFactory()
        self.assertIsNone(post.published_at)

    def test_publishing_populates_published_date(self):
        post = factories.PostFactory(status=models.Post.DRAFT)
        self.assertIsNone(post.published_at)
        post.status = models.Post.PUBLISHED
        post.save()
        self.assertIsNotNone(post.published_at)

class PostTestCase(core.tests.AuthenticatedTestCase):
    """
    Tests post API operations.
    """

    def setUp(self):
        """
        We will have two members, each with two posts (one draft and one published)
        """
        super(PostTestCase, self).setUp()
        self.second_member = member_factories.MemberFactory(
            team=self.member.team)
        self._create_draft_and_published_post(self.member)
        self._create_draft_and_published_post(self.second_member)

    def _create_draft_and_published_post(self, member):
        factories.PostFactory(member=member, team=member.team)
        factories.PostFactory(member=member, team=member.team, status=models.Post.PUBLISHED)

    def test_post_published_retrieval(self):
        response = self.client.get('/api/v1/posts/')
        self.assertEquals(response.data['count'], 2)

    def test_post_draft_list_retrieval(self):
        response = self.client.get('/api/v1/drafts/')
        self.assertEquals(response.data['count'], 1)

    def test_notes_retrieval(self):
        """
        Tests retrieval of current users published and private posts.
        """
        response = self.client.get('/api/v1/notes/')
        self.assertEquals(response.data['count'], 2)

    def test_post_creation(self):
        response = self.client.post('/api/v1/posts/', {
            'title': faker.sentence()[:69],
            'content': faker.text()
        })
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_specific_post_retrieval(self):
        post = models.Post.objects.filter(member=self.member).first()
        response = self.client.get('/api/v1/posts/%s/' % post.slug)
        self.assertEquals(response.data['title'], post.title)

    def test_invalid_post_retrieval(self):
        response = self.client.get('/api/v1/posts/fake-post-that-does-not-exist/')
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_other_teams_post_retrieval(self):
        post = factories.PostFactory(status=models.Post.PUBLISHED)
        response = self.client.get('/api/v1/posts/%s/' % post.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_other_members_draft_retrieval(self):
        other_users_draft_post = models.Post.objects.filter(member=self.second_member,
                                                            status=models.Post.DRAFT).first()
        response = self.client.get('/api/v1/posts/%s/' % other_users_draft_post.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_edit(self):
        post = models.Post.objects.filter(member=self.member).first()
        response = self.client.put(
            '/api/v1/posts/%s/' % post.slug,
            {'content': faker.text()})
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_post_deletion(self):
        post = factories.PostFactory(member=self.member,
                                     team=self.member.team)
        response = self.client.patch('/api/v1/posts/%s/' % post.slug,
                                     {'status': 'DELETED'})
        self.assertEquals(response.data['status'], 'DELETED')

    def test_other_teams_post_deletion(self):
        a_post = factories.PostFactory(status=models.Post.PUBLISHED)
        response = self.client.patch('/api/v1/posts/%s/' % a_post.slug,
                                     {'status': 'DELETED'})
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_editing_other_members_post(self):
        post = factories.PostFactory(
            member=self.second_member,
            team=self.member.team)
        response = self.client.patch(
            '/api/v1/posts/%s/' % post.slug,
            {'title': faker.text()})
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieving_own_draft(self):
        draft = models.Post.objects.filter(
            member=self.member,
            status=models.Post.DRAFT).first()
        response = self.client.get('/api/v1/posts/%s/' % draft.slug)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_retrieving_other_members_draft(self):
        draft = models.Post.objects.filter(
            member=self.second_member,
            status=models.Post.DRAFT).first()
        response = self.client.get('/api/v1/posts/%s/' % draft.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_filter_by_author(self):
        response = self.client.get('/api/v1/posts/?author=%s' %
                                   self.second_member.username)
        self.assertEquals(response.data['count'], 1)

    def test_post_filter_by_own_username(self):
        response = self.client.get('/api/v1/posts/?author=%s' %
                                   self.member.username)
        self.assertEquals(response.data['count'], 1)

    def test_post_filter_by_tag(self):
        post = models.Post.objects.filter(
            member=self.member,
            status=models.Post.PUBLISHED).first()
        post_tag = tag_factories.PostTagFactory(post=post)
        response = self.client.get('/api/v1/posts/?tag=%s' % post_tag.tag.slug)
        self.assertEquals(response.data['count'], 1)

    def test_retrieval_of_post_comments(self):
        post = models.Post.objects.filter(
            member=self.member,
            status=models.Post.PUBLISHED).first()
        a_comment = comment_factories.CommentFactory(
            post=post,
            member=self.member)
        response = self.client.get('/api/v1/posts/%s/comments/' % post.slug)
        self.assertEquals(response.data['count'], 1)

    def test_retrieval_of_another_teams_post_comments(self):
        a_comment = comment_factories.CommentFactory()
        response = self.client.get('/api/v1/posts/%s/comments/' % a_comment.post.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_deleted_post_retrieval(self):
        deleted_post = factories.PostFactory(member=self.member,
                                             team=self.member.team,
                                             status=models.Post.DELETED)
        response = self.client.get('/api/v1/posts/%s/' % deleted_post.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

class SearchTestCase(core.tests.AuthenticatedTestCase):
    """
    Team members can react to post and comment content. This class tests
    api operations related to those reactions.
    """

    def setUp(self):
        """
        Set up some post and comment content to react to.
        """
        super(SearchTestCase, self).setUp()
        self.post = factories.PostFactory(
            member=self.member,
            team=self.member.team,
            status=models.Post.PUBLISHED)
        self.post_tag = tag_factories.PostTagFactory(post=self.post)

    def test_search_endpoint(self):
        response = self.client.get('/api/v1/search/?q=%s' % self.post.title)
        self.assertNotEqual(response.data['count'], 0)

    def test_empty(self):
        response = self.client.get('/api/v1/search/?q=foobar')
        self.assertEqual(response.data['count'], 0)

    def test_tag(self):
        response = self.client.get('/api/v1/search/?q=%s' %
                                   self.post_tag.tag.slug)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['id'], self.post.id)


class TopPostsSinceTestCase(TestCase):
    def setUp(self):
        self.member = member_factories.MemberFactory()
        self.post = factories.PostFactory(
            member=self.member,
            team=self.member.team,
            status=models.Post.PUBLISHED,
            published_at=timezone.now())

    def test(self):
        posts = service.top_posts_since(self.member, timezone.now() - datetime.timedelta(hours=1))
        self.assertEqual(posts[0], self.post)

from django.db import models


class Bookmark(models.Model):
    member = models.ForeignKey(
        'member.Member', on_delete=models.CASCADE, related_name="bookmarks")
    post = models.ForeignKey(
        'post.Post', on_delete=models.CASCADE, related_name="bookmarkers")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'bookmark'
        unique_together = (
            'member',
            'post', )

    def __str__(self):
        return "%s (@%s)" % (self.post.title, self.member.user.username)

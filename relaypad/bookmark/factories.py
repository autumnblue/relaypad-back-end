import factory
from faker import Faker

from bookmark import models

# Utility to generate random fake content.
faker = Faker()


class BookmarkFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Bookmark

    member = factory.SubFactory('member.factories.MemberFactory')
    post = factory.SubFactory('post.factories.PostFactory', member=member)

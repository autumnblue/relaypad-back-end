from rest_framework import serializers

from bookmark import models


class BookmarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Bookmark

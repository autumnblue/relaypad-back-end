from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse

import core.tests
from bookmark import models
from bookmark import factories
from post import models as post_models
from post import factories as post_factories


class BookmarkTestCase(core.tests.AuthenticatedTestCase):
    """
    Tests operations related to allowing a team member to bookmark a post.
    """

    def test_bookmark_creation(self):
        p = post_factories.PostFactory(
            team=self.member.team,
            status=post_models.Post.PUBLISHED)
        response = self.client.post('/api/v1/posts/%s/bookmark/' % p.slug)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_bookmarking_of_invalid_post(self):
        response = self.client.post('/api/v1/posts/SHOULDNOTEXIST/bookmark/')
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_bookmarking_of_another_teams_post(self):
        another_teams_post = post_factories.PostFactory(status=post_models.Post.PUBLISHED)
        response = self.client.post('/api/v1/posts/%s/bookmark/' % another_teams_post.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_bookmark_retrieval(self):
        a_post = post_factories.PostFactory(
            team=self.member.team,
            status=post_models.Post.PUBLISHED)
        a_bookmark = factories.BookmarkFactory(member=self.member, post=a_post)
        response = self.client.get(reverse('bookmarks'))
        self.assertEquals(
            response.data['count'],
            models.Bookmark.objects.filter(member=self.member).count())

    def test_bookmark_removal(self):
        p = post_factories.PostFactory(member=self.member, team=self.member.team)
        bookmark = factories.BookmarkFactory(member=self.member, post=p)
        response = self.client.delete('/api/v1/posts/%s/bookmark/' % p.slug)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

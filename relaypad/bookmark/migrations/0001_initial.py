# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-01-09 00:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('member', '0001_initial'),
    ]

    state_operations = [
        migrations.CreateModel(
            name='Bookmark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('member', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bookmarks', to='member.Member')),
            ],
            options={
                'db_table': 'bookmark',
            },
        ),
    ]

    operations = [migrations.SeparateDatabaseAndState(state_operations=state_operations)]

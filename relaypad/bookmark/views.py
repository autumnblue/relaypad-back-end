from rest_framework import generics

from bookmark import models
import post.serializers
import post.models


class Bookmarks(generics.ListAPIView):
    """
    List view for bookmarks associated with current member.
    """
    serializer_class = post.serializers.PostSerializer

    def get_queryset(self):
        return post.models.Post.objects.filter(
            bookmarkers__member=self.request.user.current_membership,
            status=post.models.Post.PUBLISHED).order_by('-created_at')

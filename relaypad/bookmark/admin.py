from django.contrib import admin
from bookmark import models

@admin.register(models.Bookmark)
class BookmarkAdmin(admin.ModelAdmin):
    pass


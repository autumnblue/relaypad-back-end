import logging
from rest_framework import permissions

logger = logging.getLogger(__name__)

class IsTeamMember(permissions.BasePermission):
    """
    Only team members should be able to take action on a team
    """

    def has_object_permission(self, request, view, obj):

        return obj == request.user.current_membership.team

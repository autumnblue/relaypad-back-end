from django.contrib import admin

from team import models
import tag.models
import member.models
import member.admin


class TagsInline(admin.TabularInline):
    model = tag.models.Tag
    can_delete = True
    extra = 1
    verbose_name = "Tags"
    verbose_name_plural = "Tags available for this team"


@admin.register(models.Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'updated_at')
    fields = ('name', 'slug', 'slack_id', 'logo', 'created_at', 'updated_at')
    inlines = (member.admin.MembersInline, TagsInline)
    ordering = ('-created_at', )

    # Customize form
    readonly_fields = (
        'slug',
        'slack_id',
        'created_at',
        'updated_at', )

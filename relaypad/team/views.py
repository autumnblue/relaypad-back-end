import logging
from django.shortcuts import render
from rest_framework import viewsets, mixins, status, exceptions
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import detail_route

from core import auth0_helper
from team import models
from team import serializers
from team.permissions import IsTeamMember
from member import models as member_models
from slack import tasks as slack_tasks

logger = logging.getLogger(__name__)


class TeamViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    """
    Viewset for handling team specific endpoints.
    """
    serializer_class = serializers.TeamSerializer
    permission_classes = [IsAuthenticated, IsTeamMember]
    lookup_field = 'slug'

    def get_queryset(self):
        return models.Team.objects.filter(
            members__id=self.request.user.current_membership.id,
            members__status=member_models.Member.ACTIVE)

    def update(self, request, *args, **kwargs):
        """
        Overriding to allow data passed to PUT and PATCH requests to be
        wrapped inside a 'team' object. We do something similar for 'member'
        updates.
        TODO: This should probably be implemented via http://www.django-rest-framework.org/api-guide/serializers/#to_internal_valueself-data
        """
        try:
            instance = models.Team.objects.get(
                slug=kwargs.pop('slug'),
                members__id=self.request.user.current_membership.id,
                members__status=member_models.Member.ACTIVE)
        except models.Team.DoesNotExist:
            raise exceptions.NotFound('The team specified was not found')
        else:
            # Have to explicitly call due to object retrieval being overridden
            self.check_object_permissions(self.request, instance)

            if 'team' in request.data:
                data = request.data['team']
            else:
                data = request.data

            serializer = self.get_serializer(instance, data, partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data)

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated, IsTeamMember])
    def slack_integration(self, request, slug=None):
        """
        Manage integrations for this team. To start, it will only focus on Slack.
        """
        team = self.get_object()
        self.check_object_permissions(self.request, team)
        logger.info(
            "Configuring slack integration for %s team, installed by @%s.",
            request.user.current_membership.team.slug,
            request.user.current_membership.username)

        # Update team with integration data.
        team.slack_app_access_token = auth0_helper.get_user_slack_access_token(
            request.user.current_membership.slack_id)
        team.slack_app_installing_member = request.user.current_membership
        team.save()

        # Kickoff async syncing of Slack data.
        slack_tasks.sync_slack_team(team)

        return Response(self.get_serializer(team).data, status=status.HTTP_201_CREATED)

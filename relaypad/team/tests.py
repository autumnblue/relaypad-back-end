import logging
from django.test import TestCase
from rest_framework import status

import core.tests
from team import models
from team import factories
from member import models as member_models
from member import factories as member_factories

logger = logging.getLogger(__name__)

class TeamTestCase(core.tests.AuthenticatedTestCase):
    """
    Tests operations related to customer being a member of multiple one or more teams.
    """

    def setUp(self):
        super(TeamTestCase, self).setUp()

        self.no_membership_team = factories.TeamFactory()
        self.members_second_team = factories.TeamFactory()
        self.membership2 = member_factories.MemberFactory(
            user=self.member.user,
            team=self.members_second_team)

    def test_active_team_membership_count(self):
        response = self.client.get('/api/v1/teams/')
        self.assertEquals(
            response.data['count'],
            models.Team.objects.filter(
                members__id=self.member.id,
                members__status=member_models.Member.ACTIVE).count())

    def test_team_detail_retrieval(self):
        response = self.client.get('/api/v1/teams/%s/' % self.member.team.slug)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_no_membership_team_retrieval(self):
        response = self.client.get('/api/v1/teams/%s/' % self.no_membership_team.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_membership_status_referencing(self):
        team_with_inactive_membership = factories.TeamFactory()
        inactive_membership = member_factories.MemberFactory(
            team=team_with_inactive_membership,
            status=member_models.Member.INACTIVE)
        response = self.client.get('/api/v1/teams/')
        self.assertEquals(
            response.data['count'],
            models.Team.objects.filter(
                members__id=self.member.id,
                members__status=member_models.Member.ACTIVE).count())

    def test_team_creation(self):
        response = self.client.post('/api/v1/teams/',
                                    {'name': 'Unit test created team'})
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_team_deletion(self):
        response = self.client.delete('/api/v1/teams/%s/' % self.member.team.id)
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_team_edit(self):
        response = self.client.put('/api/v1/teams/%s/' % self.member.team.slug,
                                   {'team': {'name': 'Updated team name'}},
                                   format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_no_membership_team_edit(self):
        response = self.client.put('/api/v1/teams/%s/' % self.no_membership_team.slug,
                                   {'team': {'name': 'Updated team name'}},
                                   format='json')
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_team_creation(self):
        response = self.client.post('/api/v1/teams/%s/' % self.member.team.slug,
                                    {'name': 'Updated team name'})
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_team_partial_update(self):
        response = self.client.patch('/api/v1/teams/%s/' % self.member.team.slug,
                                     {'team': {'name': 'Updated team name'}},
                                     format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_no_membership_partial_update(self):
        response = self.client.patch('/api/v1/teams/%s/' % self.no_membership_team.slug,
                                   {'team': {'name': 'Updated team name'}},
                                   format='json')
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_team_slack_integration_configuration(self):
        # TODO: Implementing this would mean our tests always call Auth0. Can we fake that?
        # response = self.client.post('/api/v1/teams/%s/slack_integration/' % self.member.team.slug)
        # self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        pass

    def test_prevent_non_team_member_slack_configuration(self):
        # TODO: Implementing this would mean our tests always call Auth0. Can we fake that?
        # response = self.client.post('/api/v1/teams/%s/slack_integration/' % self.no_membership_team.slug)
        # self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)
        pass

    def test_no_membership_slack_integration_configuration(self):
        response = self.client.post('/api/v1/teams/%s/slack_integration/' % self.no_membership_team.slug)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_team_slack_integration_configuration(self):
        response = self.client.post('/api/v1/teams/INVALID_TEAM_SLUG/slack_integration/')
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

import logging

from relaypad.celery import app
from team import service as team_service
from team import models as team_models

logger = logging.getLogger(__name__)

@app.task(bind=True, max_retries=3, default_retry_delay=5)
def send_welcome_email(self, team_id):

    try:
        team = team_models.Team.objects.get(id=team_id)
    except team_models.Team.DoesNotExist:
        logger.error("Welcome email send requested for team that doesn't exist (id=%s)", team_id)
    else:
        logger.info("Sending welcome email for %s team", team.slug)
        email = team_service.build_team_welcome_email(team)

        try:
            email.send()
        except Exception:
            self.retry()

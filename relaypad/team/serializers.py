from rest_framework import serializers

from team import models


class TeamSerializer(serializers.ModelSerializer):
    logo_id = serializers.SerializerMethodField()
    slack_integrated = serializers.SerializerMethodField()

    def get_logo_id(self, obj):
        if obj.logo:
            return obj.logo.public_id
        else:
            return None

    def get_slack_integrated(self, obj):
        return obj.slack_app_access_token != None

    class Meta:
        model = models.Team
        exclude = ('logo', 'slack_id', 'slack_app_access_token', 'slack_app_installing_member', 'created_at', 'updated_at')
        lookup_field = 'slug'

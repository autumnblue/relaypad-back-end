from django.db import models
from autoslug import AutoSlugField

class Team(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = AutoSlugField(default=None, unique=True, populate_from='name')
    onboarded = models.BooleanField(default=False)
    slack_id = models.CharField(max_length=50, null=True, blank=True)
    slack_app_access_token = models.CharField(max_length=200, null=True, blank=True)
    slack_app_installing_member = models.OneToOneField(
        'member.Member',
        related_name='+',
        on_delete=models.SET_NULL,
        null=True,
        blank=True)
    logo = models.ForeignKey('photo.Photo', on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'team'

    def __str__(self):
        return self.name

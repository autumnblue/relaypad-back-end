from django.core.management.base import BaseCommand, CommandError

from team import tasks as team_tasks
from team import models as team_models

class Command(BaseCommand):
    help = "Send team welcome email to first team admin."

    def add_arguments(self, parser):
        parser.add_argument(
            'team',
            metavar="team")

    def handle(self, *args, **options):
        try:
            a_team = team_models.Team.objects.get(slug=options['team'])
        except team_models.Team.DoesNotExist:
            raise CommandError("Team %s does not exist." % options['team'])
        else:
            self.stdout.write("Sending welcome email for {}".format(a_team.slug))
            team_tasks.send_welcome_email(a_team.id)

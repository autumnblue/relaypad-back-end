import logging
from django.template import loader
from django.core.mail import EmailMultiAlternatives

from member import models as member_models
from relaypad import web_urls

logger = logging.getLogger(__name__)

def build_team_welcome_email(team):
    member = team.members.filter(
        status=member_models.Member.ACTIVE).first()
    from_addr = "RelayPad <team@relaypad.com>"
    subject = "{}’s RelayPad Is Ready".format(team.name)

    context = {
        "team_name": team.name,
        "welcome_post_edit_link": web_urls.frontend_url('{}/post/introducing-relaypad/edit'.format(team.slug)),
        "new_post_link": web_urls.frontend_url('/new-post'),
        "add_to_slack_link": web_urls.frontend_url('/slack-connector'),
    }

    html_content = loader.render_to_string(
        'team_welcome_email.html',
        context)
    text_content = 'HTML Only'

    email = EmailMultiAlternatives(
        subject,
        text_content,
        from_addr,
        [member.email, ],
        reply_to=['team@relaypad.com'])
    email.attach_alternative(html_content, "text/html")

    return email

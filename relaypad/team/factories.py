import factory
from faker import Faker

from team import models

# Utility to generate random fake content.
faker = Faker()


class TeamFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Team

    name = factory.LazyAttribute(lambda obj: faker.company())

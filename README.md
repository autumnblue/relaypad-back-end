# RelayPad API
Repository responsible for RelayPad *private* API, and the database model that supports it. The API is built using [Django](https://www.djangoproject.com/) and [Django Rest Framework](www.django-rest-framework.org). Reading [`/docs/api.md`](/docs/api.md) should provide a useful overview of the endpoints supported by the API.

:information_source: To get a full instance of RelayPad running locally, get this API server running and then start the RelayPad web client, hosted in `relaypad-web` repo.

| Contents |
|:---------|
| [Quick Start Guide](#quick-start-guide) |
| [Folder Structure](#folder-structure)|
| [Testing](#testing)|
| [Deployment](#deployment)|

## Quick Start Guide
 - Clone this repository
 - Run `script/setup` (one-time environment setup)
   - Follow instructions, e.g. you may need to install PostgreSQL locally.
 - Activate `virtualenv`, usually `. env/bin/activate`
 - Run `script/server`

## Folder Structure
Overview of key files and folders in this repository.

```
  README.md
  docs/
  relaypad/
    /core
     - views.py           -- entry point to API endpoints.
     - models.py          -- database model definitions.
     - serializers.py     -- rest framework serializers.
     - urls.py            -- determines routes to API endpoints
     /migrations          -- database migrations live here
     /tests               -- unit tests live here
  app.json                -- heroku review app
  script/                 -- Standard scripts to rule them all
```
## Testing
Tests for this repository can be run using `script/test`. [CircleCi](https://circleci.com) is configured to automatically run tests for every commit pushed to GitHub. Pull requests should not be merged if tests are failing, and by default merging is disabled until tests pass.

## Deployment
We use [Heroku Pipelines](https://devcenter.heroku.com/articles/pipelines) for depoyment. Overview of environments:

| Environment | URL | Heroku App |
|:------------|:----|:-----------|
| Staging | [staging-api.relaypad.com](http://staging.relaypad.com) | [relaypad-staging-api](https://dashboard.heroku.com/apps/relaypad-staging-api) |
| Production | [api.relaypad.com](http://api.relaypad.com) | [relaypad-api](https://dashboard.heroku.com/apps/relaypad-api) |

 - Create a PR
 - *Optionally* choose to go to Heroku pipeline dashboard and create a [review app](https://devcenter.heroku.com/articles/github-integration-review-apps)
 - Merge code into `master`
 - Heroku will automatically deploy to staging
 - Go to Heroku pipeline dashboard and opt to `Promote to production...`
